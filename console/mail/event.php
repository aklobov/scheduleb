<?php

use yii\helpers\Url;

/**
 * @var $this \yii\web\View
 * @var $reminder common\models\RelEventOrg
 * @var $notificationLog common\models\NotificationLog
 */
echo sprintf('%s! Пришло время действовать!', $reminder->owner->username);
echo '<br>';
$url = Url::to(['org/view', 'id' => $reminder->org->id], true);
echo sprintf('Организация: <a href="%s" target="_blank">%s</a>.', $url, $reminder->org->title);
echo '<br>';
$url = Url::to(['rel-event-org/view', 'id' => $reminder->id], true);
echo sprintf('Событие: <a href="%s" target="_blank">%s</a>.', $url, $reminder->event->title);
echo '<br>';
echo sprintf('Дата события %s .<sup>*</sup>', Yii::$app->formatter->asDate($reminder->event->start_date));
echo '<br>';
echo sprintf('Метка о состоянии события: %s .', $notificationLog::getAllStatuses($notificationLog->status));
if (in_array($notificationLog->status, [$notificationLog::STATUS_EXPIRED])) {
    echo '<sup>*</sup><br>';
    echo 'На момент наступления события вы не сделали отметку о его завершении, поэтому оно имеет вышеуказанный статус %s .';
}
echo '<br>';
$url = Yii::$app->formatter->asUrl(Url::toRoute(['rel-event-org/view', 'id' => $reminder->id], true));
echo "Ссылка на событие в вашем рабочем кабинете {$url} .";
echo '<br>';
echo '<br>';
echo '<br>';
echo '<hr><br>';
//echo sprintf('За вами числится %d незавершенных событий. Посмотреть их можно в вашем рабочем кабинете %s', rand(1, 10), Url::to(['/calendar']));
