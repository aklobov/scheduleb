<?php

use yii\helpers\Url;

/**
 * @var $this \yii\web\View
 * @var $reminder common\models\RelEventOrg
 * @var $params array
 */
echo sprintf('%s! Уведомляем Вас, что приближается событие!', $reminder->owner->username);
echo '<br>';
$url = Url::to(['org/view', 'id' => $reminder->org->id], true);
echo sprintf('Организация: <a href="%s" target="_blank">%s</a>.', $url, $reminder->org->title);
echo '<br>';
$url = Url::to(['rel-event-org/view', 'id' => $reminder->id], true);
echo sprintf('Событие: <a href="%s" target="_blank">%s</a>.', $url, $reminder->event->title);
echo '<br>';
echo sprintf('Дата события %s .<sup>*</sup>', Yii::$app->formatter->asDate($reminder->event->curr_date));
echo '<br>';
echo sprintf('Метка о состоянии события: %s .', $reminder::getAllStatuses($reminder->status));
echo '<br>';
$url = Url::to(['rel-event-org/view', 'id' => $reminder->id], true);
echo "Ссылка на событие в вашем рабочем кабинете <a href='{$url}' target='_blank'>{$url}</a>.<sup>*</sup>";
echo '<br>';
//echo sprintf('Логин: %s', $params['email']);
echo '<br>';
//echo sprintf('Пароль: %s', $params['password']);
echo '<br>';
echo sprintf('<br><sup>*</sup>До сдачи отчёта осталось %d дней.', $reminder->event->daysTillEvent);
echo '<hr><br>';
//echo sprintf('За вами числится %d незавершенных событий. Посмотреть их можно в вашем рабочем кабинете %s', rand(1, 10), Url::to(['/calendar'], true));
