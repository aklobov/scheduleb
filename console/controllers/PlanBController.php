<?php

namespace console\controllers;

use Yii;
use yii\console\Controller;
use yii\console\widgets\Table;
use yii\helpers\Console;
use yii\helpers\Url;
use common\models\Event;
use common\models\RelEventOrg;
use common\commands\SendEmailCommand;
use common\commands\SendNotificationCommand;
use common\models\NotificationLog;
use cheatsheet\Time;
use Viber\Client;
use Viber\Api\Sender;
use Viber\Api\Message\Text;

/**
 * @author terminatorliquid
 */
class PlanBController extends Controller {

    protected $viberClient = null;
    protected $viberSender = null;

    /**
     * Рассылает напоминания о скором наступлении события
     * @return void
     */
    public function actionRemindAndNotifyAll($day = null, $outdatedPeriod = 14) {
        $err = 0;
        if (empty($day)) {
            $day = date('Y-m-d');
//            $day = '2018-12-22';
        }
//        if (empty($outdatedPeriod)) {
//            $outdatedPeriod = 14;
//        }

        try {
            Yii::$app->runAction('plan-b/remind-all', [$day]);
        }
        catch (Exception $ex) {
            echo $ex->getMessage();
            $err++;
        }
        try {
            Yii::$app->runAction('plan-b/notify-all', [$day]);
        }
        catch (Exception $ex) {
            echo $ex->getMessage();
            $err++;
        }

        //Обновляем даты всех событий
        //Долгая операция!!!
        //Event::fillAll();
        $dt = \DateTime::createFromFormat('Y-m-d', $day, new \DateTimeZone(Yii::$app->timeZone));
        Event::fillOutdated($dt->sub(new \DateInterval("P{$outdatedPeriod}D"))->format('Y-m-d'));

        $this->stdout("\nОшибки: $err.\n", Console::BOLD);
        return Controller::EXIT_CODE_NORMAL;
    }

    /**
     * Рассылает напоминания о скором наступлении события
     * @return void
     */
    public function actionRemindAll($day = null) {
        if (empty($day)) {
            $day = date('Y-m-d');
//            $day = '2018-12-22';
        }

        //$allReminds = RelEventOrg::find();
        $allReminds = RelEventOrg::find()->thisDay($day)->orEarlier($day)->with('event', 'org', 'owner');
//        echo $str          = $allReminds->createCommand()->getRawSql();
        $this->stdout($day . "\n", Console::BOLD);
        if ($allReminds->exists()) {
            $this->stdout("Перечень напоминаний для рассылки\n", Console::BOLD);
            $this->stdout('Всего напоминаний: ' . $allReminds->count() . "\n", Console::BOLD);
            foreach ($allReminds->each() as $reminder) {
                $this->_printReminder($reminder);

                try {
                    $res = $this->_notifyByEmail($reminder, NotificationLog::TYPE_REMIND);
//                    $res = Yii::$app->commandBus->handle(new SendNotificationCommand([
//                        'reminder' => $reminder,
//                        'type'     => NotificationLog::TYPE_REMIND,
//                    ]));
                }
                catch (\Swift_TransportException $e) {
                    Yii::error($e->getMessage());
                }

                $this->stdout($res . "\n", Console::BOLD);
            }
        }
        else {
            $this->stdout("Напоминания отсутствуют\n", Console::BOLD);
        }

        //Обновляем даты всех событий
        //Долгая операция!!!
        //Event::fillAll();
//        Event::fillOutdated();

        return Controller::EXIT_CODE_NORMAL;
    }

    /**
     * Рассылает сообщения о наступлении событий
     */
    public function actionNotifyAll($day = null) {
        if (empty($day)) {
            $day = date('Y-m-d');
//            $day = '2019-01-21';
        }

        $events = Event::find()->thisDay($day)->orEarlier($day);
        $this->stdout($day . "\n", Console::BOLD);
        if ($events->exists()) {
            $this->stdout("Перечень событий для рассылки\n", Console::BOLD);
            $this->stdout('Всего событий: ' . $events->count() . "\n", Console::BOLD);
            foreach ($events->each() as $event) {
                $this->_printEvent($event);

                foreach ($event->reminders as $reminder) {

                    $this->_printReminder($reminder);

                    $res = $this->_notifyByEmail($reminder, NotificationLog::TYPE_EVENT);
//                    $res = Yii::$app->commandBus->handle(new SendNotificationCommand([
//                        'reminder' => $reminder,
//                        'type'     => NotificationLog::TYPE_EVENT,
//                    ]));

                    $this->stdout($res . "\n", Console::BOLD);
                }
            }
        }
        else {
            $this->stdout("События отсутствуют\n", Console::BOLD);
        }


//        return Controller::EXIT_CODE_NORMAL;
    }

    //TODO Обновить статусы завершенных событий

    /**
     * Рассылает сообщения о просроченных событиях
     * Выполнять перед выполнением
     * !!!Не используется
     */
    public function actionNotifyOutdated() {
        $day = date('Y-m-d');
        $day = '2019-01-21';

        $reminders = RelEventOrg::find()->thisDayOutdated($day);
        $this->stdout($day . "\n", Console::BOLD);
        if ($reminders) {
            $this->stdout("Перечень просроченных событий\n", Console::BOLD);
            $this->stdout('Всего просроченных событий: ' . $reminders->count() . "\n", Console::BOLD);
            foreach ($reminders->each() as $reminder) {
                $event = $reminder->event;
                $this->stdout('ID: ', Console::BOLD);
                $this->stdout($event->id . "\t", Console::ITALIC);
                $this->stdout('event: ', Console::BOLD);
                $this->stdout($event->title . "\t", Console::ITALIC);
                $this->stdout('current: ', Console::BOLD);
                $this->stdout($event->curr_date . "\t", Console::ITALIC);
                $this->stdout('previous: ', Console::BOLD);
                $this->stdout($event->previous_date . "\n", Console::ITALIC);

                $this->stdout("\trel ID: ", Console::BOLD);
                $this->stdout($reminder->id . "\t", Console::ITALIC);
                $this->stdout('org: ', Console::BOLD);
                $this->stdout($reminder->org->title . "\t", Console::ITALIC);

                $this->stdout('remind date: ', Console::BOLD);
                $this->stdout($reminder->remind_date . "\n", Console::ITALIC);

                $log = new NotificationLog([
                    'event_date'       => $reminder->event->curr_date,
                    'remind_date'      => $reminder->remind_date,
                    'type'             => NotificationLog::TYPE_OUTDATED,
                    'event_id'         => $reminder->event->id,
                    'org_id'           => $reminder->org_id,
                    'user_id'          => $reminder->owner->id,
                    'reminder_id'      => $reminder->id,
                    'is_success_email' => true,
                ]);

                //Проверка успешной отправки в прошлом (чтобы исключить повторную отправку сообщения)
                $logSuccessRecord = NotificationLog::findOne($log->attributes);
                if (empty($logSuccessRecord)) {
                    //Если не было отправлено ранее
                    $log->is_success_email = Yii::$app->commandBus->handle(new SendEmailCommand([
                        'subject' => 'Пропущенное событие ' . $reminder->event->title,
                        'view'    => 'eventOutdated',
                        'to'      => $reminder->org->owner->email,
                        'params'  => [
                            'rel' => $reminder
                        ]
                    ]));

                    $transaction = NotificationLog::getDb()->beginTransaction();
                    try {
                        $log->save();
                        $this->stdout('Уведомление: ', Console::BOLD);
                        $this->stdout($log->is_success_email ? 'Отправлено' : 'Ошибка отправки' . "\n", Console::ITALIC);
                        if ($log->is_success_email) {
                            $reminder->status          = $reminder::STATUS_ACTIVE;
                            $reminder->status_previous = $reminder::STATUS_CANCELED;
                            $reminder->save();
                        }
                        $transaction->commit();
                    }
                    catch (\Throwable $e) {
                        $transaction->rollBack();
                    }
                }
                else {
                    $this->stdout('Уведомление: ', Console::BOLD);
                    $this->stdout('Отправлено ранее ' . Yii::$app->formatter->asDate($logSuccessRecord->created_at) . "\n", Console::ITALIC);
                }
            }
        }
        else {
            $this->stdout("Пропущенные события отсутствуют\n", Console::BOLD);
        }

        //Обновляем даты всех событий
        //Долгая операция!!!
        //Event::fillAll();
//        Event::fillOutdated();
        //TODO Проверить
        RelEventOrg::find()->thisDayOutdated($day, [RelEventOrg::STATUS_FINISHED])->updateAll(['status' => RelEventOrg::STATUS_ACTIVE, 'status_previous' => RelEventOrg::STATUS_FINISHED]);

        return Controller::EXIT_CODE_NORMAL;
    }

    /**
     * Проходит по всем объектам Event с просроченной датой и вычисляет новые даты событий
     */
    public function actionCalculateEventDates() {
        $this->stdout(date('Y-m-d h:m:s') . "\n", Console::BOLD);
        $events = Event::find()->overdue(date('Y-m-d'));
        $res    = 1;
        foreach ($events->all() as $event) {

            $this->stdout('event ID: ', Console::BOLD);
            $this->stdout($event->id . "\t", Console::ITALIC);
            $this->stdout('start: ', Console::BOLD);
            $this->stdout($event->start_date . "\t", Console::ITALIC);
            $this->stdout('next: ', Console::BOLD);
            $this->stdout($event->next_date . "\t", Console::ITALIC);
            $this->stdout('name: ', Console::BOLD);
            $this->stdout($event->title . "\n", Console::ITALIC);

            //Устанавливаем новые значения
            //$event->start_date = $event->next_date;
            //$res = $res && $event->save();
            //Обнуляем статусы всех связей
            foreach ($event->reminders as $rel) {
                //$rel->status_last = $rel->status;
                //$rel->status = RelEventOrg::STATUS_ACTIVE;
                $this->stdout("\t\t\t");
                $this->stdout('rel ID: ', Console::BOLD);
                $this->stdout($rel->id . "\t", Console::ITALIC);
                $this->stdout('next: ', Console::BOLD);
                $this->stdout($rel->event->next_date . "\t", Console::ITALIC);
                $this->stdout('name: ', Console::BOLD);
                $this->stdout($rel->event->title . "\n", Console::ITALIC);
            }
        }
    }

    public function actionTest() {
        echo $url = Url::to('best/test', true);
        echo "\n";
        printf('url: %s', Url::toRoute('/best/test', true));
        echo "\n";
    }

    protected function _printEvent(Event $event) {
        $this->stdout('ID: ', Console::BOLD);
        $this->stdout($event->id . "\t", Console::ITALIC);
        $this->stdout('event: ', Console::BOLD);
        $this->stdout($event->title . "\t", Console::ITALIC);
        $this->stdout('current: ', Console::BOLD);
        $this->stdout($event->curr_date . "\t", Console::ITALIC);
        $this->stdout('next: ', Console::BOLD);
        $this->stdout($event->next_date . "\n", Console::ITALIC);
    }

    protected function _printReminder(RelEventOrg $reminder) {
        $this->stdout('rel ID: ', Console::BOLD);
        $this->stdout($reminder->id . "\t", Console::ITALIC);
        $this->stdout('org: ', Console::BOLD);
        $this->stdout($reminder->org->title . "\t", Console::ITALIC);
        $this->stdout('current: ', Console::BOLD);
        $this->stdout($reminder->event->curr_date . "\t", Console::ITALIC);
        $this->stdout('next: ', Console::BOLD);
        $this->stdout($reminder->event->next_date . "\t", Console::ITALIC);
        $this->stdout('delta: ', Console::BOLD);
        $this->stdout($reminder->remind_before . "\t", Console::ITALIC);
        $this->stdout('name: ', Console::BOLD);
        $this->stdout($reminder->event->title . "\t", Console::ITALIC);
        $this->stdout('remind date: ', Console::BOLD);
        $this->stdout($reminder->remind_date . "\n", Console::ITALIC);
    }

    /**
     * 
     * @param RelEventOrg $reminder
     * @param string $type
     * @return type
     */
    protected function _notifyByEmail(RelEventOrg $reminder, $type) {
//        $reminder         = $this->reminder;
        $model            = new NotificationLog([
            'event_date'       => $reminder->event->curr_date,
            'remind_date'      => $reminder->remind_date,
            'type'             => $type,
            'action'           => ($reminder->event->type == Event::TYPE_CUSTOM) ? Event::TYPE_CUSTOM : $reminder->event->action,
            'transport'        => NotificationLog::TRANSPORT_EMAIL,
            'event_id'         => $reminder->event->id,
            'org_id'           => $reminder->org_id,
            'user_id'          => $reminder->owner->id,
            'reminder_id'      => $reminder->id,
            'status'           => $reminder->status,
            'is_success_email' => true,
        ]);
//        $model->scenario = $model::SCENARIO_CONSOLE_NOTIFICATION;
        //Проверка успешной отправки в прошлом (чтобы исключить повторную отправку сообщения)
        $logSuccessRecord = NotificationLog::find()->where($model->toArray([
                    'event_date',
                    'remind_date',
                    'type',
                    'transport',
                    'action',
//                    'user_id',
                    'event_id',
                    'org_id',
                    'reminder_id',
                    'is_success_email',
        ])); //->one()
        //YII_ENV_DEV - при разработке всегда отправлять напоминания
//        if (YII_ENV_DEV || empty($logSuccessRecord)) {
        if (!$logSuccessRecord->exists()) {
//            $lr = $logSuccessRecord->createCommand()->getRawSql();
            switch ($type) {
                //Если не было отправлено ранее
                case NotificationLog::TYPE_REMIND:
                    $emailCommand            = new SendEmailCommand([
                        'subject' => sprintf('Напоминание о событии %s организации %s', $reminder->event->title, $reminder->org->title),
                        'view'    => 'eventReminder',
                        'to'      => $reminder->owner->email,
//                        'to'      => $reminder->org->owner->email,
                        'params'  => [
                            'reminder'        => $reminder,
                            'notificationLog' => $model,
                        ]
                    ]);
                    $model->is_success_email = Yii::$app->commandBus->handle($emailCommand);
                    $model->save();
                    $res                     = $model->is_success_email ? 'Уведомление отправлено на почту (800).' : 'Ошибка отправки уведомления (802).';
                    break;

                case NotificationLog::TYPE_EVENT:
                default:
                    //Установка метки, если событие просрочено
                    $model->status = in_array($reminder->status, [RelEventOrg::STATUS_ACTIVE, RelEventOrg::STATUS_IN_WORK]) ? NotificationLog::STATUS_EXPIRED : $reminder->status;

                    $emailCommand            = new SendEmailCommand([
                        'subject' => sprintf('Событие %s организации %s', $reminder->event->title, $reminder->org->title),
                        'view'    => 'event',
                        'to'      => $reminder->owner->email,
                        'params'  => [
                            'reminder'        => $reminder,
                            'notificationLog' => $model,
                        ]
                    ]);
                    $model->is_success_email = Yii::$app->commandBus->handle($emailCommand);
                    $transaction             = NotificationLog::getDb()->beginTransaction();
                    try {
                        $model->save();
                        $reminder->status_ = $reminder::STATUS_ACTIVE;
                        $reminder->save();
                        $res               = $model->is_success_email ? 'Уведомление отправлено на почту (800).' : 'Ошибка отправки уведомления (802).';
                        $transaction->commit();
                    }
                    catch (\Throwable $e) {
                        $transaction->rollBack();
                        $res = 'Сбой отправки уведомления (801).';
                    }
                    break;
            }
        }
        else {
            $res = sprintf('Уведомление было отправлено ранее %s (804).', Yii::$app->formatter->asDate($logSuccessRecord->one()->created_at));
        }
        return $res;
    }

    protected function _notifyByViber(RelEventOrg $reminder, $type) {

        if (!$this->viberClient instanceof Bot) {
            $this->viberSender = new Sender([
                'name'   => Yii::$app->params['viber']['name'],
                'avatar' => Url::to('/favicon.png', 'https'),
            ]);

            $this->viberClient = new Client(['token' => Yii::$app->params['viber']['token']]);
        }

        //Проверка доступа к функции
        $can = Yii::$app->authManager->checkAccess($reminder->owner->id, 'notifyByMessengers');

        if (!$can) {
            return sprintf('Нет доступа к уведомлениям через Viber у пользователя id: %s [%s].', $reminder->owner->id, $reminder->owner->username);
        }

        //Проверка наличия viber id
        if (empty($reminder->owner->userProfile->viber_id)) {
            return sprintf('Уведомление через Viber не отправлено. У пользователя id: %s [%s] не настроен доступ к Viber.', $reminder->owner->id, $reminder->owner->username);
        }

        $model = new NotificationLog([
            'event_date'       => $reminder->event->curr_date,
            'remind_date'      => $reminder->remind_date,
            'type'             => $type,
            'action'           => ($reminder->event->type == Event::TYPE_CUSTOM) ? Event::TYPE_CUSTOM : $reminder->event->action,
            'transport'        => NotificationLog::TRANSPORT_VIBER,
            'event_id'         => $reminder->event->id,
            'org_id'           => $reminder->org_id,
            'user_id'          => $reminder->owner->id,
            'reminder_id'      => $reminder->id,
            'status'           => $reminder->status,
            'is_success_email' => true,
        ]);

        $messageText = $this->renderFile('@console/viber/event.php', ['reminder' => $reminder]);
        $message     = new Text();
        $message
                ->setSender($this->viberSender)
                ->setReceiver($reminder->owner->userProfile->viber_id)
                ->setText($messageText);
        $this->viberClient->sendMessage($message);
        return sprintf('Уведомление через Viber отправлено пользователю id: %s [%s].', $reminder->owner->id, $reminder->owner->username);
    }

}
