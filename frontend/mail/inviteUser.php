<?php

/**
 * @var $this \yii\web\View
 * @var $inviteUrl string
 * @var $inviteText string
 * @var $params array
 */
echo $inviteText . "\r\n<br>";
echo sprintf('%s!<br>Приглашаем воспользоваться удобным сервисом планирования отчётов и платежей для вашей компании на нашем сайте %s.', $params['name'], Yii::$app->name);
echo '<br>';
echo sprintf('Ссылка на ваш рабочий кабинет %s .<sup>*</sup>', Yii::$app->formatter->asUrl($inviteUrl));
echo '<br>';
echo sprintf('Логин: %s', $params['email']);
echo '<br>';
echo sprintf('Пароль: %s', $params['password']);
echo '<br>';
echo '<br><sup>*</sup>Ссылка действительна в течение 7 дней.';
