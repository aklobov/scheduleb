<?php

namespace frontend\controllers;

use Yii;
use yii\web\Controller;
use common\components\ConsoleAppHelper;
use console\controllers\PlanBController;

/**
 * Run console command
 * http://qaru.site/questions/715220/how-can-i-call-a-console-command-in-web-application-in-yii-20
 * 
 * 

  используйте этот код:

  $application = new yii\console\Application($config);
  $application->runAction('controller/action');

  Я использую этот метод вместо yii консольной команды, потому что я запускаю Yii на управляемом VPS, где команды unix не поддерживаются в cron, только для php-скриптов.

  Чтобы запустить этот путь вместо консоли, сначала необходимо инициализировать конфигурацию yii, конечно:

 */
class ConsoleController extends Controller {

    public function actionIndex() {
        if (YII_ENV_DEV) {
            $d   = Yii::$app->request->get('d', date('Y-m-d'));
            $res = ConsoleAppHelper::runAction('plan-b/remind-and-notify-all', [$d], 'console\controllers');
        }
//        $controller = new PlanBController('planb', Yii::$app);
//        $controller->actionRemindAndNotifyAll('2018-11-17');
    }

}
