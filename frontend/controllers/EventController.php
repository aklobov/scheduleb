<?php

namespace frontend\controllers;

use Yii;
use common\models\Event;
use common\models\EventException;
use common\models\User;
use common\base\LinkedMultiModel;
use frontend\models\search\EventSearch;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
//use common\filters\OwnModelAccessFilter;
use Recurr\Rule;
use Recurr\Transformer\ArrayTransformer;
use Recurr\DateUtil;
use Recurr\Frequency;
use common\components\Recurr\Transformer\Constraint\WeekDayConstraint;
use common\components\Recurr\Transformer\ArrayTransformerWeekends;

/**
 * EventController implements the CRUD actions for Event model.
 */
class EventController extends Controller {

    public $layout = 'adminlte_main';

    /**
     * {@inheritdoc}
     */
    public function behaviors() {
        return [
            'verbs'  => [
                'class'   => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow'      => true,
                        'actions'    => ['update', 'delete'],
                        'roleParams' => function($rule) {
                            return [
                                'model'     => $this->findModel(Yii::$app->request->get('id', Yii::$app->request->post('id'))),
                                'attribute' => 'created_by',
                            ];
                        },
                        'permissions' => ['manageEventDefault', 'editOwnModel'],
                    ],
                    [
                        'allow'   => TRUE,
                        'actions' => ['index', 'create', 'view'],
                        'roles'   => ['@']
                    ],
                ],
            ],
//            'modelAccess' => [
//                'class'                   => OwnModelAccessFilter::className(),
//                'only'                    => ['update', 'delete'],
//                'modelCreatedByAttribute' => 'created_by',
//                'modelClass'              => Event::className()
//            ],
        ];
    }

    /**
     * Lists all Event models.
     * @return mixed
     */
    public function actionIndex() {
        $searchModel                      = new EventSearch([
            'type'   => Event::TYPE_DEFAULT,
            'action' => Event::ACTION_REPORT,
            'status' => Event::STATUS_ACTIVE,
        ]);
        $dataProvider                     = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->sort->defaultOrder = ['title' => SORT_ASC,];
//        Yii::debug(Yii::$app->request->queryParams);

        return $this->render('index', [
                    'searchModel'  => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Event model.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id) {
        $model = $this->findModel($id);
//        $subModel = $model->eventException;
//        print_r($model->dates);

        $params                = [
            'model'     => $model,
            'attribute' => 'created_by'
        ];
        $canEditOwnModel       = Yii::$app->user->can('editOwnModel', $params);
        $canManageEventDefault = Yii::$app->user->can('manageEventDefault', $params);
        return $this->render('view', [
                    'model'                 => $this->findModel($id),
                    'canEditOwnModel'       => $canEditOwnModel,
                    'canManageEventDefault' => $canManageEventDefault,
        ]);
    }

    /**
     * Creates a new Event model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        $model = new LinkedMultiModel([
            'models' => [
                'event'     => new Event([
                    'frequency'        => Event::FREQUENCY_YEAR,
                    'strict_condition' => true,
                    'type'             => Event::TYPE_DEFAULT,
                    'action'           => Event::ACTION_REPORT,
                        ]),
                'exception' => new EventException([
                    'frequency' => Event::FREQUENCY_NONE,
                        ])
            ]
        ]);

        if ($model->load(Yii::$app->request->post()) && $model->saveLinked()) {
//            $this->saveEventException($model);
//Redirect to custom view action if custom report created
            $view = ($model->getModel('event')->type == Event::TYPE_CUSTOM) ? '/custom/view' : 'view';
            return $this->redirect([$view, 'id' => $model->getModel('event')->id]);
        }

        return $this->render('create', [
                    'model' => $model,
        ]);
    }

    /**
     * Updates an existing Event model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id) {
        $eventModel = $this->findModel($id);

        if (empty($eventModel->eventException)) {
            $eventExceptionModel = new EventException();
            $eventExceptionModel->link('event', $eventModel);
            $eventModel          = $this->findModel($id);
        }
        $model = new LinkedMultiModel([
            'models' => [
                'event'     => $eventModel,
                'exception' => $eventModel->eventException
            ]
        ]);

        if ($model->load(Yii::$app->request->post()) && $model->saveLinked()) {
            return $this->redirect(['view', 'id' => $model->getModel('event')->id]);
        }

        return $this->render('update', [
                    'model' => $model,
        ]);
    }

    /**
     * Сохраняет связанный объект EventException
     * @param Event $model
     * @return boolean
     */
//    protected function saveEventException(Event $model) {
//        if (isset($model->exception_['start_date_'], $model->exception_['recurrence_pattern'])) {
////            $params = [
////                'start_date_'         => $model->exception_start_date,
////                'recurrence_pattern' => $model->exception_recurrence_pattern
////            ];
//            $event_exception = $model->eventException;
//            if (empty($event_exception)) {
//                $event_exception = new EventException($model->exception_);
//                return $event_exception->link('event', $model);
//            }
//            else {
//                $event_exception->setAttributes($model->exception_);
//                $event_exception->save();
//            }
//            return $model->eventException;
//        }
//        return false;
//    }

    /**
     * Deletes an existing Event model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id) {
//        $this->findModel($id)->delete();
        $model = $this->findModel($id);
        $model->status = $model::STATUS_DELETED;
        $model->save();

        return $this->redirect(['index']);
    }

    /*
      public function actionRecurr() {
      //        $timezone  = 'America/New_York';
      //        $startDate = new \DateTime('2018-06-12 20:00:00', new \DateTimeZone($timezone));
      //        $endDate   = new \DateTime('2018-09-14 20:00:00', new \DateTimeZone($timezone)); // Optional
      //        $rule      = new Rule('FREQ=MONTHLY;COUNT=3', $startDate, $endDate, $timezone);
      //
      //        $transformer = new ArrayTransformer();
      //
      //        echo '<pre>';
      //        print_r($transformer->transform($rule));
      //        echo '</pre>';
      //        $timezone  = 'Europe/Moscow';
      $timezone  = Yii::$app->timeZone;
      //        $timezone  = Yii::$app->formatter->timezone;
      $startDate = new \DateTime('2019-08-11 01:00:00', new \DateTimeZone($timezone));
      $endDate   = new \DateTime('2022-01-01 01:00:00', new \DateTimeZone($timezone));
      $rule      = (new Rule)
      ->setStartDate($startDate)
      ->setTimezone($timezone)
      ->setFreq('YEARLY')
      //        ->setByMonth([1])
      //        ->setByDay([20])
      //        ->setByDay(['MO', 'TU', 'WE', 'TH', 'FR'])
      //        ->setUntil(new \DateTime('2019-12-31'));
      ->setUntil($endDate);
      //        $exclConstraint = new WeekDayConstraint([3,6]);
      echo $rule->getString();

      $transformer = new ArrayTransformerWeekends(NULL, ArrayTransformerWeekends::DIRECTION_PAST);
      //        $rules = $transformer->transform($rule, $exclConstraint)->toArray();
      $rules       = $transformer->transform($rule)->toArray();
      echo '<h2>Yearly</h2>';
      echo '<pre>';
      print_r($rules);
      echo '</pre><br>';
      print_r(DateUtil::getDayOfWeek($rules[0]->getStart()));


      $relDateString = 'last day of december +10 years';
      echo '<br>';
      print date('Y-m-d', strtotime($relDateString));
      //        print date('d M Y H:i:s', strtotime('last day of december +10 year'));

      echo '<br>last day of december +' . (int) Yii::$app->params['virtualEventLimit'] . ' years';
      $rdt = new \DateTime('last day of december +' . (int) Yii::$app->params['virtualEventLimit'] . ' years');
      echo '<br>' . $rdt->format('d.m.Y');
      die();
      }

      public function actionRecurr2() {
      $timezone  = Yii::$app->timeZone;
      $startDate = new \DateTime('2019-08-11 01:00:00', new \DateTimeZone($timezone));
      $endDate   = new \DateTime('2022-01-01 01:00:00', new \DateTimeZone($timezone));
      $rule      = (new Rule)
      ->setStartDate($startDate)
      ->setTimezone($timezone)
      ->setFreq('YEARLY')
      //                ->setFreq(3)
      ->setFreq(Frequency::YEARLY)
      ->setUntil($endDate);

      echo $rule->getString();
      }
     */

    /**
     * Finds the Event model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Event the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = Event::find()
                ->where(['id' => $id, 'action' => Event::ACTION_REPORT])
                ->andWhere(['or', 'type="' . Event::TYPE_DEFAULT . '"', 'created_by=' . getMyId()])
                ->one()) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('Ошибка. Запрошенные данные отсутствуют.');
    }

}
