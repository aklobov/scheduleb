<?php

namespace frontend\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\AccessControl;
use common\models\NotificationLog;
use common\models\search\NotificationLogSearch;

/**
 * Application timeline controller
 */
class TimelineEventController extends Controller {

    public $layout = 'adminlte_main';

    /**
     * {@inheritdoc}
     */
    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow'   => TRUE,
                        'actions' => ['index', 'events'],
                        'roles'   => ['@']
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all TimelineEvent models.
     * @return mixed
     */
    public function actionIndex() {
        $searchModel        = new NotificationLogSearch();
        $dataProvider       = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->sort = [
            'defaultOrder' => ['created_at' => SORT_DESC]
        ];

        return $this->render('index', [
                    'searchModel'  => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Подтверждение ознакомления с просроченным событием
     * @param integer $id
     * @return redirect
     */
    public function actionConfirmExpired($id) {
        $model         = NotificationLog::findOne($id);
        $model->status = NotificationLog::STATUS_EXPIRED_AND_CONFIRMED;
        $model->save();
        return $this->redirect(['index']);
    }

}
