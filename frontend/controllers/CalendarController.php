<?php

namespace frontend\controllers;

use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\Calendar;
use common\models\Org;
use common\models\Event;
use common\models\EventCalendar;
use common\models\RelEventOrg;
use frontend\models\search\EventSearch;
use yii\helpers\Url;
use cheatsheet\Time;
use yii\db\Query;

/**
 * RelEventOrgController implements the CRUD actions for RelEventOrg model.
 */
class CalendarController extends Controller {

    public $layout = 'adminlte_main';

    /**
     * {@inheritdoc}
     */
    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow'   => TRUE,
                        'actions' => ['index', 'events'],
//                        'actions' => ['index', 'events', 'fill-events', 'fill-calendar'],
                        'roles'   => ['@']
                    ],
                ],
            ],
        ];
    }

    /**
     * Show Calendar.
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found or access denied
     */
    public function actionIndex() {
        return $this->render('index');
    }

    /**
     * События в заданном диапазон дат
     * @param string $start Формат '2019-01-23'
     * @param string $end Формат '2019-01-24'
     * @return array|null
     */
    public function actionEvents($start = null, $end = null, $type = Calendar::TYPE_EVENT) {
        if (!$start) {
            $start = date('Y-m-d');
        }
        if (!$end) {
            $end = date('Y-m-d', time() + Time::SECONDS_IN_A_MONTH);
        }

        $models = Calendar::find()
//                ->where('event_date > :start_date')
//                ->andWhere('event_date < :end_date')
                ->where('event_date BETWEEN :start_date AND :end_date')
//                ->where(['between', 'event_date', ':start_date', ':end_date'])
                ->andWhere('user_id = :user_id')
                ->addParams([
            ':start_date' => $start,
            ':end_date'   => $end,
            ':user_id'    => getMyId(),
        ]);
        
        if($type != 'all'){
            $models->andWhere(['type' => $type]);
        }

        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
//        $str          = $models->createCommand()->getRawSql();
        return $models->all();
    }

    public function actionFillCalendar() {
        Calendar::fillCalendar();
    }

    public function actionFillEvents() {
        Event::fillAll();
    }

//    public function actionTest3() {
//        $model = Calendar::findOne(['reminder_id' => 9, 'event_id' => 14]);
//        print_r($model);
//        die();
//    }
//    public function actionTest2() {
//        echo Yii::$app->formatter->asUrl(Url::toRoute(['rel-event-org/view', 'id' => 5], true));
//        echo '<br>';
//        echo Url::toRoute('/', true);
//        die();
//    }
//    public function actionTest() {
//        $models = RelEventOrg::find()->select([
//                    '{{%rel_event_org}}.*',
//                    '{{%event}}.start_date as event_date',
//                    'DATE_ADD({{%event}}.start_date, INTERVAL -remind_before DAY) as remind_date',
////                    '@remind_date'
//                ])->joinWith('event')
//                ->where('{{%event}}.start_date < CURDATE()')
//                ->andWhere('DATE_ADD({{%event}}.start_date, INTERVAL -remind_before DAY) < CURDATE()')
//                ->all();
////        $models  = RelEventOrg::find()->remindToday()->all();
////        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
//
//        return $this->render('test', [
//                    'models' => $models,
//        ]);
//    }

    /**
     * Finds the Calendar model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return RelEventOrg the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
//    protected function findModel($id) {
//        if (($model = RelEventOrg::findOne($id)) !== null) {
//            return $model;
//        }
//
//        throw new NotFoundHttpException('Информация отсутствует.');
//    }
//    public function actionOrgStat($org_id) {
//        $query = (new Query())
//                ->select(['status', 'count(*)'])
//                ->from('{{%rel_event_org}}')
//                ->groupBy('status');
//        print_r($query->all());
//        die();
//    }
}
