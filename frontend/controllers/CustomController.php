<?php

namespace frontend\controllers;

use Yii;
use common\models\Event;
use common\models\EventException;
use common\models\User;
use common\base\LinkedMultiModel;
use frontend\models\search\EventSearch;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
//use common\filters\OwnModelAccessFilter;
use Recurr\Rule;
use Recurr\Transformer\ArrayTransformer;
use Recurr\DateUtil;
use Recurr\Frequency;
use common\components\Recurr\Transformer\Constraint\WeekDayConstraint;
use common\components\Recurr\Transformer\ArrayTransformerWeekends;

/**
 * CustomController implements the CRUD actions for Event model.
 * Пользовательские события
 */
class CustomController extends Controller {

    public $layout = 'adminlte_main';

    /**
     * {@inheritdoc}
     */
    public function behaviors() {
        return [
            'verbs'  => [
                'class'   => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow'      => true,
                        'actions'    => ['update', 'delete'],
                        'roleParams' => function($rule) {
                            return [
                                'model'     => $this->findModel(Yii::$app->request->get('id', Yii::$app->request->post('id'))),
                                'attribute' => 'created_by',
                            ];
                        },
                        'permissions' => ['manageEventDefault', 'editOwnModel'],
                    ],
                    [
                        'allow'   => TRUE,
                        'actions' => ['index', 'create', 'view'],
                        'roles'   => ['@']
                    ],
                ],
            ],
//            'modelAccess' => [
//                'class'                   => OwnModelAccessFilter::className(),
//                'only'                    => ['update', 'delete'],
//                'modelCreatedByAttribute' => 'created_by',
//                'modelClass'              => Event::className()
//            ],
        ];
    }

    /**
     * Lists all Event models.
     * @return mixed
     */
    public function actionIndex() {
        $searchModel                      = new EventSearch([
            'type'       => Event::TYPE_CUSTOM,
            'created_by' => getMyId(),
            'status' => Event::STATUS_ACTIVE,
//            'action' => Event::ACTION_CUSTOM,
        ]);
        $dataProvider                     = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->sort->defaultOrder = ['title' => SORT_ASC,];

//        Yii::$app->user->setReturnUrl('custom/index');
        return $this->render('index', [
                    'searchModel'  => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Event model.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id) {
        $model = $this->findModel($id);
//        $subModel = $model->eventException;
//        print_r($model->dates);

        $params                = [
            'model'     => $model,
            'attribute' => 'created_by'
        ];
        $canEditOwnModel       = Yii::$app->user->can('editOwnModel', $params);
        $canManageEventDefault = Yii::$app->user->can('manageEventDefault', $params);
        return $this->render('view', [
                    'model'                 => $this->findModel($id),
                    'canEditOwnModel'       => $canEditOwnModel,
                    'canManageEventDefault' => $canManageEventDefault,
        ]);
    }

    /**
     * Creates a new Event model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
//    public function actionCreate() {
//        $model = new LinkedMultiModel([
//            'models' => [
//                'event'     => new Event([
//                    'frequency'        => Event::FREQUENCY_YEAR,
//                    'strict_condition' => true,
//                    'type' => Event::TYPE_CUSTOM,
//                    'action' => Event::ACTION_CUSTOM,
//                        ]),
//                'exception' => new EventException([
//                    'frequency' => Event::FREQUENCY_NONE,
//                        ])
//            ]
//        ]);
//
//        if ($model->load(Yii::$app->request->post()) && $model->saveLinked()) {
////            $this->saveEventException($model);
//            return $this->redirect(['view', 'id' => $model->getModel('event')->id]);
//        }
//
//        return $this->render('create', [
//                    'model' => $model,
//        ]);
//    }

    /**
     * Updates an existing Event model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id) {
        $eventModel = $this->findModel($id);

        if (empty($eventModel->eventException)) {
            $eventExceptionModel = new EventException();
            $eventExceptionModel->link('event', $eventModel);
            $eventModel          = $this->findModel($id);
        }
        $model = new LinkedMultiModel([
            'models' => [
                'event'     => $eventModel,
                'exception' => $eventModel->eventException
            ]
        ]);

        if ($model->load(Yii::$app->request->post()) && $model->saveLinked()) {
            return $this->redirect(['view', 'id' => $model->getModel('event')->id]);
        }

        return $this->render('update', [
                    'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Event model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id) {
//        $this->findModel($id)->delete();
        $model         = $this->findModel($id);
        $model->status = $model::STATUS_DELETED;
        $model->save();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Event model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Event the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = Event::find()
                ->where(['id' => $id])
                ->andWhere(['and', 'type="' . Event::TYPE_CUSTOM . '"', 'created_by=' . getMyId()])
                ->one()) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('Ошибка. Запрошенные данные отсутствуют.');
    }

}
