<?php

namespace frontend\controllers;

use Yii;
use frontend\models\ContactForm;
use frontend\models\InviteUserForm;
use yii\web\Controller;
use common\models\Page;
use yii\web\BadRequestHttpException;
use \Swift_TransportException;

/**
 * Site controller
 */
class SiteController extends Controller {

    /**
     * @inheritdoc
     */
    public function actions() {
        return [
            'error'      => [
                'class' => 'yii\web\ErrorAction'
            ],
            'captcha'    => [
                'class'           => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null
            ],
            'set-locale' => [
                'class'   => 'common\actions\SetLocaleAction',
                'locales' => array_keys(Yii::$app->params['availableLocales'])
            ]
        ];
    }

    public function actionIndex() {
//        $this->layout = 'index_main';
        return $this->render('index');
    }

    public function actionContact() {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post())) {
            if ($model->contact(Yii::$app->params['adminEmail'])) {
                Yii::$app->getSession()->setFlash('alert', [
                    'body'    => Yii::t('frontend', 'Thank you for contacting us. We will respond to you as soon as possible.'),
                    'options' => ['class' => 'alert-success']
                ]);
                return $this->refresh();
            }
            else {
                Yii::$app->getSession()->setFlash('alert', [
                    'body'    => \Yii::t('frontend', 'There was an error sending email.'),
                    'options' => ['class' => 'alert-danger']
                ]);
            }
        }

        return $this->render('contact', [
                    'model' => $model
        ]);
    }

    public function actionInviteUser() {
        $this->layout = null;
        $model        = new InviteUserForm();

        if ($model->load(Yii::$app->request->post(), '') && $model->validate()) {
            Yii::warning($model);
            try {
                if ($model->sendEmail()) {
                    //Редирект на страницу отправки, если приглашение отправлено
//                $prevUrl = Url::previous('referrals');
//                $normPrevUrl = Url::to($prevUrl, true);
//                if (!empty($prevUrl) && $normPrevUrl == Yii::$app->request->referrer) {
//                    Yii::$app->session->addFlash('info', "Приглашение вашему другу отправлено на адрес {$model->email}.");
//                    return $this->redirect($prevUrl);
//                }
//                return $this->render('invite_friend_success', ['email' => $model->email]);
                    return "Приглашение отправлено на адрес {$model->email}.";
                }
                else {
                    return "Не удалось отправить приглашение на адрес {$model->email}. Возможно вы указали неверный e-mail.";
                }
            }
            catch (Exception | Swift_TransportException $e) {
//                Yii::error($e);
            }
        }

        $errs = $model->errors;
        if (isset($errs['email'])) {
            $errs_str = implode("<br>", $errs['email']);
        }
        else {
            $errs_str = '';
        }
        return 'Не удалось отправить приглашение. Пожалуйста, повторите попытку позже.<br>' . $errs_str;
    }

}
