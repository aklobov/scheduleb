<?php

namespace frontend\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use common\models\User;
use common\models\UserToken2;
use common\commands\SendEmailCommand;
use Viber\Bot;
use Viber\Client;
use Viber\Api\Sender;
use yii\helpers\Url;
use cheatsheet\Time;

/**
 * Viber webhook
 * https://github.com/Bogdaan/viber-bot-php
 * 
 * 
 */
class ViberController extends Controller {

    protected $apiKey     = '490678c23de7d622-c43d863b8a89cf3d-bf872d8ef2a225b1';
    protected $webhookUrl = 'https://planb.amma.pw/viber';

    public function init() {
        $this->webhookUrl = Url::to('/viber', 'https');
    }

    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        //разрешить гостям
//                        'actions' => ['*'],
                        'allow' => true,
                        'roles' => ['?', '@'],
                    ],
                ],
            ],
//            'verbs'  => [
//                'class'   => VerbFilter::className(),
//                'actions' => [
//                    'index' => ['POST'],
//                    'init' => ['POST'],
//                ],
//            ],
        ];
    }

    public function actionIndex() {
        $botSender = new Sender([
            'name'   => 'PlanB',
            'avatar' => 'https://planb.amma.pw/favicon.png',
        ]);

        try {
            $bot = new Bot(['token' => $this->apiKey]);
            $bot
                    ->onSubscribe(function ($event) use ($bot, $botSender) {
                        $message = sprintf("Добро пожаловать!\n Я - бот сайта %s.\n\n" . $message, Yii::$app->name);
                        $message .= "Я помогу вам пройти регистрацию и получать уведомления о ваших событиях в этот чат.\n\n";
                        $this->getClient()->sendMessage(
                                (new \Viber\Api\Message\Text())
                                ->setSender($botSender)
                                ->setText($message)
                        );
                    })
                    ->onText('/[аА]ктивируй \w+/is', function ($event) use ($bot, $botSender) {
                        // .* - match any symbols (see PCRE)
                        preg_match('/[аА]ктивируй (\w+)\s*/is', $event->getMessage()->getText(), $match);
                        $username = trim($match[1]);
                        $user     = User::findByLogin($username);
//                        Yii::error($event->getSender());
//                        //В ответе не указывается, что e-mail распознан. Но письмо отправляется только на e-mail существующего пользователя.
                        if ($user instanceof User) {
//                        if (($user instanceof User) && $user->can('notifyByMessengers')) {
                            $token   = UserToken2::create(
                                            $user->id, UserToken2::TYPE_VIBER_ACTIVATION, Time::SECONDS_IN_AN_HOUR,
                                    ['viber_id' => $event->getSender()->getId(), 'viber_name' => $event->getSender()->getName()]
                            );
                            Yii::$app->commandBus->handle(new SendEmailCommand([
                                'subject' => 'Активация рассылки через Viber уведомлений сайта ' . Yii::$app->name,
                                'view'    => 'activation_viber',
                                'to'      => $user->email,
                                'params'  => [
                                    'url' => Url::to(['/user/sign-in/viber-activation', 'token' => $token->token], true)
                                ]
                            ]));
                            $message = sprintf('Код активации и инструкции отправлены на email, указанный в профиле пользователя "%s" сайта "%s".', $user->username, Yii::$app->name);
                        }
                        else {
                            $message = 'Я не узнал вас. Проверьте правильность введенного вами имени.';
                        }
                        $bot->getClient()->sendMessage(
                                (new \Viber\Api\Message\Text())
                                ->setSender($botSender)
                                ->setReceiver($event->getSender()->getId())
                                ->setText($message)
                        );
                    })
                    ->onText('/[Уу]дали \w+/is', function ($event) use ($bot, $botSender) {
                        // .* - match any symbols (see PCRE)
                        preg_match('/[уУ]дали (\w+)\s*/is', $event->getMessage()->getText(), $match);
                        $username = trim($match[1]);
                        $user     = User::findByLogin($username);
                        if ($user instanceof User) {
//                        if (($user instanceof User) && $user->can('notifyByMessengers')) {
                            $token   = UserToken2::create(
                                            $user->id, UserToken2::TYPE_VIBER_DEACTIVATION, Time::SECONDS_IN_AN_HOUR
//                                    ['viber_id' => $event->getSender()->getId(), 'viber_name' => $event->getSender()->getName()]
                            );
                            Yii::$app->commandBus->handle(new SendEmailCommand([
                                'subject' => 'Деактивация рассылки через Viber уведомлений сайта ' . Yii::$app->name,
                                'view'    => 'deactivation_viber',
                                'to'      => $user->email,
                                'params'  => [
                                    'url' => Url::to(['/user/sign-in/viber-deactivation', 'token' => $token->token], true)
                                ]
                            ]));
                            $message = sprintf('Код деактивации и инструкции отправлены на email, указанный в профиле пользователя "%s" сайта "%s".', $user->username, Yii::$app->name);
                        }
                        else {
                            $message = 'Я не узнал вас. Проверьте правильность введенного вами имени.';
                        }
                        $bot->getClient()->sendMessage(
                                (new \Viber\Api\Message\Text())
                                ->setSender($botSender)
                                ->setReceiver($event->getSender()->getId())
                                ->setText($message)
                        );
                    })
                    ->onText('/[Пп]омощь/is', function ($event) use ($bot, $botSender) {
                        // .* - match any symbols (see PCRE)
                        $bot->getClient()->sendMessage(
                                (new \Viber\Api\Message\Text())
                                ->setSender($botSender)
                                ->setReceiver($event->getSender()->getId())
                                ->setText("Помощь на подходе! Держитесь!\n\nкоманда: register 11111 - регистрация получателя уведомлений, где 11111 - ваш номер из рабочего кабинета.\n\n команда: помощь - справка по командам бота.")
                        );
                    })
                    ->onText('|.*|s', function ($event) use ($bot, $botSender) {
                        // .* - match any symbols (see PCRE)
                        $bot->getClient()->sendMessage(
                                (new \Viber\Api\Message\Text())
                                ->setSender($botSender)
                                ->setReceiver($event->getSender()->getId())
                                ->setText("Ваше сообщение не распознано!\n Введите слово 'Помощь' для просмотра команд.")
                        );
                    })
                    ->run();
        }
        catch (Exception $e) {
            // todo - log errors
            Yii::error($e);
            echo "Error: " . $e->getError() . "\n";
        }
        return '';
    }

    public function actionInit() {

        try {
            $client = new Client(['token' => $this->apiKey]);
            $result = $client->setWebhook($this->webhookUrl);
            echo "Success!\n";
        }
        catch (Exception $e) {
            echo "Error: " . $e->getError() . "\n";
            Yii::error($e);
        }
    }

    public function actionInit2() {
        $url      = 'https://chatapi.viber.com/pa/set_webhook';
        $jsonData = '{ "auth_token": "' . $this->apiKey . '", "url": "' . $this->webhookUrl . '" }';
        $ch       = curl_init($url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonData);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        $result   = curl_exec($ch);
        curl_close($ch);
        return '';
    }

    public function beforeAction($action) {
//        if ($action->id === 'index') {
        Yii::$app->controller->enableCsrfValidation = false;
//        }
        return parent::beforeAction($action);
    }

}
