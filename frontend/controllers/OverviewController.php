<?php

namespace frontend\controllers;

use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\Org;
use common\models\Event;
use common\models\RelEventOrg;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use cheatsheet\Time;
use yii\db\Query;
use yii\helpers\Html;

/**
 * OverviewController.
 */
class OverviewController extends Controller {

    public $layout = 'adminlte_main';

    /**
     * {@inheritdoc}
     */
    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow'   => TRUE,
                        'actions' => ['index', 'events'],
                        'roles'   => ['@']
                    ],
                ],
            ],
        ];
    }

    /**
     * Show Calendar.
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found or access denied
     */
    public function actionIndex() {
        $reminders = $this->findReminders(getMyId());

        $headers = ArrayHelper::getColumn($reminders, 'title');
        array_unshift($headers, 'Организация');

        $remindersStat            = [];
        $remindersStat[Event::ACTION_REPORT] = array_reduce($reminders, function($res, $item) {
            $res += ($item['type'] == Event::TYPE_DEFAULT) && ($item['action'] == Event::ACTION_REPORT);
            return $res;
        }, 0);
        $remindersStat[Event::ACTION_PAYMENT] = array_reduce($reminders, function($res, $item) {
            $res += ($item['type'] == Event::TYPE_DEFAULT) && ($item['action'] == Event::ACTION_PAYMENT);
            return $res;
        }, 0);
        $remindersStat[Event::ACTION_CUSTOM] = array_reduce($reminders, function($res, $item) {
            $res += ($item['type'] == Event::TYPE_CUSTOM);
            return $res;
        }, 0);
        $remindersStat['total'] = $remindersStat[Event::ACTION_REPORT] + $remindersStat[Event::ACTION_PAYMENT] + $remindersStat[Event::ACTION_CUSTOM];

        return $this->render('index', [
                    'headers' => $headers,
                    'stat'    => $remindersStat,
        ]);
    }

    /**
     * События в заданном диапазон дат
     * @param string $start Формат '2019-01-23'
     * @param string $end Формат '2019-01-24'
     * @return array|null
     */
    public function actionEvents() {

        $orgs = Org::find()
                ->with('reminders', 'reminders.event')
                ->allByUser(getMyId())
                ->all();

        $reminders = $this->findReminders(getMyId());

        $tableData = [];
        foreach ($orgs as $org) {
            $row = [
                Html::a($org->title, ['/org/view', 'id' => $org->id], ['target' => '_blank']),
            ];
            foreach ($reminders as $reminder) {
//                $rmdr  = $org->getReminder($reminder['event_id'])->one();
                $rmdr = NULL;
//                foreach ($org->reminders as $rmdrCurrent) {
//                    if ($rmdrCurrent->event_id == $reminder['event_id']) {
//                        $rmdr = $rmdrCurrent;
//                        break;
//                    }
//                }

                $orgEvents = ArrayHelper::getColumn($org->reminders, 'event_id');
                $indx         = array_search($reminder['event_id'], $orgEvents);

                if ($indx === FALSE) {
                    //Empty or unknown
//                    $row[] = Html::tag('span', 'Не применимо', ['class' => 'badge', 'title' => 'Не применимо']);
                    $row[] = '';
                }
                else {
                    $rmdr = $org->reminders[$indx];
                    switch ($rmdr->status) {
                        case RelEventOrg::STATUS_ACTIVE:
                            if ($reminder['days_till_event'] < 0) {
                                //Просрочено
                                $row[] = Html::a(Html::tag('span', '', ['class' => 'fa fa-hourglass-end']) . ' ' . $reminder['days_till_event'], ['/rel-event-org/view', 'id' => $rmdr->id], ['class' => 'badge label-warning', 'title' => 'Дата события ' . Yii::$app->formatter->asDate($reminder['curr_date']) . '. Просрочено', 'target' => '_blank']);
                            }
                            else {
                                $row[] = Html::a(Html::tag('span', '', ['class' => 'fa fa-hourglass']) . ' ' . $reminder['days_till_event'], ['/rel-event-org/view', 'id' => $rmdr->id], ['class' => 'badge label-success', 'title' => 'Дата события ' . Yii::$app->formatter->asDate($reminder['curr_date']) . '. Активно', 'target' => '_blank']);
//                                RelEventOrg::getAllStatuses($rmdr->status)
                            }
                            break;
                        case RelEventOrg::STATUS_IN_WORK:
                            if ($reminder['days_till_event'] < 0) {
                                //Просрочено
                                $row[] = Html::a(Html::tag('span', '', ['class' => 'fa fa-hourglass-end']) . ' ' . $reminder['days_till_event'], ['/rel-event-org/view', 'id' => $rmdr->id], ['class' => 'badge label-warning', 'title' => 'Дата события ' . Yii::$app->formatter->asDate($reminder['curr_date']) . '. Просрочено', 'target' => '_blank']);
                            }
                            else {
                                $row[] = Html::a(Html::tag('span', '', ['class' => 'fa fa-hourglass-2']) . ' ' . $reminder['days_till_event'], ['/rel-event-org/view', 'id' => $rmdr->id], ['class' => 'badge label-primary', 'title' => 'Дата события ' . Yii::$app->formatter->asDate($reminder['curr_date']) . '. В работе', 'target' => '_blank']);
//                                RelEventOrg::getAllStatuses($rmdr->status)
                            }
                            break;
                        case RelEventOrg::STATUS_FINISHED:
                            $row[] = Html::a(Html::tag('span', '', ['class' => 'fa fa-check']) . ' Сдано', ['/rel-event-org/view', 'id' => $rmdr->id], ['class' => 'badge label-info', 'title' => 'Дата события ' . Yii::$app->formatter->asDate($reminder['curr_date']) . '. Сдано', 'target' => '_blank']);
                            break;
                        case RelEventOrg::STATUS_CANCELED:
                            $row[] = Html::a(Html::tag('span', '', ['class' => 'glyphicon glyphicon-pause']) . ' Пропущено пользователем', ['/rel-event-org/view', 'id' => $rmdr->id], ['class' => 'badge label-info', 'title' => 'Дата события ' . Yii::$app->formatter->asDate($reminder['curr_date']) . '. Пропущено пользователем', 'target' => '_blank']);
                            break;

                        default:
                            //Empty or unknown
                            $row[] = Html::tag('span', 'Не применимо', ['class' => 'badge', 'title' => 'Не применимо']);
                            break;
                    }
                }
//                $row[] = [empty($rmdr) ? Html::tag('span', 'Не применимо', ['class' => 'badge', 'data-value' => 'Не применимо']) : ' (' . $rmdr->status . ')'];
//                $row[] = [empty($rmdr) ? '&nbsp;' : $rmdr->event->daysTillEvent . ' (' . $rmdr->status . ')'];
            }
            $tableData[] = $row;
        }

        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return ['data' => $tableData];
    }

    /**
     * Finds the Calendar model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return RelEventOrg the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findReminders(int $user_id) {
        return Yii::$app->db->createCommand('SELECT ev.id as event_id, ev.title, ev.curr_date, ev.type, ev.action , reminders.id as reminder_id, reminders.status, DATEDIFF(curr_date, CURDATE()) as days_till_event '
                                . 'FROM scheduleb_rel_event_org reminders '
                                . 'LEFT JOIN scheduleb_org org ON org.id = reminders.org_id '
                                . 'LEFT JOIN scheduleb_event ev ON ev.id = reminders.event_id '
                                . 'WHERE org.owner_id = ' . $user_id . ' '
                                . 'GROUP BY ev.id '
                                . 'ORDER BY ev.type ASC, ev.action ASC, ev.curr_date ASC')
                        ->queryAll();
    }

}
