<?php

namespace frontend\controllers;

use Yii;
use common\models\Org;
use frontend\models\search\OrgSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

/**
 * OrgController implements the CRUD actions for Org model.
 */
class OrgController extends Controller {

    public $layout = 'adminlte_main';

    /**
     * {@inheritdoc}
     */
    public function behaviors() {
        return [
            'verbs'  => [
                'class'   => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow'      => true,
                        'actions'    => ['view', 'update', 'delete', 'set-default'],
                        'roleParams' => function($rule) {
                            return [
                                'model'     => Org::findOne(Yii::$app->request->get('id')),
                                'attribute' => 'created_by',
                            ];
                        },
                        'permissions' => ['editOwnModel'],
                    ],
                    [
                        'allow' => TRUE,
                        'actions' => ['index', 'create'],
                        'roles' => ['@']
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all Org models.
     * @return mixed
     */
    public function actionIndex() {
        $searchModel  = new OrgSearch(['owner_id' => getMyId(), 'status' => Org::STATUS_ACTIVE]);
        //TODO Менять searchModel в зависимости от разрешений пользователя. Для менеджера группы - 
        //поиск по группе фирм. Для менеджера сайта - список всех фирм (без возможности изменения данных).
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
                    'searchModel'  => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Org model.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id) {
        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Org model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        $model = new Org();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
                    'model' => $model,
        ]);
    }

    /**
     * Updates an existing Org model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id) {
//        $model = $this->findModel(['id'=>$id, 'owner_id' => Yii::$app->user->id]);
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
                    'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Org model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id) {
        $this->findModel($id)->setDeleted();

        return $this->redirect(['index']);
    }

    /**
     * Устанавливает организацию по умолчанию.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionSetDefault($id) {
        $this->findModel($id)->setDefault();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Org model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Org the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
//        $params = ['id' => $id, ['or', 'owner_id=' . Yii::$app->user->id, 'owner_id=1']];
        if (($model = Org::find()
                ->where(['id' => $id])
                ->andWhere(['owner_id' => getMyId()])
//                ->andWhere(['or', 'owner_id=' . Yii::$app->user->id, 'owner_id=1'])
                ->one()) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('Информация отсутствует.');
    }

}
