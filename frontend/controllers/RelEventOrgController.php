<?php

namespace frontend\controllers;

use Yii;
use common\models\RelEventOrg;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\Org;
use common\models\Event;
use frontend\models\search\OrgSearch;
use frontend\models\search\EventSearch;
use yii\helpers\ArrayHelper;
use common\commands\SendEmailCommand;

/**
 * RelEventOrgController implements the CRUD actions for RelEventOrg model.
 */
class RelEventOrgController extends Controller {

    public $layout = 'adminlte_main';

    /**
     * {@inheritdoc}
     */
    public function behaviors() {
        return [
            'verbs'  => [
                'class'   => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                    'status' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow'         => true,
                        'actions'       => ['index', 'sync'],
                        'matchCallback' => function($rule, $action) {
                            //Доступ разрещен на index без параметров модели
                            if (('index' == $action->id) && empty(Yii::$app->request->get('org_id'))) {
                                return true;
                            }
                            //Доступ разрешен для изменения собственных фирм
                            $params = [
                                'model'     => Org::findOne(Yii::$app->request->get('org_id')),
                                'attribute' => 'created_by',
                            ];
                            if (!empty($params['model']) && Yii::$app->user->can('editOwnModel', $params)) {
                                return true;
                            }
                            return false;
                        },
//                        'permissions' => ['editOwnModel'],
                    ],
                    [
                        'allow'      => true,
                        'actions'    => ['delete', 'update', 'view', 'status'],
                        'roleParams' => function($rule) {
                            $rel = RelEventOrg::findOne(Yii::$app->request->get('id'));
                            return [
                                'model'     => isset($rel->org) ? $rel->org : null,
                                'attribute' => 'created_by',
                            ];
                        },
                        'permissions' => ['editOwnModel'],
                    ],
//                    [
//                        'allow' => TRUE,
//                        'roles' => ['@']
//                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all RelEventOrg models.
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found or access denied
     */
    public function actionIndex($org_id = null) {
        if (empty($org_id)) {
            $searchModel  = new OrgSearch();
            $dataProvider = $searchModel->searchMyActive(Yii::$app->request->queryParams);

            return $this->render('index_nodata', [
                        'searchModel'  => $searchModel,
                        'dataProvider' => $dataProvider,
            ]);
        }

        Yii::$app->user->setReturnUrl(['/rel-event-org/index', 'org_id' => $org_id]);
        //TODO Проверка прав доступа к событиям организации
        //TODO Сортировка и поиск по сязанным полям
        $org_model = Org::find()->where(['id' => $org_id])->oneByUser();
        if (empty($org_model)) {
            throw new NotFoundHttpException('Информация отсутствует.');
        }
//        $searchModel  = $org_model->getEvents();
//        $searchModel  = new RelEventOrgSearch(['org_id' => $org_id]);
        $searchModel  = new EventSearch();
        $dataProvider = $searchModel->searchRelated(Yii::$app->request->queryParams, $org_model);
//        print_r($searchModel);die;

        return $this->render('index', [
                    'org_model'    => $org_model,
                    'searchModel'  => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single RelEventOrg model.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id) {
        Yii::$app->user->setReturnUrl(['rel-event-org/view', 'id' => $id]);
        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
    }

//    public function actionViewTest() {
//        $org_model = Org::findOne($org_id);
//        $events    = Event::find()->applicable($org_model);
//        echo $events->createCommand()->getRawSql();
//        Yii::$app->commandBus->handle(new SendEmailCommand([
//                    'to'      => 'robott@planb.amma.pw',
//                    'subject' => 's privetom',
//                    'view'    => 'inviteTest',
//                    'params'  => [
//                        'inviteUrl'  => 'urt2',
//                        'inviteText' => 'hello new boy',
//                    ]
//        ]));
//    }

    /**
     * Append a new Event to Org.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found or access denied
     */
    public function actionSync($org_id) {
//        $org_model = Org::findOne($org_id);
        $org_model = Org::find()->where(['id' => $org_id])->oneByUser();
        if (empty($org_model)) {
            throw new NotFoundHttpException('Информация отсутствует.');
        }

        //Сохранение выделенных событий
        $res = (int) Yii::$app->request->post('form_changed');
        if ((int) Yii::$app->request->post('form_changed')) {
            $eventIds            = (array) Yii::$app->request->post('selection');
            $allApplicableEvents = array_merge(Event::find()->applicable($org_model)->all(), Event::find()->applicable(null, Event::TYPE_CUSTOM)->all());
//            $allApplicableEvents = Event::find()->applicable($org_model)->andWhere(['action' => Event::ACTION_REPORT])->all();
            $allOrgEventIds      = ArrayHelper::getColumn($org_model->events, function($item) {
                        return $item->id;
                    });
            foreach ($allApplicableEvents as $applicableEvent) {
                if (in_array($applicableEvent->id, $eventIds)) {
                    //Событие выбрано пользователем
                    if (!in_array($applicableEvent->id, $allOrgEventIds)) {
                        //Создаем связь, если события ещё нет у организации
                        $relEventOrg = new RelEventOrg([
                            'org_id'        => $org_id,
                            'event_id'      => $applicableEvent->id,
                            'remind_before' => Yii::$app->user->identity->userProfile->remind_before_default,
                        ]);
                        $relEventOrg->save();
                        //TODO Добавить сообщение при ошибке
                    }
                }
                else {
                    if (in_array($applicableEvent->id, $allOrgEventIds)) {
                        //Удаляем связь, если она есть у организации
                        $relEventOrg = RelEventOrg::find()->where([
                                    'org_id'   => $org_id,
                                    'event_id' => $applicableEvent->id,
                                ])->one()->delete();
                        //TODO Добавить сообщение при ошибке удаления
                    }
                }
            }
            return $this->redirect(['index', 'org_id' => $org_id]);
        }

        $searchModelReports              = new EventSearch();
//        $dataProviderReports = $searchModel->searchApplicable(Yii::$app->request->queryParams, $org_model);
        $dataProviderReports             = $searchModelReports->searchApplicableReports(Yii::$app->request->queryParams, $org_model);
        $dataProviderReports->pagination = false;
        $dataProviderReports->sort       = [
            'defaultOrder' => ['title' => SORT_ASC]
        ];
//        $dataProviderReports->pagination->pageParam = Event::TYPE_DEFAULT . Event::ACTION_REPORT;
//        echo $dataProvider->query->createCommand()->getRawSql();

        $searchModelPayments              = new EventSearch();
        $dataProviderPayments             = $searchModelPayments->searchApplicablePayments(Yii::$app->request->queryParams, $org_model);
        $dataProviderPayments->pagination = false;
        $dataProviderPayments->sort       = [
            'defaultOrder' => ['title' => SORT_ASC]
        ];
//        $dataProviderPayments->pagination->pageParam = Event::TYPE_DEFAULT . Event::ACTION_PAYMENT;

        $searchModelCustoms              = new EventSearch();
        $dataProviderCustoms             = $searchModelCustoms->searchApplicableCustoms(Yii::$app->request->queryParams);
        $dataProviderCustoms->pagination = false;
        $dataProviderCustoms->sort       = [
            'defaultOrder' => ['title' => SORT_ASC]
        ];
//        $dataProviderCustoms->pagination->pageParam = Event::TYPE_CUSTOM . Event::ACTION_CUSTOM;

        return $this->render('sync', [
                    'org_model'            => $org_model,
//                    'searchModel'  => $searchModelReports,
                    'dataProviderReports'  => $dataProviderReports,
                    'dataProviderPayments' => $dataProviderPayments,
                    'dataProviderCustoms'  => $dataProviderCustoms,
                    'activeTab'            => Yii::$app->request->get('tab', Event::ACTION_REPORT),
        ]);
    }

    /**
     * Creates a new custom event.
     * Создать новое пользовательское событие и добавить его организации
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
//    public function actionCreateCustom() {
//        $model = new RelEventOrg();
//
//        if ($model->load(Yii::$app->request->post()) && $model->save()) {
//            return $this->redirect(['view', 'id' => $model->id]);
//        }
//
//        return $this->render('create', [
//                    'model' => $model,
//        ]);
//    }

    /**
     * Updates an existing RelEventOrg model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
                    'model' => $model,
        ]);
    }

    public function actionStatus($id, $status) {
        $model          = $this->findModel($id);
//        $model->status_previous = $model->status;
        $model->status_ = $status;
        $res            = $model->save();

        return $this->goBack('/rel-event-org');
    }

    /**
     * Deletes an existing RelEventOrg model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id) {
        $this->findModel($id)->delete();

        return $this->redirect(['index', 'org_id' => $id]);
    }

    /**
     * Finds the RelEventOrg model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return RelEventOrg the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = RelEventOrg::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('Информация отсутствует.');
    }

}
