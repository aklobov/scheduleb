<?php

/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace frontend\assets;

use yii\bootstrap\BootstrapAsset;
use yii\web\AssetBundle;
use yii\web\YiiAsset;
use common\assets\FontAwesome;
use frontend\assets\FontGilroy;
use frontend\assets\SlickCarouselAsset;
use yii\bootstrap4\BootstrapAsset as Bootstrap4Asset;
use yii\bootstrap4\BootstrapPluginAsset as Bootstrap4PluginAsset;

/**
 * Frontend application asset
 */
class FrontendIndexPageAsset extends AssetBundle {

    /**
     * @var string
     */
    public $sourcePath = '@frontend/web/bundle/site';

    /**
     * @var array
     */
    public $css = [
        'css/style.css',
        'css/resp.css',
    ];

    /**
     * @var array
     */
    public $js = [
        'js/app.js',
    ];

    /**
     * @var array
     */
    public $depends = [
        YiiAsset::class,
//        BootstrapAsset::class,
        Bootstrap4Asset::class,
        Bootstrap4PluginAsset::class,
        SlickCarouselAsset::class,
        FontAwesome::class,
        FontGilroy::class,
        FontGothamPro::class,
    ];

}
