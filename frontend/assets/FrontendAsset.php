<?php

namespace frontend\assets;

use yii\web\AssetBundle;
use yii\bootstrap\BootstrapAsset;
use yii\web\JqueryAsset;
use common\assets\FontAwesome;
use frontend\assets\FontGothamPro;
use yii\bootstrap4\BootstrapAsset as Bootstrap4Asset;

/**
 * Frontend application asset
 */
class FrontendAsset extends AssetBundle {

    /**
     * @var string
     */
    public $sourcePath = '@frontend/web/bundle/site';

    /**
     * @var array
     */
    public $css = [
        'css/style.css',
        'css/resp.css',
    ];

    /**
     * @var array
     */
    public $js = [
//        'js/app.js',
    ];

    /**
     * @var array
     */
    public $depends = [
        Bootstrap4Asset::class,
        JqueryAsset::class,
        FontAwesome::class,
        FontGothamPro::class,
    ];

}
