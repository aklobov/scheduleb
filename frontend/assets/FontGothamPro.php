<?php
/**
 * Created by PhpStorm.
 * User: zein
 * Date: 7/3/14
 * Time: 3:24 PM
 */

namespace frontend\assets;

use yii\web\AssetBundle;

class FontGothamPro extends AssetBundle
{
    public $sourcePath = '@frontend/web/fonts/GothamPro';
    public $css = [
        'stylesheet.css'
    ];
}
