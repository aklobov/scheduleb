<?php

namespace frontend\assets;

use yii\web\AssetBundle;
use yii\bootstrap\BootstrapAsset;
use yii\web\JqueryAsset;

/**
 * Slick Carousel asset
 * @link http://kenwheeler.github.io/slick/ Slick carousel js plugin
 */
class SlickCarouselAsset extends AssetBundle {

    /**
     * @var string
     */
    public $sourcePath = '@frontend/web/bundle/slick-carousel/slick';

    /**
     * @var array
     */
    public $css = [
        'slick.css',
        'slick-theme.css'
    ];

    /**
     * @var array
     */
    public $js = [
        'slick.min.js',
    ];

    /**
     * @var array
     */
    public $depends = [
        JqueryAsset::class,
    ];

}
