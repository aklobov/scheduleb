<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\bootstrap\Tabs;

/* @var $this yii\web\View */
/* @var $form yii\widgets\ActiveForm */
/* @var $model \frontend\modules\user\models\LoginForm */

$this->title = 'Вход';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-login">
    <h1><?php echo Html::encode($this->title) ?></h1>

    <div class="row">
        <div class="col-lg-5">
            <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>
                <?php echo $form->field($model, 'identity') ?>
                <?php echo $form->field($model, 'password')->passwordInput() ?>
                <?php echo $form->field($model, 'rememberMe')->checkbox() ?>
                <div style="color:#999;margin:1em 0">
                    <?php echo Yii::t('frontend', 'Восстановить забытый пароль можно <a href="{link}">здесь</a>', [
                        'link'=>yii\helpers\Url::to(['sign-in/request-password-reset'])
                    ]) ?>
                </div>
                <div class="form-group">
                    <?php echo Html::submitButton('Войти', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
                </div>
                <div class="form-group">
                    <?php echo Html::a(Yii::t('frontend', 'Нет учетной записи? Зарегистрируйся.'), ['signup']) ?>
                </div>
                <h2>Войти с помощью:</h2>
                <div class="form-group">
                    <?php echo yii\authclient\widgets\AuthChoice::widget([
                        'baseAuthUrl' => ['/user/sign-in/oauth']
                    ]) ?>
                </div>
            <?php ActiveForm::end(); ?>
                
            <?php
            echo Tabs::widget([
                'items'=>[
                    [
                        'label' => 'User',
                        'content' => '<p>Пользователь с минимальными правами.</p><p>Логин:<span class="label label-default">user</span><br>Пароль:<span class="label label-default">user</span></p>',
                    ],
                    [
                        'label' => 'User paid',
                        'content' => '<p>Пользователь с платной учётной записью.</p><p>Логин:<span class="label label-default">user_paid</span><br>Пароль:<span class="label label-default">user_paid</span></p>',
                    ],
                    [
                        'label' => 'Manager',
                        'content' => '<p>Руководитель группы пользователей.</p><p>Логин:<span class="label label-default">manager</span><br>Пароль:<span class="label label-default">manager</span></p>',
                    ],
                    [
                        'label' => 'Web manager',
                        'content' => '<p>Управляющий содержимым сайта.</p><p>Логин:<span class="label label-default">web_manager</span><br>Пароль:<span class="label label-default">web_manager</span></p>',
                    ],
                    [
                        'label' => 'Администратор',
                        'content' => '<p>Администратор сайта.</p><p>Логин:<span class="label label-default">planb</span><br>Пароль:<span class="label label-default">planb</span></p>',
                    ],
                ]
            ]);
            ?>
        </div>
    </div>
</div>
