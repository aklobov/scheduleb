<?php

use common\models\UserProfile;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use common\widgets\TabsAdminLte as Tabs;

/* @var $this yii\web\View */
/* @var $model common\models\UserProfile */
/* @var $form yii\bootstrap\ActiveForm */

$this->title    = 'Профиль пользователя';
?>

<div class="user-profile-form">

    <?php $form           = ActiveForm::begin() ?>

    <?php
    $profileContent = $form->field($model, 'picture')->widget(\trntv\filekit\widget\Upload::class, [
        'url' => ['avatar-upload']
    ]);

    $profileContent .= $form->field($model, 'firstname')->textInput(['maxlength' => 255]);

    $profileContent .= $form->field($model, 'middlename')->textInput(['maxlength' => 255]);

    $profileContent .= $form->field($model, 'lastname')->textInput(['maxlength' => 255]);

    $profileContent .= $form->field($model, 'locale')->dropDownlist(Yii::$app->params['availableLocales']);

    $profileContent .= $form->field($model, 'gender')->dropDownlist([
        \common\models\UserProfile::GENDER_FEMALE => 'Женщина',
        \common\models\UserProfile::GENDER_MALE   => 'Мужчина'
    ]);

    $notificationContent = $form->field($model, 'remind_before_default')->textInput()->hint('Напоминание может быть получено не более чем за 15 дней до события. Значение по умолчанию для всех новых событий.');

    if ($model->viber_id) {
        //Ссылка на удаление профиля
        $viberContent = '<span class="label label-success">Уведомления через Viber подключены.</span><p /><p />' . Html::a(Html::img('/img/viber_icon.png', ['height' => '20em']) . '&nbsp;Отключить уведомления через Viber.', 'viber://pa?chatURI=plan_b&context=context_pb&text=удали%20' . $model->user->username, ['class' => 'btn btn-default', 'role' => 'button']);
    }
    else {
        $viberContent = Html::a(Html::img('/img/viber_icon.png', ['height' => '20em']) . '&nbsp;Активировать уведомления через Viber.', 'viber://pa?chatURI=plan_b&context=context_pb&text=активируй%20' . $model->user->username, ['class' => 'btn btn-default', 'role' => 'button']);
    }

    echo Tabs::widget([
        'items' => [
            [
                'label'         => 'Основные',
                'content'       => $profileContent,
                'headerOptions' => [
                    'title' => 'Основные настройки',
                ],
                'active'        => 'profile' == $activeTab,
            ],
            [
                'label'         => 'Уведомления',
                'content'       => $notificationContent,
                'headerOptions' => [
                    'title' => 'Настройки уведомлений',
                ],
                'active'        => 'notification' == $activeTab,
            ],
            [
                'label'         => 'Viber',
                'content'       => $viberContent,
                'headerOptions' => [
                    'title' => 'Настройки Viber',
                ],
                'active'        => 'viber' == $activeTab,
            ],
        ],
    ]);
    ?>

    <div class="form-group">
        <?php echo Html::submitButton('Сохранить', ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end() ?>

</div>
