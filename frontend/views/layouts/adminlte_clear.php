<?php
/**
 * @var $this yii\web\View
 */
?>
<?php $this->beginContent('@frontend/views/layouts/adminlte_common.php'); ?>
    <?php echo $content ?>
<?php $this->endContent(); ?>