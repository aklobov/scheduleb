<?php

use yii\helpers\Html;

/* @var $this \yii\web\View */
/* @var $content string */


if ((Yii::$app->controller->action->id == 'index') && (Yii::$app->controller->id == 'site')) {
    \frontend\assets\FrontendIndexPageAsset::register($this);
}
else {
    \frontend\assets\FrontendAsset::register($this);
}
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?php echo Yii::$app->language ?>">
    <head>
        <meta charset="<?php echo Yii::$app->charset ?>"/>
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title><?php echo Html::encode($this->title) ?></title>
<?php $this->head() ?>
<?php echo Html::csrfMetaTags() ?>
        <link rel="icon" type="image/png" href="/favicon.png" />
        <link rel="apple-touch-icon" href="/favicon.png" />
    </head>
    <body data-spy="scroll" data-target="#navbar-example">
        <?php $this->beginBody() ?>
<?php echo $content ?>
<?php $this->endBody() ?>
    </body>
</html>
<?php $this->endPage() ?>
