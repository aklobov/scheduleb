﻿<?php

use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;

/* @var $this \yii\web\View */
/* @var $content string */

$this->beginContent('@frontend/views/layouts/_clear.php');
$linkPrefix = (Yii::$app->controller->action->id == 'index') && (Yii::$app->controller->id == 'site') ? '' : '/';
?>

<header>
    <nav class="navbar navbar-expand-lg navbar-dark" >
        <div class="container">
            <a class="navbar-brand" href="/"><img src="/img/logo.png" alt=""></a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <div class="header-2-columm-nav ml-auto">
                    <ul class="navbar-nav ml-auto" id="navbar-example">
                        <li class="nav-item">
                            <a class="nav-link" href="<?=$linkPrefix?>#services">Услуги</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="<?=$linkPrefix?>#special">Кто мы</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="<?=$linkPrefix?>#price">Цены</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="<?=$linkPrefix?>#reviews">Отзывы</a>
                        </li>
                    </ul>
                    <?php
                    if (Yii::$app->user->isGuest) {
                        echo '<a href="/user/sign-in/login" class="btn btn-custom btn_submit">Войти</a>';
                    }else{
                        echo '<a href="/overview" class="btn btn-custom btn_submit">В кабинет</a>';
                    }
                    ?>
                </div>
            </div>
        </div>
    </nav>
</header>

<?php echo $content ?>

<footer>
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <a href="/"><img class="footer-img" src="/img/logo-footer.png" alt=""></a>
            </div>
            <div class="col-md-4">
                <div class="foot-menu">
                    <ul>
                        <li><a href="<?=$linkPrefix?>#services">Услуги</a></li>
                        <li><a href="<?=$linkPrefix?>#special">Кто мы</a></li>
                        <li><a href="<?=$linkPrefix?>#price">Цены</a></li>
                        <li><a href="<?=$linkPrefix?>#reviews">Отзывы</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-md-4">
                <div class="foot-contact">
                    <ul>
                        <li><i class="fa fa-phone"></i> + 7 (978) 777-77-77</li>
                    </ul>
                    <ul>
                        <li><i class="fa fa-envelope"></i> mail@gmail.com</li>
                    </ul>
                    <div class="social">
                        <a href="#"><img src="/img/vk.png" alt=""></a>
                        <a href="#"><img src="/img/fb.png" alt=""></a>
                        <a href="#"><img src="/img/ok.png" alt=""></a>
                    </div>
                </div>
            </div>
        </div>
    </div>

</footer>
<div class="copyright"><p><?= date('Y') ?> &laquoПлан Б&raquo. Все права защищены.</p></div>
<?php $this->endContent() ?>