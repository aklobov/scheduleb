<?php

/**
 * @var $this    yii\web\View
 * @var $content string
 */
use backend\assets\BackendAsset;
//use backend\modules\system\models\SystemLog;
use backend\widgets\Menu;
use common\models\TimelineEvent;
use common\models\NotificationLog;
use common\models\Event;
use common\models\RelEventOrg;
use common\models\Org;
use yii\bootstrap\Alert;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\log\Logger;
use yii\widgets\Breadcrumbs;

$bundle = BackendAsset::register($this);
?>

<?php $this->beginContent('@frontend/views/layouts/adminlte_base.php'); ?>

<div class="wrapper">
    <!-- header logo: style can be found in header.less -->
    <header class="main-header">
        <a href="<?php echo Yii::$app->urlManagerFrontend->createAbsoluteUrl('/') ?>" class="logo">
            <!-- Add the class icon to your logo image or logo icon to add the margining -->
            <?php echo Yii::$app->name ?>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top" role="navigation">
            <!-- Sidebar toggle button-->
            <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                <span class="sr-only"><?php echo Yii::t('backend', 'Toggle navigation') ?></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </a>
            <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">
                    <!--                    <li id="payments" class="notifications-menu">
                                            <a href="<?php //echo Url::to(['/event/index'])  ?>">
                                                <i class="fa fa-rub"></i>
                                                <span class="label label-warning">
                    <?php //echo TimelineEvent::find()->today()->count() ?>
                                                </span>
                                            </a>
                                        </li>-->
                    <!-- Notifications: style can be found in dropdown.less -->
                    <li id="timeline-notifications" class="dropdown notifications-menu" title="Последние наступившие события">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="fa fa-bell"></i>
                            <span class="label label-info">
                                <?php $evts   = RelEventOrg::find()->joinWith('event ev')->where(['{{%rel_event_org}}.created_by' => getMyId()])->andWhere('curr_date > :curr_date AND previous_date < :curr_date', [':curr_date' => date('Y-m-d')])->groupBy('event_id');
                                $evCnt  = $evts->count();
                                echo $evCnt; ?>
                            </span>
                        </a>
                        <ul class="dropdown-menu">
                            <li class="header">Последние наступившие события:  <?= $evCnt ?></li>
                            <li>
                                <!-- inner menu: contains the actual data -->
                                <ul class="menu">
<?php foreach ($evts->with('event')->orderBy(['previous_date' => SORT_ASC])->limit(5)->all() as $reo): ?>
                                        <li title="<?= \Yii::$app->formatter->asDate($reo->event->previous_date) . ' ' . $reo->event->title ?>">
                                            <a href="<?php echo Yii::$app->urlManager->createUrl(['/event/view', 'id' => $reo->event->id]); ?>">
                                                <i class="fa fa-info text-red"></i> <?= $reo->event->title ?>
                                            </a>
                                        </li>
<?php endforeach; ?>
                                </ul>
                            </li>
                            <li class="footer">
<?php echo Html::a(sprintf('Всего %s.', \Yii::$app->i18n->messageFormatter->format('{n, plural, one{# событие} few{# различных события} other{# различных событий}}', ['n' => $evCnt], \Yii::$app->language)), '#') ?>
                            </li>
                        </ul>
                    </li>
                    <!-- Notifications: style can be found in dropdown.less -->
                    <li id="timeline-notifications" class="dropdown notifications-menu" title="Подходит срок событий">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="fa fa-bell"></i>
                            <span class="label label-danger">
<?php $evts  = RelEventOrg::find()->comingSoon()->andWhere(['{{%rel_event_org}}.created_by' => getMyId()])->count();
echo $evts; ?>
                            </span>
                        </a>
                        <ul class="dropdown-menu">
                            <li class="header">Подходит срок для <?php echo \Yii::$app->i18n->messageFormatter->format('{n, plural, one{# события} other{# событий}}!', ['n' => $evts], \Yii::$app->language) ?></li>
                            <li>
                                <!-- inner menu: contains the actual data -->
                                <ul class="menu">
<?php $evCnt = 0;
foreach (RelEventOrg::find()->comingSoon()->andWhere(['{{%rel_event_org}}.created_by' => getMyId()])->groupBy('ev.id')->orderBy(['curr_date' => SORT_ASC])->limit(5)->all() as $reo): ?>
                                        <li title="<?= \Yii::$app->formatter->asDate($reo->event->curr_date) . ' ' . $reo->event->title ?>">
                                            <a href="<?php echo Yii::$app->urlManager->createUrl(['/event/view', 'id' => $reo->event->id]);
    $evCnt++; ?>">
                                                <i class="fa fa-warning text-red"></i> <?= $reo->event->title ?>
                                            </a>
                                        </li>
<?php endforeach; ?>
                                </ul>
                            </li>
                            <li class="footer">
<?php echo Html::a(sprintf('Всего %s.', \Yii::$app->i18n->messageFormatter->format('{n, plural, one{# событие} few{# различных события} other{# различных событий}}', ['n' => $evCnt], \Yii::$app->language)), '#') ?>
                            </li>
                        </ul>
                    </li>
                    <!-- User Account: style can be found in dropdown.less -->
                    <li class="dropdown user user-menu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <img src="<?php echo Yii::$app->user->identity->userProfile->getAvatar($this->assetManager->getAssetUrl($bundle, 'img/anonymous.png')) ?>"
                                 class="user-image">
                            <span><?php echo Yii::$app->user->identity->username ?> <i class="caret"></i></span>
                        </a>
                        <ul class="dropdown-menu">
                            <!-- User image -->
                            <li class="user-header light-blue">
                                <img src="<?php echo Yii::$app->user->identity->userProfile->getAvatar($this->assetManager->getAssetUrl($bundle, 'img/anonymous.png')) ?>"
                                     class="img-circle" alt="Фото пользователя"/>
                                <p>
<?php echo Yii::$app->user->identity->username ?>
                                    <small>
                                    <?php printf('На сайте с %s', date('d.m.y', Yii::$app->user->identity->created_at)) ?>
                                    </small>
                            </li>
                            <!-- Menu Footer-->
                            <li class="user-footer">
                                <div class="pull-left">
                                    <?php echo Html::a('Профиль', ['/sign-in/profile'], ['class' => 'btn btn-default btn-flat btn-sm']) ?>
                                </div>
                                <div class="pull-left">
<?php echo Html::a('Учётная запись', ['/sign-in/account'], ['class' => 'btn btn-default btn-flat btn-sm']) ?>
                                </div>
                                <div class="pull-right">
                    <?php echo Html::a('Выход', ['/sign-in/logout'], ['class' => 'btn btn-default btn-flat btn-sm', 'data-method' => 'post']) ?>
                                </div>
                            </li>
                        </ul>
                    </li>
                    <!--<li>-->
<?php //echo Html::a('<i class="fa fa-cogs"></i>', ['/system/settings'])  ?>
                    <!--</li>-->
                </ul>
            </div>
        </nav>
    </header>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
            <!-- Sidebar user panel -->
            <div class="user-panel">
                <div class="pull-left image">
                    <img src="<?php echo Yii::$app->user->identity->userProfile->getAvatar($this->assetManager->getAssetUrl($bundle, 'img/anonymous.png')) ?>"
                         class="img-circle"/>
                </div>
                <div class="pull-left info">
                    <p><?php echo Yii::t('backend', 'Hello, {username}', ['username' => Yii::$app->user->identity->getPublicIdentity()]) ?></p>
                    <a href="<?php echo Url::to(['/sign-in/profile']) ?>">
                        <i class="fa fa-circle text-success"></i>
            <?php echo Yii::$app->formatter->asDatetime(time()) ?>
                    </a>
                </div>
            </div>
            <!-- sidebar menu: : style can be found in sidebar.less -->
            <?php
            echo Menu::widget([
                'options'         => ['class' => 'sidebar-menu'],
                'linkTemplate'    => '<a href="{url}">{icon}<span>{label}</span>{right-icon}{badge}</a>',
                'submenuTemplate' => "\n<ul class=\"treeview-menu\">\n{items}\n</ul>\n",
                'activateParents' => true,
                'items'           => [
                    [
                        'label'  => 'Обзор',
                        'icon'   => '<i class="glyphicon glyphicon-tasks"></i>',
                        'url'    => ['/overview/index'],
                        'active' => (Yii::$app->controller->id == 'overview'),
                    ],
                    [
                        'label'   => 'Организации',
                        'options' => ['class' => 'header'],
                    ],
                    [
                        'label'        => 'Организации',
                        'url'          => '/org',
                        'icon'         => '<i class="fa fa-industry"></i>',
                        'badge'        => Org::find()->where(['owner_id' => getMyId(), 'status' => Org::STATUS_ACTIVE])->count(),
                        'badgeBgClass' => 'label-success',
//                        'options' => ['class' => 'treeview'],
                        'active'       => (Yii::$app->controller->id == 'org'),
//                        'items'   => [
//                            [
//                                'label'  => Yii::t('backend', 'Список'),
//                                'url'    => ['/org'],
//                                'icon'   => '<i class="fa fa-list-ul"></i>',
//                                'active' => (Yii::$app->controller->action->id == 'index') && (Yii::$app->controller->id == 'org'),
//                            ],
//                            [
//                                'label'  => Yii::t('backend', 'Добавить'),
//                                'url'    => ['/org/create'],
//                                'icon'   => '<i class="fa fa-copy"></i>',
//                                'active' => in_array(Yii::$app->controller->action->id, ['create', 'update']) && (Yii::$app->controller->id == 'org'),
//                            ],
//                        ],
                    ],
                    [
                        'label'   => Yii::t('backend', 'Календарь'),
                        'options' => ['class' => 'header'],
                    ],
                    [
                        'label'  => Yii::t('backend', 'Календарь'),
                        'url'    => ['/calendar/index'],
                        'icon'   => '<i class="fa fa-calendar"></i>',
                        'active' => (Yii::$app->controller->id == 'calendar'),
                    ],
                    [
                        'label'   => 'События',
                        'options' => ['class' => 'header'],
                    ],
                    [
                        'label'        => 'Отчеты',
                        'url'          => '/event',
                        'icon'         => '<i class="fa fa-newspaper-o"></i>',
                        'badge'        => Event::find()->where(['type' => Event::TYPE_DEFAULT, 'action' => Event::ACTION_REPORT])->count(),
                        'badgeBgClass' => 'label-success',
//                        'options' => ['class' => 'treeview'],
                        'active'       => (Yii::$app->controller->id == 'event'),
//                        'items'   => [
//                            [
//                                'label'  => 'Список',
//                                'url'    => ['/event'],
//                                'icon'   => '<i class="fa fa-list-ul"></i>',
//                                'active' => (Yii::$app->controller->action->id == 'index') && (Yii::$app->controller->id == 'event'),
//                            ],
//                            [
//                                'label'  => Yii::t('backend', 'Добавить'),
//                                'url'    => ['/event/create'],
//                                'icon'   => '<i class="fa fa-copy"></i>',
//                                'active' => in_array(Yii::$app->controller->action->id, ['create', 'update']) && (Yii::$app->controller->id == 'event'),
//                            ],
//                            [
//                                'label' => 'Recurr',
//                                'url'   => ['/event/recurr'],
//                                'icon'  => '<i class="fa fa-circle-o"></i>',
////                                'active' => in_array(Yii::$app->controller->id, ['carousel', 'carousel-item']),
//                            ],
//                        ],
                    ],
                    [
                        'label'        => Yii::t('backend', 'Платежи'),
                        'url'          => '/payment',
                        'icon'         => '<i class="fa fa-percent"></i>',
                        'badge'        => Event::find()->where(['type' => Event::TYPE_DEFAULT, 'action' => Event::ACTION_PAYMENT])->count(),
                        'badgeBgClass' => 'label-success',
                        'active'       => (Yii::$app->controller->id == 'payment'),
                    ],
                    [
                        'label'        => Yii::t('backend', 'Мои события'),
                        'url'          => '/custom',
                        'icon'         => '<i class="fa  fa-hand-pointer-o"></i>',
                        'badge'        => Event::find()->where(['type' => Event::TYPE_CUSTOM, 'created_by' => getMyId()])->count(),
                        'badgeBgClass' => 'label-success',
                        'active'       => (Yii::$app->controller->id == 'custom'),
                    ],
                    [
                        'label'   => Yii::t('backend', 'Информация'),
                        'options' => ['class' => 'header'],
                    ],
                    [
                        'label'        => 'Лог уведомлений',
                        'icon'         => '<i class="fa fa-bar-chart-o"></i>',
                        'url'          => ['/timeline-event/index'],
                        'badge'        => NotificationLog::find()->lastMonth()->andWhere(['user_id' => getMyId()])->count(),
                        'badgeBgClass' => 'label-success',
                        'active'       => (Yii::$app->controller->id == 'timeline-event'),
                    ],
                ],
            ])
            ?>
        </section>
        <!-- /.sidebar -->
    </aside>

    <!-- Right side column. Contains the navbar and content of the page -->
    <aside class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
            <?php echo $this->title ?>
            <?php if (isset($this->params['subtitle'])): ?>
                    <small><?php echo $this->params['subtitle'] ?></small>
            <?php endif; ?>
            </h1>
            <?php
            echo Breadcrumbs::widget([
                'tag'     => 'ol',
                'options' => ['class' => 'breadcrumb', 'style' => 'padding-top: 25px;'],
                'links'   => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ])
            ?>
        </section>

        <!-- Main content -->
        <section class="content">
            <?php if (Yii::$app->session->hasFlash('alert')): ?>
                <?php
                echo Alert::widget([
                    'body'    => ArrayHelper::getValue(Yii::$app->session->getFlash('alert'), 'body'),
                    'options' => ArrayHelper::getValue(Yii::$app->session->getFlash('alert'), 'options'),
                ])
                ?>
<?php endif; ?>
<?php echo $content ?>
        </section><!-- /.content -->
    </aside><!-- /.right-side -->
</div><!-- ./wrapper -->

<?php $this->endContent(); ?>
