<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
//use yii\bootstrap\Tabs;
use common\widgets\TabsAdminLte as Tabs;
use common\models\Org;
use common\models\Event;
use common\assets\AdminLteDatepickerPluginAsset;

/* @var $this yii\web\View */
/* @var $model common\models\Event */
/* @var $model common\Base\MultiModel */
/* @var $form yii\widgets\ActiveForm */


//TODO Заменить виджет даты на https://github.com/trntv/yii2-datetime-widget
AdminLteDatepickerPluginAsset::register($this);
$can            = Yii::$app->user->can('manageEventDefault', ['model' => $model->getModel('event')]);
?>

<div class="event-form">

    <?php $form           = ActiveForm::begin(); ?>

    <?= $form->field($model->getModel('event'), 'title')->textInput(['maxlength' => 250]) ?>

    <?= Html::hiddenInput('type', $can ? Event::TYPE_DEFAULT : Event::TYPE_CUSTOM); ?>

    <?php //$form->field($model, 'is_default')->dropDownList($can ? [1 => 'Обязательный', 0 => 'Дополнительный',] : [0 => 'Дополнительный',]) ?>

    <?php //$form->field($model, 'type')->dropDownList([ 'default' => 'Default', 'custom' => 'Custom', ], ['prompt' => ''])  ?>

    <?php // $form->field($model, 'tags')->textInput(['maxlength' => 1024]) ?>

    <?= $form->field($model->getModel('event'), 'description')->textarea(['rows' => 3]) ?>

    <h2>Сроки</h2>

    <?php
    $timesContent[] = $form->field($model->getModel('event'), 'start_date_')->textInput([
        'data-provide'        => 'datepicker',
        'data-date-format'    => 'dd.mm.yyyy',
        'data-date-language'  => 'ru',
        'data-date-autoclose' => 'true',
        'data-date-todayBtn'  => 'linked',
        'placeholder'         => date('d.m.Y')
    ]);
    $timesContent[] = $form->field($model->getModel('event'), 'end_date_')->textInput([
        'data-provide'        => 'datepicker',
        'data-date-format'    => 'dd.mm.yyyy',
        'data-date-language'  => 'ru',
        'data-date-autoclose' => 'true',
        'placeholder'         => '31.12.2099'
//        'value' => '31.12.2025'
    ]);
    $timesContent[] = $form->field($model->getModel('event'), 'frequency')->dropDownList(Event::getAllFrequencies());

    $exceptionsContent[] = $form->field($model->getModel('exception'), 'start_date_')->textInput([
        'data-provide'        => 'datepicker',
        'data-date-format'    => 'dd.mm.yyyy',
        'data-date-language'  => 'ru',
        'data-date-autoclose' => 'true',
        'data-date-todayBtn'  => 'linked',
        'placeholder'         => date('d.m.Y')
    ]);
    $exceptionsContent[] = '<div class="form-group field-event-exception_start_date">
<label class="control-label" for="event-exception_start_date">Дата окончания</label>
<p>Дата окончания соответствует дате окончания события.</p>
</div>';
    $exceptionsContent[] = $form->field($model->getModel('exception'), 'frequency')->dropDownList(Event::getAllFrequencies());

    $tabs = Tabs::widget([
                'items' => [
                    [
                        'label'         => 'Сроки',
                        'content'       => implode(' ', $timesContent),
                        'headerOptions' => [
                            'title' => 'Сроки события',
                        ],
                    ],
                    [
                        'label'         => 'Исключения',
                        'content'       => implode(' ', $exceptionsContent),
                        'headerOptions' => [
                            'title' => 'Даты, которые нужно исключить из дат события. Например, ежеквартальный отчёт не нужно сдавать в последнем квартале года.',
                        ],
                    ],
                ],
    ]);
//    echo Html::tag('div', $tabs, ['class' => 'nav-tabs-custom']);
    echo $tabs;
    ?>

    <?php if ($can) { ?>

        <h2>Применимость</h2>

        <div class="panel panel-default">
            <div class="panel-heading">Параметры применимости</div>
            <div class="panel-body">

                <div class="well">
                    <?= $form->field($model->getModel('event'), 'strict_condition')->checkbox() ?>
                    При <mark>строгом соответствии</mark> для применения события необходимо совпадение всех выбранных параметров организации. Иначе событие будет применено к организации, если совпадает любой параметр из выбранных.
                </div>            
                <?= $form->field($model->getModel('event'), 'org_type_')->checkboxList(Org::getAllOrgTypes(), ['unselect' => '']) ?>

                <?= $form->field($model->getModel('event'), 'tax_type_')->checkboxList(Org::getAllTaxTypes(), ['unselect' => '']) ?>

                <?= $form->field($model->getModel('event'), 'hired_labour_')->checkboxList(Org::getAllHiredLabourTypes(), ['unselect' => '']) ?>

                <div class="panel panel-default">
                    <div class="panel-heading">Обязательства по уплате налога</div>
                    <div class="panel-body">

                        <?= $form->field($model->getModel('event'), 'is_vehicles_tax')->checkbox() ?>

                        <?= $form->field($model->getModel('event'), 'is_property_tax')->checkbox() ?>
                    </div>
                </div>

            </div>
        </div>
        
        <h2>Группа</h2>
        
        <?= $form->field($model->getModel('event'), 'group_id')->textInput()->hint('Необязательный параметр') ?>

    <?php } ?>
    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
        <?= Html::a('Отмена', ['index'], ['class' => 'btn btn-primary']) ?>
        <?php //Html::resetButton('Очистить форму', ['class' => 'reset btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
