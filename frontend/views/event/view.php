<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Event */
/* @var $canEditOwnModel boolean */
/* @var $canManageEventDefault boolean */

$this->title                   = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Отчёты', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="event-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?php
        if ($canEditOwnModel || $canManageEventDefault) {
            echo Html::a('Изменить', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']);
            echo '&nbsp;';
            echo Html::a('Удалить', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data'  => [
                    'confirm' => 'Удаленный отчёт нельзя будет добавить к событиям организации. Назначенные ранее отчёты данного типа можно удалить ис списка отчётов организации самостоятельно (при необходимости). Удалить отчёт?',
                    'method'  => 'post',
                ],
            ]);
        }
        ?>
    </p>

    <?=
    DetailView::widget([
        'model'      => $model,
        'attributes' => [
//            'id',
            'title',
//            'action',
//            'handler',
//            'is_default',
            [
                'value' => function($model) {
                    $vars = [
                        'default' => 'Стандартный',
                        'custom'  => 'Пользовательский',
                    ];
                    return $vars[$model->type];
                },
                'format' => 'text',
//                        'attribute' => 'type',
                'label'  => 'Стандартный/Пользовательский',
            ],
            [
                'value' => function($model) {
                    $value = $model::getAllStatuses($model->status);
                    return "<i>{$value}</i>";
                },
                'format'     => 'html',
                'attribute'  => 'status',
            ],
            'start_date:date',
            'curr_date:date',
            'end_date:date',
            [
                'value' => function($model) {
                    $value = $model::getAllFrequencies($model->frequency);
                    return "<i>{$value}</i>";
                },
                'format'     => 'html',
                'attribute'  => 'frequency',
            ],
            [
                'visible' => !empty($model->eventException->start_date),
                'format'  => 'date',
                'value'   => function($model) {
                    return is_null($model->eventException) ? '' : $model->eventException->start_date;
                },
                'label' => 'Исключить даты. Начало отсчёта',
            ],
            [
                'visible' => !empty($model->eventException->start_date),
                'value'   => function($model) {
                    $value = $model::getAllFrequencies($model->eventException->frequency);
                    return "<i>{$value}</i>";
                },
                'format'    => 'html',
                'attribute' => 'frequency',
                'label'     => 'Исключить даты. Период повтора',
            ],
//        [
//            'visible' => !empty($model->eventException->start_date),
//            'value'   => function($model) {
//                return is_null($model->eventException) ? '' : "<i>{$model->eventException->recurrence_pattern}</i>";
//            },
//            'format' => 'html',
////                'attribute' => 'recurrence_pattern',
//            'label'  => 'Формула расчёта времени исключения (Recurrence pattern)',
//        ],
//            'eventException.recurrence_pattern:html:Формула расчёта времени исключения (Recurrence pattern)',
            [
                'label' => 'Даты события (с учётом дат исключений)',
                'value' => function($model) {
                    $dates = array_slice($model->dates, 0, (int) Yii::$app->params['virtualEventLimit']);
                    array_walk($dates, function(&$val) {
//                        return $val->format('d.m.Y');
                                $val = Yii::$app->formatter->asDate($val->getStart());
                            });

                    return implode(', ', $dates);
                }
            ],
            'strict_condition:boolean',
            [
                'visible'   => !empty($model->org_type),
                'attribute' => 'org_type',
                'format'    => 'html',
                'value' => str_replace(',', ',<br>', $model->org_type)
            ],
            [
                'visible'   => !empty($model->tax_type),
                'attribute' => 'tax_type',
                'format'    => 'html',
                'value' => str_replace(',', ',<br>', $model->tax_type)
            ],
            [
                'visible'   => !empty($model->hired_labour),
                'attribute' => 'hired_labour',
                'format'    => 'html',
                'value' => str_replace(',', ',<br>', $model->hired_labour)
            ],
            [
                'visible'   => !empty($model->is_property_tax),
                'attribute' => 'is_property_tax',
                'format'    => 'boolean',
            ],
            [
                'visible'   => !empty($model->is_vehicles_tax),
                'attribute' => 'is_vehicles_tax',
                'format'    => 'boolean',
            ],
            [
                'visible'   => !empty($model->description),
                'attribute' => 'description',
                'format'    => 'ntext',
            ],
//            'description:ntext',
            [
                'visible'   => !is_null($model->created_at) && ($canEditOwnModel || $canManageEventDefault),
                'attribute' => 'created_at',
                'format'    => 'date',
            ],
            [
                'visible'   => !is_null($model->updated_at) && ($canEditOwnModel || $canManageEventDefault),
                'attribute' => 'updated_at',
                'format'    => 'date',
                'label'     => 'Дата последнего изменения',
            ],
//            'status',
//            'created_at:date',
//            'updated_at:date',
//            'created_by',
//            'updated_by',
        ],
    ])
    ?>
</div>
