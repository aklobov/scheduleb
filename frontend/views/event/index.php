<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use common\models\Org;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\search\EventSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title                   = 'Отчеты';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="event-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]);  ?>

    <p>
        <?= Html::a('Добавить отчет', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel'  => $searchModel,
//        'formatter' => ['class' => 'yii\i18n\Formatter','nullDisplay' => '&nbsp;'],
        'columns'      => [
            ['class' => 'yii\grid\SerialColumn'],
            'title',
//            [
//                'class'     => '\common\grid\SQLEnumColumn',
//                'attribute' => 'org_type',
//                'enum'      => Org::getAllOrgTypes()
//            ],
//            [
//                'class'     => '\common\grid\SQLEnumColumn',
//                'attribute' => 'tax_type',
//                'enum'      => Org::getAllTaxTypes()
//            ],
//            [
//                'class'     => '\common\grid\SQLEnumColumn',
//                'attribute' => 'hired_labour',
//                'enum'      => Org::getAllHiredLabourTypes()
//            ],
            [
                'class'     => '\common\grid\EnumColumn',
                'attribute' => 'frequency',
                'enum'      => $searchModel::getAllFrequencies()
            ],
//            'action',
//            'handler',
            'curr_date',
//            'end_date',
            //'description:ntext',
            //'status',
            //'created_at',
            //'updated_at',
            //'created_by',
            //'updated_by',
            [
                'class'    => 'yii\grid\ActionColumn',
                'template' => '{view} {update} {delete}',
                'buttons'  => [
                    'update' => function($url, $model, $key) {
                        $params                = [
                            'model'     => $model,
                            'attribute' => 'created_by'
                        ];
                        $canEditOwnModel       = Yii::$app->user->can('editOwnModel', $params);
                        $canManageEventDefault = Yii::$app->user->can('manageEventDefault', $params);
                        return ($canEditOwnModel || $canManageEventDefault) ? Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, ['title' => 'Редактировать']) : '<span class="glyphicon glyphicon-pencil text-muted"></span>';
                    },
                    'delete'   => function($url, $model, $key) {
                        $params                = [
                            'model'     => $model,
                            'attribute' => 'created_by'
                        ];
                        $canEditOwnModel       = Yii::$app->user->can('editOwnModel', $params);
                        $canManageEventDefault = Yii::$app->user->can('manageEventDefault', $params);
                        return ($canEditOwnModel || $canManageEventDefault) ? Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                                    'title' => 'Удалить',
                                    'data'  => [
                                        'confirm' => 'Удалить отчёт?',
                                        'method'  => 'post',
                                    ],]) : '<span class="glyphicon glyphicon-trash text-muted"></span>';
                    },
                ],
            ],
        ],
    ]);
    ?>
    <?php Pjax::end(); ?>
</div>
