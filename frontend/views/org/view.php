<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\grid\GridView;
//use yii\bootstrap\Tabs;
use common\widgets\TabsAdminLte as Tabs;
use yii\data\ActiveDataProvider;
use common\models\RelEventOrg;
use common\models\Event;
use common\models\Org;

/* @var $this yii\web\View */
/* @var $model common\models\Org */

$this->title                   = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Организации', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="org-view">

    <?php
    if (count($model->events) < 3) {
        ?>
        <div class="jumbotron">
            <div class="container">
                <h1><?= $model->title ?></h1>
                <p>Назначьте вашей организации перечень отчётов, платежей. Мы выберем необходимые, требуемые законодательством.<br>Вы также можете создать свои события и назначить их организациям.<br>А мы с удовольствием напомним вам о всех событиях!</p>
                <p>
                    <?= Html::a('Назначить отчёты', ['rel-event-org/sync', 'org_id' => $model->id, 'tab' => Event::ACTION_REPORT], ['class' => 'btn btn-primary btn-lg', 'role' => 'button']) ?>
                    <?= Html::a('Назначить платежи', ['rel-event-org/sync', 'org_id' => $model->id, 'tab' => Event::ACTION_PAYMENT], ['class' => 'btn btn-primary btn-lg', 'role' => 'button']) ?>
                    <?= Html::a('Назначить свои события', ['rel-event-org/sync', 'org_id' => $model->id, 'tab' => Event::ACTION_CUSTOM], ['class' => 'btn btn-primary btn-lg', 'role' => 'button']) ?>
                </p>
            </div>
        </div>
        <?php
    }
    ?>

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Изменить', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?=
        Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data'  => [
                'confirm' => 'Удалить организацию?',
                'method'  => 'post',
            ],
        ])
        ?>
    </p>

    <?=
    DetailView::widget([
        'model'      => $model,
        'attributes' => [
            'group_id',
            'is_default:boolean',
            'org_type',
            [
                'attribute' => 'tax_type',
                'format'    => 'html',
                'value'     => str_replace(',', ',<br>', $model->tax_type),
            ],
            'description:ntext',
            'hired_labour:text',
            'is_vehicles_tax:boolean',
            'is_property_tax:boolean',
            'created_at:date',
            'updated_at:date',
        ],
    ])
    ?>

    <br /><br />
    <h1>Прикрепленные события</h1>
    <?php
    $reportDataProvider   = new ActiveDataProvider([
        'query' => Event::find()->joinWith('reminders')->where([
            'org_id'            => $model->id,
            '{{%event}}.type'   => Event::TYPE_DEFAULT,
            '{{%event}}.action' => EVENT::ACTION_REPORT
        ])
    ]);
    $paymentsDataProvider = new ActiveDataProvider([
        'query' => Event::find()->joinWith('reminders')->where([
            'org_id'            => $model->id,
            '{{%event}}.type'   => Event::TYPE_DEFAULT,
            '{{%event}}.action' => EVENT::ACTION_PAYMENT
        ])
    ]);
    $customsDataProvider  = new ActiveDataProvider([
        'query' => Event::find()->joinWith('reminders')->where([
            'org_id'            => $model->id,
            '{{%event}}.type'   => Event::TYPE_CUSTOM,
//            '{{%event}}.action' => EVENT::ACTION_PAYMENT
        ])
    ]);


    $eventsContent   = GridView::widget([
                'dataProvider' => $reportDataProvider,
                'layout'       => '{items}{summary}{pager}',
                'columns'      => [
                    ['class' => 'yii\grid\SerialColumn'],
                    'title',
//                    [
//                        'class'     => '\common\grid\SQLEnumColumn',
//                        'attribute' => 'org_type',
//                        'enum'      => Org::getAllOrgTypes()
//                    ],
//                    [
//                        'class'     => '\common\grid\SQLEnumColumn',
//                        'attribute' => 'tax_type',
//                        'enum'      => Org::getAllTaxTypes()
//                    ],
//                    [
//                        'class'     => '\common\grid\SQLEnumColumn',
//                        'attribute' => 'hired_labour',
//                        'enum'      => Org::getAllHiredLabourTypes()
//                    ],
                    [
                        'class'     => '\common\grid\EnumColumn',
                        'attribute' => 'frequency',
                        'enum'      => Event::getAllFrequencies()
                    ],
                    'curr_date',
//                    'end_date',
                ],
    ]);
    $paymentsContent = GridView::widget([
                'dataProvider' => $paymentsDataProvider,
                'layout'       => '{items}{summary}{pager}',
                'columns'      => [
                    ['class' => 'yii\grid\SerialColumn'],
                    'title',
//                    [
//                        'class'     => '\common\grid\SQLEnumColumn',
//                        'attribute' => 'org_type',
//                        'enum'      => Org::getAllOrgTypes()
//                    ],
//                    [
//                        'class'     => '\common\grid\SQLEnumColumn',
//                        'attribute' => 'tax_type',
//                        'enum'      => Org::getAllTaxTypes()
//                    ],
//                    [
//                        'class'     => '\common\grid\SQLEnumColumn',
//                        'attribute' => 'hired_labour',
//                        'enum'      => Org::getAllHiredLabourTypes()
//                    ],
                    [
                        'class'     => '\common\grid\EnumColumn',
                        'attribute' => 'frequency',
                        'enum'      => Event::getAllFrequencies()
                    ],
                    'curr_date',
//                    'end_date',
                ],
    ]);
    $customsContent  = GridView::widget([
                'dataProvider' => $customsDataProvider,
                'layout'       => '{items}{summary}{pager}',
                'columns'      => [
                    ['class' => 'yii\grid\SerialColumn'],
                    'title',
//                    [
//                        'class'     => '\common\grid\SQLEnumColumn',
//                        'attribute' => 'org_type',
//                        'enum'      => Org::getAllOrgTypes()
//                    ],
//                    [
//                        'class'     => '\common\grid\SQLEnumColumn',
//                        'attribute' => 'tax_type',
//                        'enum'      => Org::getAllTaxTypes()
//                    ],
//                    [
//                        'class'     => '\common\grid\SQLEnumColumn',
//                        'attribute' => 'hired_labour',
//                        'enum'      => Org::getAllHiredLabourTypes()
//                    ],
                    [
                        'class'     => '\common\grid\EnumColumn',
                        'attribute' => 'frequency',
                        'enum'      => Event::getAllFrequencies()
                    ],
                    'curr_date',
//                    'end_date',
                ],
    ]);

    $tabs =  Tabs::widget([
        'items' => [
            [
                'label'         => 'Отчёты',
                'content'       => '<br />' .
                Html::a('Перечень отчётов', ['rel-event-org/index', 'org_id' => $model->id, 'EventSearch[action]' => Event::ACTION_REPORT,], ['class' => 'btn btn-primary']) . '<br /><br />' .
                $eventsContent,
                'headerOptions' => [
                    'title' => 'Перечень отчётов',
                ],
            ],
            [
                'label'         => 'Платежи',
                'content'       => '<br />' .
                Html::a('Перечень платежей', ['rel-event-org/index', 'org_id' => $model->id, 'EventSearch[action]' => Event::ACTION_PAYMENT,], ['class' => 'btn btn-primary']) . '<br /><br />' .
                $paymentsContent,
                'headerOptions' => [
                    'title' => 'Перечень платежей',
                ],
            ],
            [
                'label'         => 'Мои события',
                'content'       => '<br />' .
                Html::a('Перечень моих событий', ['rel-event-org/index', 'org_id' => $model->id, 'EventSearch[action]' => Event::ACTION_CUSTOM,], ['class' => 'btn btn-primary']) . '<br /><br />' .
                $customsContent,
                'headerOptions' => [
                    'title' => 'Перечень моих событий',
                ],
            ],
        ],
    ]);
    
//    echo Html::tag('div', $tabs, ['class' => 'nav-tabs-custom']);
    echo $tabs;
    ?>


</div>
