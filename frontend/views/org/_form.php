<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Org */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="org-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => 250]) ?>

    <?php //$form->field($model, 'org_type')->radioList($model::getAllOrgTypes(), ['unselect' => NULL]) ?>
    <?= $form->field($model, 'org_type')->radioList($model::getAllOrgTypes()) ?>

    <?= $form->field($model, 'tax_type_')->checkboxList($model::getAllTaxTypes(), ['unselect' => '']) ?>

    <?= $form->field($model, 'hired_labour')->radioList($model::getAllHiredLabourTypes()) ?>

    <div class="panel panel-default">
        <div class="panel-heading">Обязательства по уплате налога</div>
        <div class="panel-body">

            <?= $form->field($model, 'is_vehicles_tax')->checkbox() ?>

            <?= $form->field($model, 'is_property_tax')->checkbox() ?>
        </div>
    </div>

    <?= $form->field($model, 'description')->textarea(['rows' => 3, 'maxlength' => 250]) ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
        <?= Html::a('Отмена', ['index'], ['class'=>'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
