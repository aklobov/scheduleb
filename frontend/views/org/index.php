<?php

use yii\helpers\Html;
//use yii\helpers\Url;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use common\models\RelEventOrg;
use common\models\Event;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\search\OrgSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title                   = 'Организации';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="org-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]);  ?>

    <p>
        <?= Html::a('Добавить', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
//        'filterModel'  => $searchModel,
        'columns'      => [
            ['class' => 'yii\grid\SerialColumn'],
//            'title',
            [
                'attribute' => 'title',
                'format' => 'html',
                'value' => function($model) {
                    return Html::a($model->title, ['/rel-event-org/index', 'org_id' => $model->id]);
//                    return print_r($model->title, 1);
                },
            ],
//            'owner_id',
//            'group_id',
//            [
//                'class'     => '\common\grid\EnumColumn',
//                'attribute' => 'org_type',
//                'enum'      => $searchModel::getAllOrgTypes()
//            ],
//            [
//                'class'     => '\common\grid\SQLEnumColumn',
//                'attribute' => 'tax_type',
//                'enum'      => $searchModel::getAllTaxTypes()
//            ],
//            [
//                'class'     => '\common\grid\EnumColumn',
//                'attribute' => 'hired_labour',
//                'enum'      => $searchModel::getAllHiredLabourTypes()
//            ],
//            'is_default:boolean',
//            'updated_at:date',
            /*            [
              'label'   => 'Отчеты',
              'content' => function($model) {
              $stat = (new Query())
              ->select(['reo.status', 'count(*) AS cnt'])
              ->from('{{%rel_event_org}} reo')
              ->join('LEFT JOIN', '{{%event}} ev', 'ev.id = reo.event_id')
              ->where([
              'org_id'    => $model->id,
              'ev.action' => Event::ACTION_REPORT,
              ])
              ->groupBy('reo.status')
              //                            ->createCommand()->getRawSql();
              ->all();
              $stat = ArrayHelper::map($stat, 'status', 'cnt');

              //                    $expiredCnt = RelEventOrg::find()->joinWith('event ev')
              //                            ->where(['org_id' => $model->id])
              //                            ->andWhere(['not', ['{{%rel_event_org}}.status' => RelEventOrg::STATUS_FINISHED]])
              //                            ->andWhere(['<=', 'ev.curr_date', date('Y-m-d')])
              //                            ->count();
              //Подходит срок сдачи
              //                    $currentCnt = RelEventOrg::find()->joinWith('event ev')
              //                            ->where(['org_id' => $model->id])
              //                            ->andWhere(['not', ['{{%rel_event_org}}.status' => RelEventOrg::STATUS_FINISHED]])
              //                            ->andWhere(['>', 'curr_date', date('Y-m-d')]) //CURDATE()
              //                            ->andWhere('DATEDIFF(curr_date, CURDATE()) < remind_before')
              $currentCnt = RelEventOrg::find()
              ->joinWith('event ev')
              ->comingSoon()
              ->andWhere([
              'org_id'    => $model->id,
              'ev.action' => Event::ACTION_REPORT,
              ])
              ->andWhere(['>', 'curr_date', date('Y-m-d')]) //CURDATE()
              //                            ->createCommand()->getRawSql();
              ->count();

              return implode('&nbsp;', [
              Html::a(array_key_exists(RelEventOrg::STATUS_FINISHED, $stat) ? $stat[RelEventOrg::STATUS_FINISHED] : 0, ['/rel-event-org', 'org_id' => $model->id, 'tab' => Event::ACTION_REPORT, 'EventSearch[reminder_status]' => RelEventOrg::STATUS_FINISHED], ['class' => 'label label-info', 'title' => 'Сдано', 'target' => '_blank']),
              Html::a((array_key_exists(RelEventOrg::STATUS_ACTIVE, $stat) ? $stat[RelEventOrg::STATUS_ACTIVE]: 0) + (array_key_exists(RelEventOrg::STATUS_IN_WORK, $stat) ? $stat[RelEventOrg::STATUS_IN_WORK] : 0), ['/rel-event-org', 'org_id' => $model->id, 'tab' => Event::ACTION_REPORT, 'EventSearch[reminder_status]' => RelEventOrg::STATUS_NEEDED_FAKE], ['class' => 'label label-success', 'title' => 'Осталось сдать', 'target' => '_blank']),
              Html::a(array_key_exists(RelEventOrg::STATUS_IN_WORK, $stat) ? $stat[RelEventOrg::STATUS_IN_WORK] : 0, ['/rel-event-org', 'org_id' => $model->id, 'tab' => Event::ACTION_REPORT, 'EventSearch[reminder_status]' => RelEventOrg::STATUS_IN_WORK], ['class' => 'label label-primary', 'title' => 'В работе', 'target' => '_blank']),
              Html::a(array_key_exists(RelEventOrg::STATUS_CANCELED, $stat) ? $stat[RelEventOrg::STATUS_CANCELED] : 0, ['/rel-event-org', 'org_id' => $model->id, 'tab' => Event::ACTION_REPORT, 'EventSearch[reminder_status]' => RelEventOrg::STATUS_CANCELED], ['class' => 'label label-warning', 'title' => 'Пропущено по решению пользователя', 'target' => '_blank']),
              Html::a($currentCnt, ['/rel-event-org', 'org_id' => $model->id, 'tab' => Event::ACTION_REPORT, 'EventSearch[reminder_status]' => RelEventOrg::STATUS_SOON_FAKE], ['class' => 'label label-danger', 'title' => 'Подходит срок сдачи', 'target' => '_blank']), //TODO ссылку на список
              //                        Html::a($expiredCnt, ['/rel-event-org', 'org_id' => $model->id], ['class' => 'label label-danger', 'title' => 'Просрочено', 'target' => '_blank']),
              Html::a(array_sum($stat), ['/rel-event-org', 'org_id' => $model->id, 'tab' => Event::ACTION_REPORT], ['class' => 'label label-default', 'title' => 'Всего', 'target' => '_blank']),
              ]) . '<br><br>' . Html::a('Просмотр отчётов', ['rel-event-org/index', 'org_id' => $model->id, 'tab' => Event::ACTION_REPORT], ['class' => 'label label-success']);
              },
              //                'filter' => [
              //                    '1' => 'Всё сдано',
              //                    '3' => 'Есть просроченные',
              //                    '4' => 'Подходит срок',
              //                    '2' => 'Ничего не сдано',
              //                ]
              ],
              [
              'label'   => 'Платежи',
              'content' => function($model) {
              $stat = (new Query())
              ->select(['reo.status', 'count(*) AS cnt'])
              ->from('{{%rel_event_org}} reo')
              ->join('INNER JOIN', '{{%event}} ev', 'ev.id = reo.event_id')
              ->where([
              'org_id'    => $model->id,
              'ev.action' => Event::ACTION_PAYMENT,
              ])
              ->groupBy('reo.status')
              ->all();
              $stat = ArrayHelper::map($stat, 'status', 'cnt');

              $currentCnt = RelEventOrg::find()
              ->joinWith('event ev')
              ->comingSoon()
              ->andWhere([
              'org_id'    => $model->id,
              'ev.action' => Event::ACTION_PAYMENT,
              ])
              ->andWhere(['>', 'curr_date', date('Y-m-d')]) //CURDATE()
              ->count();

              return implode('&nbsp;', [
              Html::a(ArrayHelper::getValue($stat, RelEventOrg::STATUS_FINISHED, 0), ['/rel-event-org', 'org_id' => $model->id, 'tab' => Event::ACTION_PAYMENT, 'EventSearch[reminder_status]' => RelEventOrg::STATUS_FINISHED], ['class' => 'label label-info', 'title' => 'Сдано', 'target' => '_blank']),
              Html::a(ArrayHelper::getValue($stat, RelEventOrg::STATUS_ACTIVE, 0), ['/rel-event-org', 'org_id' => $model->id, 'tab' => Event::ACTION_PAYMENT, 'EventSearch[reminder_status]' => RelEventOrg::STATUS_ACTIVE], ['class' => 'label label-success', 'title' => 'Осталось заплатить', 'target' => '_blank']),
              Html::a(ArrayHelper::getValue($stat, RelEventOrg::STATUS_IN_WORK, 0), ['/rel-event-org', 'org_id' => $model->id, 'tab' => Event::ACTION_PAYMENT, 'EventSearch[reminder_status]' => RelEventOrg::STATUS_IN_WORK], ['class' => 'label label-primary', 'title' => 'В работе', 'target' => '_blank']),
              Html::a(ArrayHelper::getValue($stat, RelEventOrg::STATUS_CANCELED, 0), ['/rel-event-org', 'org_id' => $model->id, 'tab' => Event::ACTION_PAYMENT, 'EventSearch[reminder_status]' => RelEventOrg::STATUS_CANCELED], ['class' => 'label label-warning', 'title' => 'Пропущено по решению пользователя', 'target' => '_blank']),
              Html::a($currentCnt, ['/rel-event-org', 'org_id' => $model->id, 'tab' => Event::ACTION_PAYMENT, 'EventSearch[reminder_status]' => RelEventOrg::STATUS_SOON_FAKE], ['class' => 'label label-danger', 'title' => 'Подходит срок сдачи', 'target' => '_blank']),
              //                                Html::a(rand(1, 10), ['create'], ['class' => 'label label-danger', 'title' => 'Просрочено']),
              Html::a(array_sum($stat), ['/rel-event-org', 'org_id' => $model->id, 'tab' => Event::ACTION_PAYMENT], ['class' => 'label label-default', 'title' => 'Всего', 'target' => '_blank']),
              ]) . '<br><br>' . Html::a('Просмотр платежей', ['rel-event-org/index', 'org_id' => $model->id, 'tab' => Event::ACTION_PAYMENT], ['class' => 'label label-success']);
              },
              ],
             */
            [
                'class'    => 'yii\grid\ActionColumn',
                'template' => '{events} {share} {set_default}',
                'buttons'  => [
//                    'share' => function($url, $model, $key) {
//                        //Ссылка для расшаривания организации руководителю группы
//                        return Html::a('<span class="glyphicon glyphicon-share"></span>', '#', ['title' => 'Ссылка для предоставления доступа к организации руководителю']);
//                    },
                    'set_default' => function($url, $model, $key) {
                        return Html::a('<span class="glyphicon glyphicon-flag text-info"></span>', ['/org/set-default', 'id' => $key], ['title' => 'Сделать организацией по умолчанию']);
                    },
                    'events'   => function($url, $model, $key) {
                        return Html::a('<span class="glyphicon glyphicon-paperclip text-success"></span>', ['/rel-event-org', 'org_id' => $key], ['title' => 'Cобытия, связанные с организацией']);
                    },
                ],
                'header'       => 'Действия',
            ],
            [
                'class'  => 'yii\grid\ActionColumn',
                'header' => 'Управление',
            ],
        ],
    ]);
    ?>
    <?php Pjax::end(); ?>
</div>
