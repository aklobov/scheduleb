<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\DetailView;
use yii\widgets\Pjax;
use common\models\Event;
use common\models\RelEventOrg;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\RelEventOrgSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $org_model common\models\Org */

$this->title = 'События организации "' . Html::encode($org_model->title) . '"';
$this->params['breadcrumbs'][] = ['label' => 'Организации', 'url' => ['org/index']];
$this->params['breadcrumbs'][] = ['label' => Html::encode($org_model->title), 'url' => ['org/view', 'id' => $org_model->id]];
$this->params['breadcrumbs'][] = 'События организации';
?>
<div class="rel-event-org-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Управление событиями', ['sync', 'org_id' => $org_model->id], ['class' => 'btn btn-success']) ?>
    </p>

    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
//            'id',
//            'org_id',
//            'event_id',
//            'event.title',
//            'title',
            [
                'attribute' => 'title',
                'format' => 'html',
                'value' => function($model) {
                    return Html::a($model->title, ['/event/view', 'id' => $model->id], ['target' => '_blank']);
//                    return print_r($model->title, 1);
                },
            ],
            [
                'class' => '\common\grid\SQLEnumColumn',
                'attribute' => 'action',
                'value' => function($model){
                    $action = Event::ACTION_CUSTOM;
                    
                    if($model->type == $model::TYPE_DEFAULT) {
                        $action = $model->action;
                    }
                    return $action;
                },
                'enum' => Event::getAllActions() + [Event::ACTION_CUSTOM => 'Мои события'],
            ],
//            [
//                'class' => '\common\grid\SQLEnumColumn',
//                'attribute' => 'org_type',
//                'enum' => $org_model::getAllOrgTypes()
//            ],
//            [
//                'class' => '\common\grid\SQLEnumColumn',
//                'attribute' => 'tax_type',
//                'enum' => $org_model::getAllTaxTypes()
//            ],
//            [
//                'class' => '\common\grid\SQLEnumColumn',
//                'attribute' => 'hired_labour',
//                'enum' => $org_model::getAllHiredLabourTypes()
//            ],
//            'is_vehicles_tax:boolean',
//            'is_property_tax:boolean',
            [
                'class' => '\common\grid\EnumColumn',
                'attribute' => 'frequency',
                'enum' => Event::getAllFrequencies(),
            ],
            [
                'class' => '\common\grid\SQLEnumColumn',
                'format' => 'raw',
                'label' => 'Состояние',
                'attribute' => 'reminder_status',
                'value' => function($model) use($org_model) {
                    $reminder = $model->getOrgReminder($org_model->id)->one();
                    $actions = [];
                    if ($reminder->status != RelEventOrg::STATUS_ACTIVE) {
                        $actions[] = Html::a('<span class="glyphicon glyphicon-step-backward text-success"></span>', ['status', 'id' => $reminder->id, 'status' => RelEventOrg::STATUS_ACTIVE], [
                                    'title' => 'Сбросить отметку о состоянии события',
                                    'data' => [
                                        'confirm' => 'Сбросить отметку о состоянии события к начальному состоянию?',
                                        'method' => 'post',
                                    ]
                        ]);
                    } else {
                        $actions[] = '<span class="glyphicon glyphicon-step-backward text-muted" title="Отметка о событии соброшена в начальное состояние"></span>';
                    }
                    if ($reminder->status != RelEventOrg::STATUS_IN_WORK) {
                        $actions[] = Html::a('<span class="glyphicon glyphicon-play text-success"></span>', ['status', 'id' => $reminder->id, 'status' => RelEventOrg::STATUS_IN_WORK], [
                                    'title' => 'Поставить отметку "В работе"',
                                    'data' => [
                                        'confirm' => 'Пометить событие как находящееся в работе?',
                                        'method' => 'post',
                                    ]
                        ]);
                    } else {
                        $actions[] = '<span class="glyphicon glyphicon-play text-muted" title=\'Событие имеет отметку "В работе"\'></span>';
                    }
                    if ($reminder->status != RelEventOrg::STATUS_CANCELED) {
                        $actions[] = Html::a('<span class="glyphicon glyphicon-pause text-warning"></span>', ['status', 'id' => $reminder->id, 'status' => RelEventOrg::STATUS_CANCELED], [
                                    'title' => 'Пропустить одно ближайшее событие', 'data' => [
                                        'confirm' => 'Поставить отметку о пропуске события?',
                                        'method' => 'post',
                                    ]
                        ]);
                    } else {
                        $actions[] = '<span class="glyphicon glyphicon-pause text-muted" title=\'Событие имеет отметку "Пропустить"\'></span>';
                    }
                    if ($reminder->status != RelEventOrg::STATUS_FINISHED) {
                        $actions[] = Html::a('<span class="glyphicon glyphicon-step-forward text-success"></span>', ['status', 'id' => $reminder->id, 'status' => RelEventOrg::STATUS_FINISHED], [
                                    'title' => 'Поставить отметку о завершении работы над событием', 'data' => [
                                        'confirm' => 'Отметить событие как завершенное?',
                                        'method' => 'post',
                                    ]
                        ]);
                    } else {
                        $actions[] = '<span class="glyphicon glyphicon-step-forward text-muted" title=\'Событие имеет отметку "Завершено"\'></span>';
                    }
                    switch ($reminder->status) {
                        case RelEventOrg::STATUS_ACTIVE:
                            $tagClass = 'label label-success';
                            break;
                        case RelEventOrg::STATUS_IN_WORK:
                            $tagClass = 'label label-primary';
                            break;
                        case RelEventOrg::STATUS_CANCELED:
                            $tagClass = 'label label-warning';
                            break;
                        case RelEventOrg::STATUS_FINISHED:
                        default:
                            $tagClass = 'label label-info';
                            break;
                    }
                    return Html::tag('span', RelEventOrg::getAllStatuses($reminder->status), ['class' => $tagClass]) .
                            '<br><br>' . implode('&nbsp;', $actions);
                },
                'enum' => RelEventOrg::getAllStatuses() + [
            RelEventOrg::STATUS_SOON_FAKE => 'Подходит срок сдачи',
            RelEventOrg::STATUS_NEEDED_FAKE => 'Осталось сдать',
            RelEventOrg::STATUS_EXPIRED_FAKE => 'Просрочено',
                ],
            ],
//            [
//                'value' => function($model)use($org_model) {
//                    return $model->getOrgReminder($org_model->id)->one()->remind_date;
//                },
//                'label'  => 'Дата напоминания',
//                'format' => 'date',
//            ],
//            'curr_date:date:Дата события',
//            'end_date:date',
//            'show_before',
            [
                'label' => 'Даты события',
                'format' => 'html',
                'value' => function($model)use($org_model) {
                    $reminder = $model->getOrgReminder($org_model->id)->one();
                    $spanOptions = [
                        'class' => 'label label-primary',
                    ];
//                    return sprintf('Напоминание: %s<br>Начало: %s<br>Окончание: %s', Html::tag('span', Yii::$app->formatter->asDate($reminder->remind_date), $spanOptions), Html::tag('span', Yii::$app->formatter->asDate($reminder->event->curr_date), $spanOptions), Html::tag('span', Yii::$app->formatter->asDate($reminder->event->end_date), $spanOptions));
                    return sprintf('Напоминание: %s<br>Событие: %s', Html::tag('span', Yii::$app->formatter->asDate($reminder->remind_date), $spanOptions), Html::tag('span', Yii::$app->formatter->asDate($reminder->event->curr_date), $spanOptions));
                },
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'header' => 'Действия',
//                'template' => '{view} {update}<br>{status_restore} {status_finished} {status_canceled} {delete}',
                'template' => '{view} {update}&nbsp;&nbsp;&nbsp;{delete}',
                'buttons' => [
                    'view' => function($url, $model, $key) use($org_model) {
                        return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', Url::to(['view', 'id' => $model->getOrgReminder($org_model->id)->one()->id]), ['title' => 'Просмотр']);
                    },
                    'update' => function($url, $model, $key) use($org_model) {
                        return Html::a('<span class="glyphicon glyphicon-pencil"></span>', Url::to(['update', 'id' => $model->getOrgReminder($org_model->id)->one()->id]), ['title' => 'Редактировать']);
                    },
                    'delete' => function($url, $model, $key) use($org_model) {
                        return Html::a('<span class="glyphicon glyphicon-trash text-danger"></span>', Url::to(['delete', 'id' => $model->getOrgReminder($org_model->id)->one()->id]), ['title' => 'Удалить', 'data' => [
                                        'confirm' => 'Удалить событие из списка событий организации?',
                                        'method' => 'post',
                        ]]);
                    },
//                    'status_finished' => function($url, $model, $key) use($org_model) {
//                        if ($model->getOrgReminder($org_model->id)->one()->status != RelEventOrg::STATUS_FINISHED) {
//                            return Html::a('<span class="glyphicon glyphicon-ok text-success"></span>', Url::to(['status', 'id' => $model->getOrgReminder($org_model->id)->one()->id, 'status' => RelEventOrg::STATUS_FINISHED]), ['title' => 'Завершить событие', 'data'  => [
//                                            'confirm' => 'Отметить событие как завершенное?',
//                                            'method'  => 'post',
//                            ]]);
//                        }
//                    },
//                    'status_canceled' => function($url, $model, $key) use($org_model) {
//                        if ($model->getOrgReminder($org_model->id)->one()->status != RelEventOrg::STATUS_CANCELED) {
//                            return Html::a('<span class="glyphicon glyphicon-pause text-warning"></span>', Url::to(['status', 'id' => $model->getOrgReminder($org_model->id)->one()->id, 'status' => RelEventOrg::STATUS_CANCELED]), ['title' => 'Отменить одно ближайшее событие', 'data'  => [
//                                            'confirm' => 'Отменить одно ближайшее событие? Последующие события не будут отменены.',
//                                            'method'  => 'post',
//                            ]]);
//                        }
//                    },
//                    'status_restore' => function($url, $model, $key) use($org_model) {
//                        if ($model->getOrgReminder($org_model->id)->one()->status != RelEventOrg::STATUS_ACTIVE) {
//                            return Html::a('<span class="glyphicon glyphicon-play text-success"></span>', Url::to(['status', 'id' => $model->getOrgReminder($org_model->id)->one()->id, 'status' => RelEventOrg::STATUS_ACTIVE]), ['title' => 'Восстановить напоминание о ближайшем событии', 'data'  => [
//                                            'confirm' => 'Восстановить напоминание о ближайшем событии?',
//                                            'method'  => 'post',
//                            ]]);
//                        }
//                    },
                ],
            ],
        ],
    ]);
    ?>
    <?php Pjax::end(); ?>

    <br /><br />

    <h1>Параметры организации "<?= $org_model->title ?>"</h1>

    <?= Html::a('Управление параметрами организации', ['org/view', 'id' => $org_model->id], ['class' => 'btn btn-primary']) ?>
    <br /><br />

    <?=
    DetailView::widget([
        'model' => $org_model,
        'attributes' => [
//            'group_id',
//            'is_default:boolean',
            'org_type',
            [
                'attribute' => 'tax_type',
                'format' => 'html',
                'value' => str_replace(',', ',<br>', $org_model->tax_type),
            ],
            'hired_labour:text',
            'is_vehicles_tax:boolean',
            'is_property_tax:boolean',
            'description:ntext',
        ],
    ])
    ?>
</div>
