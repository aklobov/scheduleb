<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;
//use yii\bootstrap\Tabs;
use common\widgets\TabsAdminLte as Tabs;

//yii\web\YiiAsset::register($this);


/* @var $this yii\web\View */
/* @var $model common\models\RelEventOrg */

//\Yii::$app->session->setFlash('alert', [
//    'options' => ['class' => 'alert-success'],
//    'body'    => "Логин не существует!"
//]);

$this->title                   = 'Отчёт "' . Html::encode($model->event->title) . '" организации "' . Html::encode($model->org->title) . '"';
$this->params['subtitle']      = 'Напоминание';
$this->params['breadcrumbs'][] = ['label' => 'Организации', 'url' => ['org/index']];
$this->params['breadcrumbs'][] = ['label' => Html::encode($model->org->title), 'url' => ['org/view', 'id' => $model->org->id]];
//$this->params['breadcrumbs'][] = ['label' => 'Отчёты', 'url' => ['event/view', 'id' => $model->event->id]];
$this->params['breadcrumbs'][] = ['label' => 'Напоминания', 'url' => ['rel-event-org/index', 'org_id' => $model->org->id]];
$this->params['breadcrumbs'][] = $model->event->title;
?>
<?= Html::csrfMetaTags() ?>
<div class="rel-event-org-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Изменить', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?=
        Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data'  => [
                'confirm' => 'Удалить напоминание?',
                'method'  => 'post',
            ],
        ])
        ?>
    </p>

    <?php
    $tab1Content                   = DetailView::widget([
                'model'      => $model,
                'attributes' => [
                    'org.title:text:Организация',
//            'event.title:text:Отчёт',
//            'event.start_date:date',
//            'event.frequency',
                    'remind_before',
                    'remind_date:date',
                    'event.curr_date:date',
                    [
                        'value' => function($model) {
                            return "<i>{$model::getAllStatuses($model->status)}</i>";
                        },
                        'format'    => 'html',
                        'label'     => 'Метка состояния',
                        'attribute' => 'status',
                    ],
                    [
                        'format' => 'raw',
                        'label'  => 'Действия над меткой состояния',
                        'value'  => function($model) {
                            $actions = [];
                            if ($model->status != $model::STATUS_ACTIVE) {
                                $actions[] = Html::a(
                                                '<span class="glyphicon glyphicon-step-backward"></span>&nbsp;Сбросить', ['/rel-event-org/status', 'id' => $model->id, 'status' => $model::STATUS_ACTIVE], [
                                            'title' => 'Сбросить отметку о состоянии события',
                                            'class' => 'btn btn-success',
                                            'data'  => [
                                                'method'  => 'post',
                                                'confirm' => 'Сбросить отметку о состоянии события к начальному состоянию?',
                                                'params'  => [
                                                    'method' => 'post',
                                                ]
                                            ]
                                ]);
                            }
                            else {
                                $actions[] = Html::tag('span', Html::tag('span', '<span class="glyphicon glyphicon-step-backward"></span>&nbsp;Сбросить', [
                                                    'title' => 'Отметка о событии соброшена в начальное состояние',
                                                    'class' => 'btn btn-success disabled',
                                                ]), ['class' => 'disabled']);
                            }
                            if ($model->status != $model::STATUS_IN_WORK) {
                                $actions[] = Html::a(
                                                '<span class="glyphicon glyphicon-play"></span>&nbsp;В работе', ['/rel-event-org/status', 'id' => $model->id, 'status' => $model::STATUS_IN_WORK], [
                                            'title' => 'Поставить отметку "В работе"',
                                            'class' => 'btn btn-primary',
                                            'data'  => [
                                                'method'  => 'post',
                                                'confirm' => 'Пометить событие как находящееся в работе?',
                                                'params'  => [
                                                    'method' => 'post',
                                                ]
                                            ]
                                ]);
                            }
                            else {
                                $actions[] = Html::tag('span', Html::tag('span', '<span class="glyphicon glyphicon-play"></span>&nbsp;В работе', [
                                                    'title' => 'Событие имеет отметку "В работе"',
                                                    'class' => 'btn btn-primary disabled',
                                                ]), ['class' => 'disabled']);
                            }
                            if ($model->status != $model::STATUS_CANCELED) {
                                $actions[] = Html::a('<span class="glyphicon glyphicon-pause"></span>&nbsp;Пропустить', Url::to(['/rel-event-org/status', 'id' => $model->id, 'status' => $model::STATUS_CANCELED]), [
                                            'title' => 'Пропустить одно ближайшее событие',
                                            'class' => 'btn btn-warning',
                                            'data'  => [
                                                'confirm' => 'Поставить отметку о пропуске события?',
                                                'method'  => 'post',
                                ]]);
                            }
                            else {
                                $actions[] = Html::tag('span', Html::tag('span', '<span class="glyphicon glyphicon-pause"></span>&nbsp;Пропустить', [
                                                    'title' => 'Событие имеет отметку "Пропустить"',
                                                    'class' => 'btn btn-warning disabled',
                                                ]), ['class' => 'disabled']);
                            }
                            if ($model->status != $model::STATUS_FINISHED) {
                                $actions[] = Html::a('<span class="glyphicon glyphicon-step-forward"></span>&nbsp;Завершить', Url::to(['/rel-event-org/status', 'id' => $model->id, 'status' => $model::STATUS_FINISHED]), [
                                            'title' => 'Поставить отметку о завершении работы над событием',
                                            'class' => 'btn btn-info',
                                            'data'  => [
                                                'confirm' => 'Отметить событие как завершенное?',
                                                'method'  => 'post',
                                ]]);
                            }
                            else {
                                $actions[] = Html::tag('span', Html::tag('span', '<span class="glyphicon glyphicon-step-forward"></span>&nbsp;Завершить', [
                                                    'title' => 'Событие имеет отметку "Завершено"',
                                                    'class' => 'btn btn-info disabled',
                                                ]), ['class' => 'disabled']);
                            }
                            return implode('&nbsp;&nbsp;', $actions);
                        }
                    ],
                    [
                        'visible'   => !empty($model->description),
                        'attribute' => 'description',
                        'format'    => 'ntext',
                    ],
                ],
    ]);

    $eventModel            = $model->event;
    $params                = [
        'model'     => $eventModel,
        'attribute' => 'created_by'
    ];
    $canEditOwnModel       = Yii::$app->user->can('editOwnModel', $params);
    $canManageEventDefault = Yii::$app->user->can('manageEventDefault', $params);

    $tab2Content = DetailView::widget([
                'model'      => $eventModel,
                'attributes' => [
                    [
                        'value' => function($model) {
                            $vars = [
                                'default' => 'Стандартный',
                                'custom'  => 'Пользовательский',
                            ];
                            return $vars[$model->type];
                        },
                        'format' => 'text',
//                        'attribute' => 'type',
                        'label'  => 'Стандартный/Пользовательский',
                    ],
                    [
                        'visible'   => !empty($eventModel->description),
                        'attribute' => 'description',
                        'format'    => 'ntext',
                    ],
                    [
                        'visible'   => !is_null($eventModel->created_at) && ($canEditOwnModel || $canManageEventDefault),
                        'attribute' => 'created_at',
                        'format'    => 'date',
                    ],
                    [
                        'visible'   => !is_null($eventModel->updated_at) && ($canEditOwnModel || $canManageEventDefault),
                        'attribute' => 'updated_at',
                        'format'    => 'date',
                        'label'     => 'Дата последнего изменения',
                    ],
                ],
    ]);
    $tab3Content = DetailView::widget([
                'model'      => $eventModel,
                'attributes' => [
                    'start_date:date',
                    'end_date:date',
                    [
                        'value' => function($model) {
                            $values = $model::getAllFrequencies();
                            return "<i>{$values[$model->frequency]}</i>";
                        },
                        'format'    => 'html',
                        'attribute' => 'frequency',
                    ],
                    [
                        'visible' => !empty($eventModel->eventException->start_date),
                        'format'  => 'date',
                        'value'   => function($model) {
                            return is_null($model->eventException) ? '' : $model->eventException->start_date;
                        },
                        'label'      => 'Исключить даты. Начало отсчёта',
                    ],
                    [
                        'visible' => !empty($eventModel->eventException->start_date),
                        'value'   => function($model) {
                            $values = $model::getAllFrequencies();
                            return "<i>{$values[$model->eventException->frequency]}</i>";
                        },
                        'format'     => 'html',
                        'attribute'  => 'frequency',
                        'label'      => 'Исключить даты. Период повтора',
                    ],
                    [
                        'label' => 'Даты события (с учётом дат исключений)',
                        'value' => function($model) {
                            $dates = $model->dates;
                            array_walk($dates, function(&$val) {
                                        $val = Yii::$app->formatter->asDate($val->getStart());
                                    });

                            return implode(', ', $dates);
                        }
                    ],
                ],
    ]);
    $tab4Content = DetailView::widget([
                'model'      => $eventModel,
                'attributes' => [
                    'strict_condition:boolean',
                    [
                        'visible'   => !empty($eventModel->org_type),
                        'attribute' => 'org_type',
                        'format'    => 'html',
                        'value'     => str_replace(',', ',<br>', $eventModel->org_type)
                    ],
                    [
                        'visible'   => !empty($eventModel->tax_type),
                        'attribute' => 'tax_type',
                        'format'    => 'html',
                        'value'     => str_replace(',', ',<br>', $eventModel->tax_type)
                    ],
                    [
                        'visible'   => !empty($eventModel->hired_labour),
                        'attribute' => 'hired_labour',
                        'format'    => 'html',
                        'value'     => str_replace(',', ',<br>', $eventModel->hired_labour)
                    ],
                    [
                        'visible'   => !empty($eventModel->is_vehicles_tax),
                        'attribute' => 'is_vehicles_tax',
                        'format'    => 'boolean',
                    ],
                    [
                        'visible'   => !empty($eventModel->is_property_tax),
                        'attribute' => 'is_property_tax',
                        'format'    => 'boolean',
                    ],
                ],
    ]);

    $tabs = Tabs::widget([
                'items' => [
                    [
                        'label'         => 'Параметры напоминания',
                        'content'       => '<br />' . $tab1Content,
                        'headerOptions' => [
                            'title' => 'Параметры отчёта, связанные с организацией "' . $model->org->title . '"',
                        ],
                    ],
                    [
                        'label'         => 'Описание',
                        'content'       => '<br />' . $tab2Content,
                        'headerOptions' => [
                            'title' => 'Описание отчёта',
                        ],
                    ],
                    [
                        'label'         => 'Даты',
                        'content'       => '<br />' . $tab3Content,
                        'headerOptions' => [
                            'title' => 'Даты',
                        ],
                    ],
                    [
                        'label'         => 'Условия',
                        'content'       => '<br />' . $tab4Content,
                        'headerOptions' => [
                            'title' => 'Условия применимости отчёта',
                        ],
                    ],
                ],
    ]);

//    echo Html::tag('div', $tabs, ['class' => 'nav-tabs-custom']);
    echo $tabs;
    ?>

</div>
