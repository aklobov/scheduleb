<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\RelEventOrg */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="rel-event-org-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'remind_before')->textInput()->hint('Напоминание может быть получено не более чем за 15 дней до события') ?>

    <?= $form->field($model, 'status')->dropDownList($model::getAllStatuses()) ?>

    <?= $form->field($model, 'description')->textarea(['rows' => 3]) ?>
    
    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
        <?= Html::a('Отмена', ['index', 'org_id' => $model->org->id], ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
