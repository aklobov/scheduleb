<?php

use yii\helpers\Html;
use yii\grid\GridView;
//use yii\grid\ActionColumn;
use common\widgets\TabsAdminLte as Tabs;
use yii\grid\CheckboxColumn;
use yii\widgets\DetailView;
use yii\widgets\Pjax;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use common\models\RelEventOrg;
use common\models\Event;

/* @var $this yii\web\View */
/* @var $searchModelReports frontend\models\search\EventSearch */
/* @var $dataProviderReports yii\data\ActiveDataProvider */
/* @var $dataProviderPayments yii\data\ActiveDataProvider */
/* @var $dataProviderCustoms yii\data\ActiveDataProvider */
/* @var $org_model common\models\Org */

$this->title = 'Управление событиями организации "' . $org_model->title . '"';
$this->params['breadcrumbs'][] = ['label' => 'Организации', 'url' => ['org/index']];
$this->params['breadcrumbs'][] = ['label' => Html::encode($org_model->title), 'url' => ['org/view', 'id' => $org_model->id]];
$this->params['breadcrumbs'][] = ['label' => 'События', 'url' => ['rel-event-org/index', 'org_id' => $org_model->id]];
$this->params['breadcrumbs'][] = 'Управление событиями организации';


$checked = ArrayHelper::toArray($org_model->reminders, [
            RelEventOrg::class => [
                'id',
                'event_id'
            ]
        ]);
$checked = ArrayHelper::map($checked, 'event_id', 'id');
?>
<div class="event-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <div class="jumbotron jumbotron-fluid alert-primary">
        <div class="container">
            <h1 class="display-4">Назначение связанных событий</h1>
            <p class="lead">Отметьте события, связанные с организацией "<?= Html::encode($org_model->title) ?>".</p>
        </div>
    </div>

    <?php
    $form = ActiveForm::begin([
//        'method' => 'post',
                'action' => ['sync', 'org_id' => $org_model->id],
    ]);
    ?>

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]);   ?>


    <?php
    $reportsContent = GridView::widget([
                'dataProvider' => $dataProviderReports,
//        'filterModel'  => $searchModel,
                'layout' => '{items}{summary}{pager}',
                'columns' => [
//            ['class' => 'yii\grid\SerialColumn'],
                    [
                        'class' => CheckboxColumn::class,
                        'checkboxOptions' => function($model, $key, $index, $column) use ($checked) {
                            $params = [
                                'class' => 'rel_event_control',
                                'checked' => array_key_exists($key, $checked),
                            ];
                            if (!empty(trim($model->group_id))) {
                                $params['data-group'] = $model->group_id;
                            }
                            return $params;
                        }
                    ],
                    'title',
                    [
                        'class' => '\common\grid\SQLEnumColumn',
                        'attribute' => 'org_type',
                        'enum' => $org_model::getAllOrgTypes()
                    ],
                    [
                        'class' => '\common\grid\SQLEnumColumn',
                        'attribute' => 'tax_type',
                        'enum' => $org_model::getAllTaxTypes()
                    ],
                    [
                        'class' => '\common\grid\SQLEnumColumn',
                        'attribute' => 'hired_labour',
                        'enum' => $org_model::getAllHiredLabourTypes()
                    ],
                    [
                        'class' => '\common\grid\SQLEnumColumn',
                        'attribute' => 'frequency',
                        'enum' => Event::getAllFrequencies(),
                    ],
                    'is_vehicles_tax:boolean',
                    'is_property_tax:boolean',
                    'curr_date:date',
//                    'end_date:date',
//            [
//                'label'   => 'Добавлено',
//                'content' => function() {
//                    return '<span class="glyphicon glyphicon-ok"></span>';
//                },
//                'filter'       => [
//                    '1' => '<span class="glyphicon glyphicon-ok"></span>Включено',
//                    '0' => '<span class="glyphicon glyphicon-minus"></span>Не включено',
////                    '4' => 'Подходит срок',
////                    '2' => 'Ничего не сдано',
//                ]
//            ],
//            [
//                'class'    => ActionColumn::class,
//                'template' => '{sync} {delete}',
//                'buttons'  => [
//                    'sync' => function($url, $model, $key) {
//                        return Html::a('<span class="glyphicon glyphicon-plus"></span>', '#', ['title' => 'Ссылка для предоставления доступа к организации руководителю']);
//                    },
//                    'delete'   => function($url, $model, $key) {
//                        return Html::a('<span class="glyphicon glyphicon-minus"></span>', ['/org/set-default', 'id' => $key], ['title' => 'Сделать организацией по умолчанию']);
//                    },
//                ],
//            ],
                ],
    ]);

    $paymentsContent = GridView::widget([
                'dataProvider' => $dataProviderPayments,
//        'filterModel'  => $searchModel,
                'layout' => '{items}{summary}{pager}',
                'columns' => [
//            ['class' => 'yii\grid\SerialColumn'],
                    [
                        'class' => CheckboxColumn::class,
                        'checkboxOptions' => function($model, $key, $index, $column) use ($checked) {
                            $params = [
                                'class' => 'rel_event_control',
                                'checked' => array_key_exists($key, $checked),
                            ];
                            if (!empty(trim($model->group_id))) {
                                $params['data-group'] = $model->group_id;
                            }
                            return $params;
                        }
                    ],
                    'title',
                    [
                        'class' => '\common\grid\SQLEnumColumn',
                        'attribute' => 'org_type',
                        'enum' => $org_model::getAllOrgTypes()
                    ],
                    [
                        'class' => '\common\grid\SQLEnumColumn',
                        'attribute' => 'tax_type',
                        'enum' => $org_model::getAllTaxTypes()
                    ],
                    [
                        'class' => '\common\grid\SQLEnumColumn',
                        'attribute' => 'hired_labour',
                        'enum' => $org_model::getAllHiredLabourTypes()
                    ],
                    [
                        'class' => '\common\grid\SQLEnumColumn',
                        'attribute' => 'frequency',
                        'enum' => Event::getAllFrequencies(),
                    ],
                    'is_vehicles_tax:boolean',
                    'is_property_tax:boolean',
                    'curr_date:date',
//                    'end_date:date',
                ],
    ]);
    $customsContent = GridView::widget([
                'dataProvider' => $dataProviderCustoms,
//        'filterModel'  => $searchModel,
                'layout' => '{items}{summary}{pager}',
                'columns' => [
//            ['class' => 'yii\grid\SerialColumn'],
                    [
                        'class' => CheckboxColumn::class,
                        'checkboxOptions' => function($model, $key, $index, $column) use ($checked) {
                            $params = [
                                'class' => 'rel_event_control',
                                'checked' => array_key_exists($key, $checked),
                            ];
                            if (!empty(trim($model->group_id))) {
                                $params['data-group'] = $model->group_id;
                            }
                            return $params;
                        }
                    ],
                    'title',
                    [
                        'class' => '\common\grid\SQLEnumColumn',
                        'attribute' => 'action',
                        'enum' => Event::getAllActions()
                    ],
                    [
                        'class' => '\common\grid\SQLEnumColumn',
                        'attribute' => 'frequency',
                        'enum' => Event::getAllFrequencies(),
                    ],
                    'curr_date:date',
//                    'end_date:date',
                ],
    ]);

    $tabs = Tabs::widget([
                'items' => [
                    [
                        'label' => 'Отчёты',
                        'content' => $reportsContent,
                        'headerOptions' => [
                            'title' => 'Перечень отчётов',
                        ],
                        'active' => Event::ACTION_REPORT == $activeTab,
                    ],
                    [
                        'label' => 'Платежи',
                        'content' => $paymentsContent,
                        'headerOptions' => [
                            'title' => 'Перечень платежей',
                        ],
                        'active' => Event::ACTION_PAYMENT == $activeTab,
                    ],
                    [
                        'label' => 'Мои события',
                        'content' => $customsContent,
                        'headerOptions' => [
                            'title' => 'Перечень моих событий',
                        ],
                        'active' => Event::ACTION_CUSTOM == $activeTab,
                    ],
                ],
    ]);
    echo $tabs;
    ?>
    <?php Pjax::end(); ?>

    <br />
    <div class="form-group">
        <?= Html::hiddenInput('form_changed', 0, ['id' => 'form_changed']); ?>
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success', 'name' => 'sync']) ?>
        <?= Html::a('Отмена', ['index', 'org_id' => $org_model->id], ['class' => 'btn btn-primary']) ?>
        <?php // Html::submitInput('Исключить выбранные', ['class' => 'btn btn-success', 'name' => 'remove'])  ?>
    </div>

    <?php ActiveForm::end(); ?>


    <br /><br />

    <h1>Параметры организации "<?= $org_model->title ?>"</h1>

    <?=
    DetailView::widget([
        'model' => $org_model,
        'attributes' => [
//            'group_id',
//            'is_default:boolean',
            'org_type',
            [
                'attribute' => 'tax_type',
                'format' => 'html',
                'value' => str_replace(',', ',<br>', $org_model->tax_type),
            ],
            'hired_labour:text',
            'is_vehicles_tax:boolean',
            'is_property_tax:boolean',
            'description:ntext',
        ],
    ])
    ?>
</div>


<script>
    document.addEventListener("DOMContentLoaded", function (event) {
        toggledClass = 'alert-info';

        //Установка начальных значений
        $('.rel_event_control').each(function (i, item) {
            if (this.checked) {
                $(this).parent().parent().addClass(toggledClass);
            } else {
                $(this).parent().parent().removeClass(toggledClass);
            }
        });

        //Выбор всех
        $('.select-on-check-all').click(function (e) {
//            var keys = $("#w0").yiiGridView("getSelectedRows");
            if (this.checked) {
                $("div.tab-pane.active tbody>tr").addClass(toggledClass);
            } else {
                $("div.tab-pane.active tbody>tr").removeClass(toggledClass);
            }
            $('.rel_event_control').change();
            $('input#form_changed').val(1);
        });

        //Выбор строки
        $('.rel_event_control').change(function (e) {
//            var keys = $("#w0").yiiGridView("getSelectedRows");
            if (this.checked) {
                $(this).parent().parent().addClass(toggledClass);
            } else {
                $(this).parent().parent().removeClass(toggledClass);
            }
            $('input#form_changed').val(1);
//   var checked = $(this).is(':checked');
//   $.ajax('route/target', {data: {id: $(this).closest('tr').data('key'), checked: checked}});   
        });

        //Выбор строки и уведомление о группе похожих событий
        $('.rel_event_control').change(function (e) {
            if ($(this).is('[data-group]')) {
                group = $(this).attr('data-group').trim()
                $('#group-list').html('');
                $('input[data-group='.concat(group, ']')).each(function () {
                    $('#group-list').append('<li>'.concat($(this).parent().next('td').text(), '</li>'));
//                    console.log($(this).parent().next('td').text());
                });
                $('#modal-primary').modal('show');
            }
        });

    });
</script>

<div class="modal modal-primary fade" id="modal-primary">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">x</span></button>
                <h4 class="modal-title">Связанные события</h4>
            </div>
            <div class="modal-body">
                <p>Вы выбрали/исключили событие из группы:</p>
                <ol id="group-list"></ol>
                <p>Проверьте правильность Вашего выбора!</p>
            </div>
            <div class="modal-footer">
                <!--<button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>-->
                <button type="button" class="btn btn-outline" data-dismiss="modal">Понятно</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<div class="modal fade" id="myModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Modal title</h4>
            </div>
            <div class="modal-body">
                <p>One fine body&hellip;</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save changes</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->