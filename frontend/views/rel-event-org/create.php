<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\RelEventOrg */

$this->title = 'Create Rel Event Org';
$this->params['breadcrumbs'][] = ['label' => 'Rel Event Orgs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="rel-event-org-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
