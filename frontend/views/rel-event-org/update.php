<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\RelEventOrg */

$this->title                   = 'Изменить "' . $model->event->title . '"';
$this->params['breadcrumbs'][] = ['label' => 'Организации', 'url' => ['org/index']];
$this->params['breadcrumbs'][] = ['label' => Html::encode($model->org->title), 'url' => ['org/view', 'id' => $model->org->id]];
$this->params['breadcrumbs'][] = ['label' => 'Отчёты', 'url' => ['rel-event-org/index', 'org_id' => $model->org->id]];
$this->params['breadcrumbs'][] = ['label' => $model->event->title, 'url' => ['rel-event-org/view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Изменить';
?>
<div class="rel-event-org-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
