<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\RelEventOrgSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title                   = 'Cвязанные события';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="rel-event-org-index">

    <div class="jumbotron jumbotron-fluid alert-warning">
        <div class="container">
            <h1 class="display-4">Нет связанных событий</h1>
            <p class="lead">Выберите организацию из списка ниже для просмотра связанных с ней событий.</p>
        </div>
    </div>

    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel'  => $searchModel,
        'columns'      => [
            ['class' => 'yii\grid\SerialColumn'],
            'title',
            [
                'class'       => 'yii\grid\ActionColumn',
                'template'    => '{view}',
                'buttons'     => [
                    'view' => function($url, $model, $key) {
                        //Ссылка для расшаривания организации руководителю группы
                        return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', ['/rel-event-org', 'org_id' => $key], ['title' => 'Смотреть связанные события']);
                    },
                ],
            ],
        ],
    ]);
    ?>

</div>
