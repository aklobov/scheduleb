<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use common\models\Org;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\search\EventSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title                   = 'Мои события';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="event-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]);  ?>

    <p>
        <?= Html::a('Добавить отчёт', ['/event/create'], ['class' => 'btn btn-success']) ?> &nbsp;
        <?= Html::a('Добавить платеж', ['/payment/create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel'  => $searchModel,
//        'formatter' => ['class' => 'yii\i18n\Formatter','nullDisplay' => '&nbsp;'],
        'columns'      => [
            ['class' => 'yii\grid\SerialColumn'],
            'title',
//            [
//                'class'     => '\common\grid\EnumColumn',
//                'attribute' => 'action',
//                'enum'      => $searchModel::getAllActions()
//            ],
            [
                'class'     => '\common\grid\EnumColumn',
                'attribute' => 'frequency',
                'enum'      => $searchModel::getAllFrequencies()
            ],
            'curr_date',
//            'status',
            [
                'class'    => 'yii\grid\ActionColumn',
                'template' => '{view} {update} {delete}',
                'buttons'  => [
                    'update' => function($url, $model, $key) {
                        $params                = [
                            'model'     => $model,
                            'attribute' => 'created_by'
                        ];
                        $canEditOwnModel       = Yii::$app->user->can('editOwnModel', $params);
                        $canManageEventDefault = Yii::$app->user->can('manageEventDefault', $params);
                        return ($canEditOwnModel || $canManageEventDefault) ? Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, ['title' => 'Редактировать']) : '<span class="glyphicon glyphicon-pencil text-muted"></span>';
                    },
                    'delete'   => function($url, $model, $key) {
                        $params                = [
                            'model'     => $model,
                            'attribute' => 'created_by'
                        ];
                        $canEditOwnModel       = Yii::$app->user->can('editOwnModel', $params);
                        $canManageEventDefault = Yii::$app->user->can('manageEventDefault', $params);
                        return ($canEditOwnModel || $canManageEventDefault) ? Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                                    'title' => 'Удалить',
                                    'data'  => [
                                        'confirm' => sprintf('Удалить %s?', $model::getAllActions($model->action)),
                                        'method'  => 'post',
                                    ],]) : '<span class="glyphicon glyphicon-trash text-muted"></span>';
                    },
                ],
            ],
        ],
    ]);
    ?>
    <?php Pjax::end(); ?>
</div>
