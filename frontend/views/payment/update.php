<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Event */

$this->title = 'Изменить данные платежа: ' . $model->getModel('event')->title;
$this->params['breadcrumbs'][] = ['label' => 'Платежи', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->getModel('event')->title, 'url' => ['view', 'id' => $model->getModel('event')->id]];
$this->params['breadcrumbs'][] = 'Изменить';
?>
<div class="event-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
