<?php

use yii\helpers\Html;
//use yii\grid\GridView;
use common\models\Event;
use common\assets\DataTablesPluginAsset;
use common\assets\DataTablesPluginsAsset;

DataTablesPluginAsset::register($this);
DataTablesPluginsAsset::register($this);

/* @var $this yii\web\View */
/* @var $headers array */

//https://www.datatables.net

$reports = [];
if ($stat[Event::ACTION_REPORT] > 0) {
    if ($stat[Event::ACTION_REPORT] < $stat['total']) {
        $reports['show'] = implode(',', range(1, $stat[Event::ACTION_REPORT]));
        $reports['hide'] = implode(',', range($stat[Event::ACTION_REPORT] + 1, $stat['total']));
    }
    else {
        $reports['show'] = implode(',', range(1, $stat['total']));
        $reports['hide'] = '';
    }
}
else {
    $reports['show'] = '';
    $reports['hide'] = implode(',', range(1, $stat['total']));
}

$payments = [];
if ($stat[Event::ACTION_PAYMENT] > 0) {
    if ($stat[Event::ACTION_REPORT] + $stat[Event::ACTION_PAYMENT] < $stat['total']) {
        $payments['show'] = implode(',', range($stat[Event::ACTION_REPORT] + 1, $stat[Event::ACTION_REPORT] + $stat[Event::ACTION_PAYMENT]));
        $payments['hide'] = $reports['show'] . ',' . implode(',', range($stat[Event::ACTION_REPORT] + $stat[Event::ACTION_PAYMENT] + 1, $stat['total']));
    }
    else {
        $payments['show'] = implode(',', range($stat[Event::ACTION_REPORT] + 1, $stat['total']));
        $payments['hide'] = $reports['show'];
    }
}
else {
    $payments['show'] = '';
    $payments['hide'] = implode(',', range(1, $stat['total']));
}

$customs = [];
if ($stat[Event::ACTION_CUSTOM] > 0) {
    $customs['show'] = implode(',', range($stat['total'] - $stat[Event::ACTION_CUSTOM] + 1, $stat['total']));
    $customs['hide'] = $reports['show'] . ',' . $payments['show'];
}
else {
    $customs['show'] = '';
    $customs['hide'] = implode(',', range(1, $stat['total']));
}


//$payments         = [];
//$start            = $stat['reports'] + 1;
//$end              = $start + $stat['payments'] - 1;
//$payments['show'] = implode(',', range($start, $end));
//$payments['hide'] = implode(',', range($end + 1, $stat['total'] + 1)) . ',' . $reports['show'];

//if ($stat[Event::TYPE_CUSTOM]) {
//    $customs         = [];
//    $start           = $stat['reports'] + $stat['payments'] + 1;
//    $end             = $start + $stat[Event::ACTION_CUSTOM] - 1;
//    $customs['show'] = implode(',', range($start, $end));
//    $customs['hide'] = implode(',', range($end + 1, $stat['total'] + 1)) . ', ' . $reports['show'] . ', ' . $payments['show'];
//}
//else {
//    $customs['show'] = '';
//    $customs['hide'] = implode(',', range(1, $stat['total']));
//}

//$payments['hide']

$this->title                   = 'Обзор';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="overview-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <div id='btns'></div>
    <table id='overviewTable' class='display cell-border' style="width:100%">
        <thead>
            <tr>
                <?php
                foreach ($headers as $header) {
                    printf('<th>%s</th>', $header);
                }
                ?>
            </tr>
        </thead>
    </table>
</div>

<script>
    document.addEventListener("DOMContentLoaded", function (event) {
        $('#overviewTable').DataTable({
            ajax: '/overview/events',
            "scrollX": true,
//            "scrollY": "300",
//            scrollCollapse: true,
            fixedColumns: 1,
            fixedHeader: {
                header: true,
//                footer: false
            },
            'language': {
                "processing": "Подождите...",
                "search": "Поиск:",
                "lengthMenu": "Показать _MENU_ записей",
                "info": "Записи с _START_ до _END_ из _TOTAL_ записей",
                "infoEmpty": "Записи с 0 до 0 из 0 записей",
                "infoFiltered": "(отфильтровано из _MAX_ записей)",
                "infoPostFix": "",
                "loadingRecords": "Загрузка записей...",
                "zeroRecords": "Записи отсутствуют.",
                "emptyTable": "В таблице отсутствуют данные",
                "paginate": {
                    "first": "Первая",
                    "previous": "Предыдущая",
                    "next": "Следующая",
                    "last": "Последняя"
                },
                "aria": {
                    "sortAscending": ": активировать для сортировки столбца по возрастанию",
                    "sortDescending": ": активировать для сортировки столбца по убыванию"
                },
                "buttons": {
                    copy: 'Текст',
                    copySuccess: {
                        1: "Одна строка скопирована в буфер обмена",
                        _: "%d строк(и) скопировано в буфер обмена"
                    },
                    copyTitle: 'Копировать в буфер обмена',
                    copyKeys: 'Нажмите <i>ctrl</i> или <i>\u2318</i> + <i>C</i> чтобы скопировать информацию<br>в буфер обмена.<br><br>Для отмены щёлкните это сообщение или нажмите клавишу escape.'
                }
            },
            buttons: [
//                'copyHtml5', 
//                'csvHtml5',
//                {
//                    text: 'Копировать Текст',
//                    extend: 'copyHtml5',
//                    fieldSeparator: '\t'
//                },
                {
                    extend: 'colvisGroup',
                    text: 'Отчёты <span class="badge"><?=$stat[Event::ACTION_REPORT]?></span>',
                    show: [0, <?= $reports['show'] ?>],
                    hide: [<?= $reports['hide'] ?>]
                },
                {
                    extend: 'colvisGroup',
                    text: 'Платежи <span class="badge"><?=$stat[Event::ACTION_PAYMENT]?></span>',
                    show: [0, <?= $payments['show'] ?>],
                    hide: [<?= $payments['hide'] ?>]
                },
                {
                    extend: 'colvisGroup',
                    text: 'Мои события <span class="badge"><?=$stat[Event::ACTION_CUSTOM]?></span>',
                    show: [0, <?= $customs['show'] ?>],
                    hide: [<?= $customs['hide'] ?>]
                },
                {
                    extend: 'colvisGroup',
                    text: 'Всё',
                    show: ':hidden'
                }
            ],
            dom: '<"row"<"col-sm-4"l><"col-sm-5"B><"col-sm-3"f>>rtip',
//            dom: 'lBfrtip'
//            dom: 'lfrtip'
            "columnDefs": [
                {"className": "dt-center", "targets": "_all"
                }
            ],
        });

//        $('a.toggle-vis').on('click', function (e) {
//            e.preventDefault();
//
//            // Get the column API object
//            var column = table.column($(this).attr('data-column'));
//
//            // Toggle the visibility
//            column.visible(!column.visible());
//        });
    });
</script>

<style>
    th.dt-center, td.dt-center { text-align: center; }
</style>