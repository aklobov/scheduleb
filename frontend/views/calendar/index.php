<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use common\models\Org;
use common\assets\FullCalendarPluginAsset;

FullCalendarPluginAsset::register($this);

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\search\EventSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

//https://fullcalendar.io/docs/intro

$this->title                   = 'Календарь';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="calendar-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <div id='calendar'></div>
</div>

<script>
    document.addEventListener("DOMContentLoaded", function (event) {

        // page is now ready, initialize the calendar...

        $('#calendar').fullCalendar({
            // put your options and callbacks here
            locale: 'ru',
            editable: false,
//            navLinks: true, // can click day/week names to navigate views
            eventLimit: true, // allow "more" link when too many events
            defaultView: 'month',
            aspectRatio: 2,
            header: {
                left: 'prev,today,next',
                center: 'title',
                right: 'month,listYear,listMonth,listWeek'
            },
            views: {
                listWeek: {buttonText: 'Расписание на неделю'},
                listMonth: {buttonText: 'Расписание на месяц'},
                listYear: {buttonText: 'Расписание на год'},
                month: {buttonText: 'Календарь'}
            },
            events: {
                // you can also specify a plain string like 'json/events.json'
                // https://fullcalendar.io/docs/event-data
                url: '/calendar/events',
                error: function () {
                    $('#script-warning').show();
                },
                success: function () {
                    $('#script-warning').hide();
                },
                loading: function (bool) {
                    $('#loading').toggle(bool);
                }
            },
            eventRender: function (event, element) {
                element.attr('data-toogle', 'tooltip');
                element.attr('title', event.description);
            },
            eventClick: function (event) {
                if (event.url) {
                    window.open(event.url);
                    return false;
                }
            },
            validRange: {
                start: '<?= date('Y-m-d', strtotime('first day of previous year'))?>',
//                end: '2025-12-31'
            }
        })

    });
</script>

<div id='script-warning'>
    Ошибка загрузки данных. Повторите запрос позже.
</div>

<div id='loading'>Загрузка данных...</div>

<style>
    #script-warning {
        display: none;
        background: #eee;
        border-bottom: 1px solid #ddd;
        padding: 0 10px;
        line-height: 40px;
        text-align: center;
        font-weight: bold;
        font-size: 12px;
        color: red;
    }

    #calendar {
        /*max-width: 900px;*/
        margin: 10px auto;
    }

    #loading {
        display: none;
        position: absolute;
        top: 10px;
        right: 10px;
    }
</style>