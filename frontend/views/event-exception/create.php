<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\EventException */

$this->title = 'Create Event Exception';
$this->params['breadcrumbs'][] = ['label' => 'Event Exceptions', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="event-exception-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
