<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\EventException */

$this->title = 'Update Event Exception: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Event Exceptions', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="event-exception-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
