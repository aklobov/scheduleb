<?php

use yii\helpers\Url;

/**
 * @var $model common\models\NotificationLog
 */
?>
<i class="fa fa-bell bg-blue"></i>
<div class="timeline-item">
    <span class="time" title="<?= Yii::$app->formatter->asDate($model->created_at) ?>">
        <i class="fa fa-clock-o"></i>
        <?php echo Yii::$app->formatter->asRelativeTime($model->created_at) ?>
    </span>
    <h3 class="timeline-header">Напоминание</h3>

    <div class="timeline-body">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <a href="<?= Url::to(['rel-event-org/view', 'id' => $model->reminder_id]) ?>" target="_blank">
                        <div class="info-box bg-blue">
                            <span class="info-box-icon"><i class="fa fa-bell"></i></span>

                            <div class="info-box-content">
                                <span class="info-box-text" title="Организация"><?= $model->org->title ?></span>
                                <span class="info-box-number" title="Отчёт"><?= $model->event->title ?></span>

                                <div class="progress">
                                    <div class="progress-bar" style="width: 10%"></div>
                                </div>
                                <span class="progress-description" title="Дата">
                                    Отчёт: <?= Yii::$app->formatter->asDate($model->event_date) ?>
                                    (<?= Yii::$app->formatter->asRelativeTime($model->event_date, $model->remind_date) ?>)
                                </span>
                            </div>
                            <!-- /.info-box-content -->
                        </div>
                        <!-- /.info-box -->
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>