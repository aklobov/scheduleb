<?php

use yii\helpers\Url;

/**
 * @var $model common\models\NotificationLog
 */
//$model->status = $model::STATUS_EXPIRED_AND_CONFIRMED;
$is_expired = in_array($model->status, [$model::STATUS_EXPIRED, $model::STATUS_EXPIRED_AND_CONFIRMED]);
$bgColor    = $is_expired ? 'bg-yellow' : 'bg-green';
$icon       = $is_expired ? 'fa-calendar-times-o' : 'fa-calendar-check-o';
?>
<i class="fa fa-newspaper-o <?= $bgColor ?>"></i>
<div class="timeline-item">
    <span class="time" title="<?= Yii::$app->formatter->asDate($model->created_at) ?>">
        <i class="fa fa-clock-o"></i>
        <?php echo Yii::$app->formatter->asRelativeTime($model->created_at) ?>
    </span>
    <h3 class="timeline-header"><?= $is_expired ? 'Просроченный отчёт' : 'Отчёт' ?></h3>

    <div class="timeline-body">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <a href="<?= Url::to(['rel-event-org/view', 'id' => $model->reminder_id]) ?>" target="_blank">
                        <div class="info-box <?= $bgColor ?>">
                            <span class="info-box-icon"><i class="fa <?= $icon ?>"></i></span>

                            <div class="info-box-content">
                                <span class="info-box-text" title="Организация"><?= $model->org->title ?></span>
                                <span class="info-box-number" title="Отчёт"><?= $model->event->title ?></span>

                                <div class="progress">
                                    <div class="progress-bar" style="width: <?= $is_expired ? 10 : 100 ?>%"></div>
                                </div>
                                <span class="progress-description" title="Дата">
                                    Состояние: <?= $model::getAllStatuses($model->status) ?>
                                </span>
                            </div>
                            <!-- /.info-box-content -->
                        </div>
                        <!-- /.info-box -->
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>