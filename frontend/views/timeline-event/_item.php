<?php
/**
 * @var $model common\models\NotificationLog
 */
?>
<div class="timeline-item">
    <span class="time">
        <i class="fa fa-clock-o"></i>
        <?php echo Yii::$app->formatter->asRelativeTime($model->created_at) ?>
    </span>
    <h3 class="timeline-header">
        <?php
        switch ($model->type) {
            case $model::TYPE_REMIND:
                echo 'Напоминание о событии';
                break;
            default:
                echo in_array($model->status, [$model::STATUS_EXPIRED, $model::STATUS_EXPIRED_AND_CONFIRMED]) ? 'Просроченное событие' : 'Событие';
                break;
        }
        ?>
    </h3>

    <div class="timeline-body">
        <dl>
            <dt>Организация:</dt>
            <dd><?php echo $model->org->title ?></dd>

            <dt>Событие:</dt>
            <dd><?php echo $model->event->title ?></dd>

            <dt>Дата:</dt>
            <dd><?php echo Yii::$app->formatter->asDate(($model->type == $model::TYPE_REMIND) ? $model->remind_date : $model->event_date) ?></dd>

            <dt>Состояние:</dt>
            <dd><?php echo $model::getAllStatuses($model->status) ?></dd>

        </dl>
        <?php
        if ($model->type == $model::TYPE_EVENT && in_array($model->status, [$model::STATUS_EXPIRED, $model::STATUS_EXPIRED_AND_CONFIRMED])) {
            echo 'Кнопка подтверждения просроченного статуса';
            ?>
            <div class="info-box">
                <span class="info-box-icon bg-green"><i class="fa fa-flag-o"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">Bookmarks</span>
                    <span class="info-box-number">410</span>
                </div>
                <!-- /.info-box-content -->
            </div>
            <?php
        }
        ?>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <div class="info-box bg-yellow">
                    <span class="info-box-icon bg-green"><i class="fa fa-flag-o"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text">Bookmarks</span>
                        <span class="info-box-number">410</span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
            </div>
            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="info-box bg-yellow">
                    <span class="info-box-icon"><i class="fa fa-calendar"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text">Events</span>
                        <span class="info-box-number">41,410</span>

                        <div class="progress">
                            <div class="progress-bar" style="width: 70%"></div>
                        </div>
                        <span class="progress-description">
                            70% Increase in 30 Days
                        </span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>
        </div>
    </div>
</div>