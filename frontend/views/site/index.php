<?php

//use frontend\assets\FrontendIndexPageAsset;

/* @var $this yii\web\View */
$this->title = 'Удобная бухгалтерия для бухгалтеров и предпринимателей "План Б"';
//FrontendIndexPageAsset::register($this);
?>
<div class="first">
    <div class="container">
        <div class="header-content mt-5">
            <div class="row">
                <div class="col-md-7">
                    <div class="header-content-text">
                        <h1>Удобная бухгалтерия
                            для <span class="purpul">бухгалтеров</span>
                            и <span class="purpul">предпринимателей<span></h1>
                        <p class="subTitile">Онлайн-сервис для учета и контроля</p>
                        <div class="header-content-text-service">
                            <p>Lorem Ipsum is simply dummy text of the printing
                                typesetting industry. Lorem Ipsum has been
                                the industry's standard dummy text ever since.</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-5">
                    <div class="header-content-form">
                        <form class="form blue-bg ajax-contact" action="/site/invite-user" method="post">
                            <div class="form-group">
                                <p>Ваше имя<p>
                                    <img src="/img/icon-user.png" alt="">
                                    <input type="text" name="name" id="fieldColor"  class="user_name form-control" placeholder="Дмитрий" >
                                <div class="invalid-feedback if1">Введите ваше имя</div>
                            </div>
                            <div class="form-group">
                                <p>Ваш e-mail<p>
                                    <img src="/img/icon-mail.png" alt="">
                                    <input type="text" name="email" id="fieldColor"  class="user_email form-control"  placeholder="mail@mail.ru"  >
                                <div class="invalid-feedback if2">Введите контактный e-mail</div>
                            </div>
                            <button class="btn btn-form btn_submit " type="submit">Попробовать бесплатно</button>
                            <div class="form-group" style="border-bottom: none;font-size: 11px;font-weight: 400;padding-top: 10px;color:#aba8a8!important;">
                                <input type="checkbox" name="ny" style="width:5%;" required="" class="tnp-privacy">&nbsp; Используя сервис, вы соглашаетесь с условиями 
                                <a style="color:#96a9f8!important;" target="_blank" href="/page/agreement">Лицензионного договора</a></div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<section class="services" id="services">
    <a name="services" />
    <div class="container">
        <h2 class="section-head">У нас есть решение для каждого</h2>
        <div class="row">
            <div class="tabs">
                <input id="tab1" type="radio" name="tabs" checked>
                <label for="tab1" title="Вкладка 1"><img src="/img/icon-accounter.png" alt=""> Бухгалтеру</label>

                <input id="tab2" type="radio" name="tabs">
                <label for="tab2" title="Вкладка 2"><img src="/img/icon-bussinesman.png" alt=""> Предпринимателю</label>

                <input id="tab3" type="radio" name="tabs">
                <label for="tab3" title="Вкладка 3"><img src="/img/icon-company.png" alt=""> Бухгалтерской фирме</label>

                <section id="content-tab1">
                    <?= \common\widgets\DbText::widget(['key' => 'solutions-accounter']) ?>
                </section>  
                <section id="content-tab2">
                    <?= \common\widgets\DbText::widget(['key' => 'solutions-bussinesman']) ?>
                </section> 
                <section id="content-tab3">
                    <?= \common\widgets\DbText::widget(['key' => 'solutions-company']) ?>
                </section> 
            </div>
        </div>
    </div>
</section>
<section class="special" id="special">
    <a name="special" />
    <div class="container">
        <div class="row">
            <?= \common\widgets\DbText::widget(['key' => 'section-special']) ?>
        </div>
    </div>
</section>
<section class="price" id="price">
    <a name="price" />
    <img class="fon-pils" src="/img/pils2.png" alt="">
    <div class="container">
        <h2 class="section-head">Сколько стоят наши услуги</h2>
        <div class="price-card">
            <?= \common\widgets\DbText::widget(['key' => 'section-prices']) ?>
        </div>
    </div>
</section>

<section class="reviews" id="reviews">
    <a name="reviews" />
    <div class="container">
        <h2 class="section-head1" style="padding:0;">Отзывы</h2>
        <div class="row">
            <div class="col-md-12 m-auto p-5">
                <div class="rev_slid slider">
                    <div class="rev-item">
                        <div class="rev-img-text">
                            <img src="/img/rev.png" alt="">
                        </div>
                        <p class="review-name">Анна Иванова</p>
                        <p class="rev-content">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Atque delectus eius fugiat, iusto minima odio pariatur quae sapiente ullam voluptate. A aperiam aspernatur culpa cupiditate delectus deserunt ea enim.</p>
                    </div>
                    <div class="rev-item">
                        <div class="rev-img-text">
                            <img src="/img/rev.png" alt="">
                        </div>
                        <p class="review-name">Анна Иванова</p>
                        <p class="rev-content">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad adipisci consectetur eos eveniet excepturi in iste nesciunt officiis, optio perspiciatis quisquam repudiandae sint voluptas. Accusantium adipisci.</p>
                    </div>
                    <div class="rev-item">
                        <div class="rev-img-text">
                            <img src="/img/rev.png" alt="">
                        </div>
                        <p class="review-name">Анна Иванова</p>
                        <p class="rev-content">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad adipisci consectetur eos eveniet excepturi in iste nesciunt officiis, optio perspiciatis quisquam repudiandae sint voluptas. Accusantium adipisci, assumenda.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<style>
.form-group { position: relative; }
.form-control { border-radius: 0; }
.error { color: #b30000; }
.fa {
    position: absolute;
    right: 1px;
    top: 26px;
    width: 32px;
    overflow: hidden;
    height: 32px;
    -webkit-transition: width 0.1s; /* For Safari 3.1 to 6.0 */
    transition: width 0.1s;
}
.fa:before {
    z-index: 3;
    position: absolute;
    top: 3px;
    left: -6px;
    color: #fff;
    width: 100%;
    height: 100%;
    text-align: right;
}
.fa:after {
    position: absolute;
    content: "";
    right: 0;
    top: 0;
    width: 0;
    height: 0;
/*        border-top: 32px solid #b30000;
*        border-left: 32px solid transparent;
*/
}
.fa span {
    display: none;
    background-color: #b30000;
    color: #fff;
    position: absolute;
    width: 200px;
    height: 32px;
    line-height: 32px;
    font-size: 14px;
    font-family: verdana;
    right: 0;
    padding: 0 32px 0 10px;
    top: 0;
    text-align: right;
}
.fa:hover { width: 200px; cursor: pointer; }
.fa:hover span { display: block; }

.alert {
    display: block;
    width: 400px;
    margin: auto;
    position: fixed;
    left: 50%;
    margin-left: -200px;
    top: 200px;
}
</style>

<div class="modal fade" id="successfullyModal" tabindex="-1" role="dialog" aria-labelledby="successfullyModal" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Регистрация учетной записи</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                На указанный вами e-mail отправлены регистрационнные данные для входа на сайт.
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>
