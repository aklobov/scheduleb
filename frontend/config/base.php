<?php

return [
    'id'         => 'frontend',
    'basePath'   => dirname(__DIR__),
    'components' => [
        'urlManager'   => require(__DIR__ . '/_urlManager.php'),
        'cache'        => require(__DIR__ . '/_cache.php'),
        'assetManager' => [
            'bundles' => [
                'yii\bootstrap4\BootstrapAsset'       => [
                    'sourcePath' => '@npm/bootstrap/dist'
                ],
                'yii\bootstrap4\BootstrapPluginAsset' => [
                    'sourcePath' => '@npm/bootstrap/dist'
                ]
            ]
        ]
    ],
];
