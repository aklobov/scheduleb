<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use common\models\Page;
use common\models\UserToken2;
use common\models\User;
use cheatsheet\Time;
use common\commands\SendEmailCommand;
use yii\helpers\Url;

/**
 * Invite User form form
 */
class InviteUserForm extends Model {

    public $email;
    public $name;

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            // name, email are required
            [['name', 'email'], 'required'],
            // We need to sanitize them
            ['name', 'filter', 'filter' => 'strip_tags'],
            // email has to be a valid email address
            ['email', 'email'],
            ['email', 'unique', 'targetClass' => User::class, 'targetAttribute' => 'email'],
            ['name', 'string', 'max' => 50],
        ];
    }

    public function attributeLabels() {
        return [
            'email' => Yii::t('frontend', 'E-mail'),
            'name'  => Yii::t('frontend', 'Имя'),
        ];
    }

    /**
     * Sends an email with a link for signup.
     *
     * @return boolean whether the email was send
     */
    public function sendEmail() {
        $subject     = 'Приглашение от "План Б"';
        $refParams   = [];
//        $refParams[] = '/user/sign-in/signup';
        $refParams[] = '/user/sign-in/signup-by-params';


        $page       = Page::find()->where(['slug' => 'inviteviaemail', 'status' => Page::STATUS_PUBLISHED])->one();
        $inviteText = is_null($page) ? '' : $page->body;

        $params = ['email' => $this->email, 'name' => $this->name, 'password' => Yii::$app->getSecurity()->generateRandomString(8)];
        $token  = UserToken2::create(UserToken2::GUEST_USER_ID, UserToken2::TYPE_INVITE_USER, Time::SECONDS_IN_A_WEEK, $params);
        $refParams['token'] = $token->token;
        return Yii::$app->commandBus->handle(new SendEmailCommand([
                    'to'      => $this->email,
                    'subject' => $subject,
                    'view'    => 'inviteUser',
                    'params'  => [
                        'inviteUrl'  => Url::to($refParams, true),
                        'inviteText' => $inviteText,
                        'params'     => $params,
                    ]
        ]));
    }

}
