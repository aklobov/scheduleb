<?php

namespace frontend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Event;
use common\models\Org;
use common\models\RelEventOrg;

/**
 * EventSearch represents the model behind the search form of `common\models\Event`.
 */
class EventSearch extends Event {

    public $reminder_status;

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
            [['id', 'is_default', 'is_vehicles_tax', 'is_property_tax'], 'integer'],
            [['title', 'type', 'handler', 'frequency', 'tags', 'gov_org', 'recurrence_pattern', 'description',], 'safe'],
            [['org_type', 'tax_type', 'hired_labour', 'status', 'reminder_status', 'action'], 'string'],
            ['action', 'in', 'range' => array_merge(array_keys(Event::getAllActions()), [Event::ACTION_CUSTOM, ''])],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
        $query = Event::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query = $this->addFilterConditions($query);

        return $dataProvider;
    }

    /**
     * Creates data provider instance with search query applied
     * События, применимые к заданной организации
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function searchApplicable($params, Org $org) {
        $query        = Event::find()->applicable($org);
        // add conditions that should always apply here
//        $query = $query->innerJoinWith('{{%rel_event_org}}')
//        $query        = $query->joinWith('orgs o')
//                ->where(['o.id' => $org->id]);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
//        $str          = $query->createCommand()->getRawSql();

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query = $this->addFilterConditions($query);

        return $dataProvider;
    }

    public function searchApplicableReports($params, Org $org) {
        $dataProvider = $this->searchApplicable($params, $org);
        $dataProvider->query->andWhere([
            'action' => Event::ACTION_REPORT,
//            ['in', 'action', [Event::ACTION_REPORT]],
        ]);
        return $dataProvider;
    }

    public function searchApplicablePayments($params, Org $org) {
        $dataProvider = $this->searchApplicable($params, $org);
        $dataProvider->query->andWhere([
            'action' => Event::ACTION_PAYMENT,
        ]);
        return $dataProvider;
    }

    public function searchApplicableCustoms($params) {
        $query = Event::find()->applicable(null, Event::TYPE_CUSTOM);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query = $this->addFilterConditions($query);

        return $dataProvider;
    }

    /**
     * Назначенные отчёты
     * @param type $params
     * @param Org $org
     */
    public function searchRelated($params, Org $org) {
        $query = Event::find()
                ->where([
            'or',
            [
                'type' => Event::TYPE_DEFAULT,
            ],
            [
                'and',
                [
                    'type'       => Event::TYPE_CUSTOM,
                    'created_by' => getMyId(),
                ]
            ]
        ]);

        // add conditions that should always apply here
        $query        = $query->joinWith('orgs o')
                ->where(['o.id' => $org->id]);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query = $this->addFilterConditions($query);

//        $str          = $query->createCommand()->getRawSql();
        return $dataProvider;
    }

    /**
     * Назначенные платежи
     * @param type $params
     * @param Org $org
     */
    public function searchRelatedPayments($params, Org $org) {
        $query = Event::find()
                ->where([
                    'or',
                    [
                        'type' => Event::TYPE_DEFAULT,
                    ],
                    [
                        'and',
                        [
                            'type'       => Event::TYPE_CUSTOM,
                            'created_by' => getMyId(),
                        ]
                    ]
                ])
                ->andWhere([
            'action' => Event::ACTION_PAYMENT
        ]);

        // add conditions that should always apply here
        $query        = $query->joinWith('orgs o')
                ->where(['o.id' => $org->id]);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query = $this->addFilterConditions($query);

//        $str          = $query->createCommand()->getRawSql();
        return $dataProvider;
    }

    protected function addFilterConditions(yii\db\Query $query) {
        // grid filtering conditions
        $query->andFilterWhere([
            'id'                         => $this->id,
            'is_default'                 => $this->is_default,
            'start_date'                 => $this->start_date,
            'end_date'                   => $this->end_date,
            '{{%event}}.frequency'       => $this->frequency,
//            '{{%event}}.org_type'        => $this->org_type,
//            '{{%event}}.tax_type'        => $this->tax_type,
//            '{{%event}}.hired_labour'    => $this->hired_labour,
            '{{%event}}.is_vehicles_tax' => $this->is_vehicles_tax,
            '{{%event}}.is_property_tax' => $this->is_property_tax,
//            'created_at'   => $this->created_at,
//            'updated_at'   => $this->updated_at,
            'created_by'   => $this->created_by,
//            'updated_by'   => $this->updated_by,
        ]);

        if (!empty($this->org_type)) {
//            $query->andWhere(new \yii\db\Expression('FIND_IN_SET(:org_type,org_type)'));
            $query->andFilterWhere(['like', '{{%event}}.org_type', $this->org_type]);
        }
        if (!empty($this->tax_type)) {
//            $query->andWhere(new \yii\db\Expression('FIND_IN_SET(:tax_type,tax_type)'));
            $query->andFilterWhere(['like', '{{%event}}.tax_type', $this->tax_type]);
        }
        if (!empty($this->hired_labour)) {
//            $query->andWhere(new \yii\db\Expression('FIND_IN_SET(:hired_labour,hired_labour)'));
            $query->andFilterWhere(['like', '{{%event}}.hired_labour', $this->hired_labour]);
        }

        $query->andFilterWhere(['like', '{{%event}}.title', $this->title])
                ->andFilterWhere(['like', 'type', $this->type])
                ->andFilterWhere(['like', 'tags', $this->tags])
                ->andFilterWhere(['like', 'gov_org', $this->gov_org])
//                ->andFilterWhere(['like', 'frequency', $this->frequency])
                ->andFilterWhere(['like', 'description', $this->description])
                ->andFilterWhere(['like', '{{%event}}.status', $this->status]);
//                ->addParams([
//                    ':org_type'     => $this->org_type,
//                    ':tax_type'     => $this->tax_type,
//                    ':hired_labour' => $this->hired_labour,
//        ]);

        if (!empty($this->reminder_status)) {
            $query->joinWith('reminders rem');
            if ($this->reminder_status == RelEventOrg::STATUS_SOON_FAKE) {
                $query->andWhere(['not', ['{{%rel_event_org}}.status' => RelEventOrg::STATUS_FINISHED]])
                        ->andWhere(['>', 'curr_date', date('Y-m-d')]) //CURDATE()
                        ->andWhere('DATEDIFF(curr_date, CURDATE()) < {{%rel_event_org}}.remind_before');
            }
            elseif($this->reminder_status == RelEventOrg::STATUS_NEEDED_FAKE){
                $query->andWhere(['in', '{{%rel_event_org}}.status', [RelEventOrg::STATUS_ACTIVE, RelEventOrg::STATUS_IN_WORK]]);
            }
            elseif($this->reminder_status == RelEventOrg::STATUS_EXPIRED_FAKE){
                $query->andWhere(['not in', '{{%rel_event_org}}.status', [RelEventOrg::STATUS_FINISHED, RelEventOrg::STATUS_CANCELED]])
                        ->andWhere(['<', 'curr_date', date('Y-m-d')]);
            }
            else {
                $query->andWhere(['{{%rel_event_org}}.status' => $this->reminder_status]);
            }
        }
        if (!empty($this->action)) {
            if($this->action == Event::ACTION_CUSTOM){
                $query->andWhere(['type' => Event::TYPE_CUSTOM]);
            }else{
                $query->andWhere(['type' => Event::TYPE_DEFAULT, 'action' => $this->action]);
            }
        }

//        die($query->createCommand()->getRawSql());
        return $query;
    }

}
