<?php

namespace frontend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Org;

/**
 * OrgSearch represents the model behind the search form of `common\models\Org`.
 */
class OrgSearch extends Org {

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
            [['owner_id', 'group_id', 'is_default', 'is_vehicles_tax', 'is_property_tax',], 'integer'],
            [['status',], 'safe'],
            [['title', 'hired_labour', 'org_type', 'tax_type'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
        $query = Org::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id'           => $this->id,
            'owner_id'     => $this->owner_id,
            'group_id'     => $this->group_id,
            'hired_labour' => $this->hired_labour,
            'is_default'   => $this->is_default,
            'created_at'   => $this->created_at,
            'updated_at'   => $this->updated_at,
            'created_by'   => $this->created_by,
            'updated_by'   => $this->updated_by,
            'status'       => $this->status,
        ]);

        $query->andFilterWhere(['like', 'org_type', $this->org_type])
                ->andFilterWhere(['like', 'tax_type', $this->tax_type])
                ->andFilterWhere(['like', 'title', $this->title]);

        return $dataProvider;
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function searchMyActive($params) {
        $query = Org::find()->where([
            'owner_id' => getMyId(),
            'status'   => Org::STATUS_ACTIVE,
        ]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id'           => $this->id,
            'owner_id'     => $this->owner_id,
            'group_id'     => $this->group_id,
            'hired_labour' => $this->hired_labour,
            'is_default'   => $this->is_default,
            'created_at'   => $this->created_at,
            'updated_at'   => $this->updated_at,
            'created_by'   => $this->created_by,
            'updated_by'   => $this->updated_by,
            'status'       => $this->status,
        ]);

        $query->andFilterWhere(['like', 'org_type', $this->org_type])
                ->andFilterWhere(['like', 'tax_type', $this->tax_type])
                ->andFilterWhere(['like', 'title', $this->title]);

        return $dataProvider;
    }

}
