-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Хост: localhost
-- Время создания: Окт 17 2018 г., 14:30
-- Версия сервера: 5.7.16
-- Версия PHP: 7.1.19

SET FOREIGN_KEY_CHECKS=0;
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `scheduleb`
--

-- --------------------------------------------------------

--
-- Структура таблицы `scheduleb_rbac_auth_assignment`
--

CREATE TABLE `scheduleb_rbac_auth_assignment` (
  `item_name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- СВЯЗИ ТАБЛИЦЫ `scheduleb_rbac_auth_assignment`:
--   `item_name`
--       `scheduleb_rbac_auth_item` -> `name`
--

-- --------------------------------------------------------

--
-- Структура таблицы `scheduleb_rbac_auth_item`
--

CREATE TABLE `scheduleb_rbac_auth_item` (
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `type` smallint(6) NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `rule_name` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `data` blob,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- СВЯЗИ ТАБЛИЦЫ `scheduleb_rbac_auth_item`:
--   `rule_name`
--       `scheduleb_rbac_auth_rule` -> `name`
--

-- --------------------------------------------------------

--
-- Структура таблицы `scheduleb_rbac_auth_item_child`
--

CREATE TABLE `scheduleb_rbac_auth_item_child` (
  `parent` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `child` varchar(64) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- СВЯЗИ ТАБЛИЦЫ `scheduleb_rbac_auth_item_child`:
--   `parent`
--       `scheduleb_rbac_auth_item` -> `name`
--   `child`
--       `scheduleb_rbac_auth_item` -> `name`
--

-- --------------------------------------------------------

--
-- Структура таблицы `scheduleb_rbac_auth_rule`
--

CREATE TABLE `scheduleb_rbac_auth_rule` (
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `data` blob,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- СВЯЗИ ТАБЛИЦЫ `scheduleb_rbac_auth_rule`:
--

-- --------------------------------------------------------

--
-- Структура таблицы `scheduleb_system_db_migration`
--

CREATE TABLE `scheduleb_system_db_migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- СВЯЗИ ТАБЛИЦЫ `scheduleb_system_db_migration`:
--

--
-- Дамп данных таблицы `scheduleb_system_db_migration`
--

INSERT INTO `scheduleb_system_db_migration` (`version`, `apply_time`) VALUES
('m000000_000000_base', 1539775790),
('m140703_123000_user', 1539775797),
('m140703_123055_log', 1539775797),
('m140703_123104_page', 1539775797),
('m140703_123803_article', 1539775798),
('m140703_123813_rbac', 1539775799),
('m140709_173306_widget_menu', 1539775799),
('m140709_173333_widget_text', 1539775799),
('m140712_123329_widget_carousel', 1539775800),
('m140805_084745_key_storage_item', 1539775800),
('m141012_101932_i18n_tables', 1539775800),
('m150318_213934_file_storage_item', 1539775800),
('m150414_195800_timeline_event', 1539775800),
('m150725_192740_seed_data', 1539775802),
('m150929_074021_article_attachment_order', 1539775802),
('m160203_095604_user_token', 1539775802);

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `scheduleb_rbac_auth_assignment`
--
ALTER TABLE `scheduleb_rbac_auth_assignment`
  ADD PRIMARY KEY (`item_name`,`user_id`);

--
-- Индексы таблицы `scheduleb_rbac_auth_item`
--
ALTER TABLE `scheduleb_rbac_auth_item`
  ADD PRIMARY KEY (`name`),
  ADD KEY `rule_name` (`rule_name`),
  ADD KEY `idx-auth_item-type` (`type`);

--
-- Индексы таблицы `scheduleb_rbac_auth_item_child`
--
ALTER TABLE `scheduleb_rbac_auth_item_child`
  ADD PRIMARY KEY (`parent`,`child`),
  ADD KEY `child` (`child`);

--
-- Индексы таблицы `scheduleb_rbac_auth_rule`
--
ALTER TABLE `scheduleb_rbac_auth_rule`
  ADD PRIMARY KEY (`name`);

--
-- Индексы таблицы `scheduleb_system_db_migration`
--
ALTER TABLE `scheduleb_system_db_migration`
  ADD PRIMARY KEY (`version`);

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `scheduleb_rbac_auth_assignment`
--
ALTER TABLE `scheduleb_rbac_auth_assignment`
  ADD CONSTRAINT `scheduleb_rbac_auth_assignment_ibfk_1` FOREIGN KEY (`item_name`) REFERENCES `scheduleb_rbac_auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `scheduleb_rbac_auth_item`
--
ALTER TABLE `scheduleb_rbac_auth_item`
  ADD CONSTRAINT `scheduleb_rbac_auth_item_ibfk_1` FOREIGN KEY (`rule_name`) REFERENCES `scheduleb_rbac_auth_rule` (`name`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `scheduleb_rbac_auth_item_child`
--
ALTER TABLE `scheduleb_rbac_auth_item_child`
  ADD CONSTRAINT `scheduleb_rbac_auth_item_child_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `scheduleb_rbac_auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `scheduleb_rbac_auth_item_child_ibfk_2` FOREIGN KEY (`child`) REFERENCES `scheduleb_rbac_auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE;
SET FOREIGN_KEY_CHECKS=1;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
