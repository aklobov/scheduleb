-- phpMyAdmin SQL Dump
-- version 4.4.15.10
-- https://www.phpmyadmin.net
--
-- Хост: localhost
-- Время создания: Ноя 11 2018 г., 11:11
-- Версия сервера: 10.1.36-MariaDB
-- Версия PHP: 5.4.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `planb`
--

-- --------------------------------------------------------

--
-- Структура таблицы `scheduleb_event`
--

CREATE TABLE IF NOT EXISTS `scheduleb_event` (
  `id` int(11) unsigned NOT NULL,
  `title` varchar(250) DEFAULT NULL COMMENT 'Название события/платежа',
  `type` enum('default','custom') NOT NULL DEFAULT 'default' COMMENT 'Событие стандартное или  создано пользователем',
  `action` enum('report','payment') NOT NULL DEFAULT 'report',
  `handler` varchar(128) DEFAULT NULL COMMENT 'ссылка на функцию вычисляющую даты события',
  `tags` varchar(1024) DEFAULT NULL,
  `gov_org` varchar(1024) DEFAULT NULL COMMENT 'Государственная контролирующая организация',
  `is_default` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Показывать отчет/платеж при создании организации',
  `start_date` date DEFAULT NULL,
  `curr_date` date DEFAULT NULL COMMENT 'Ближайшая дата события ',
  `end_date` date DEFAULT NULL,
  `frequency` enum('none','month','quarter','year') NOT NULL DEFAULT 'year' COMMENT 'Период повторения',
  `description` text,
  `status` enum('active','trashed','archived','deleted') NOT NULL DEFAULT 'active' COMMENT 'Состояние события',
  `org_type` set('Юридическое лицо','Индивидуальный предприниматель') DEFAULT NULL COMMENT 'Организационно-правовая форма',
  `tax_type` set('Общая система налогообложения','Упрощенная система налогообложения','Единый налог на вмененный доход','Патентная система налогообложения','Единый сельскохозяйственный налог') DEFAULT NULL COMMENT 'Система налогообложения',
  `hired_labour` set('Есть','Нет') DEFAULT 'Нет' COMMENT 'Использование наемного труда',
  `is_vehicles_tax` tinyint(1) DEFAULT NULL COMMENT 'Для плательщика транспортного налога',
  `is_property_tax` tinyint(1) DEFAULT NULL COMMENT 'Для плательщика налога на имущество',
  `strict_condition` tinyint(1) DEFAULT '0' COMMENT 'строгое соответствие параметрам (тип организации, наемный труд, ...)',
  `created_at` int(11) unsigned DEFAULT NULL,
  `updated_at` int(11) unsigned DEFAULT NULL,
  `created_by` int(11) unsigned DEFAULT NULL,
  `updated_by` int(11) unsigned DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='Список доступных в системе событий и платежей';

--
-- Дамп данных таблицы `scheduleb_event`
--

INSERT INTO `scheduleb_event` (`id`, `title`, `type`, `action`, `handler`, `tags`, `gov_org`, `is_default`, `start_date`, `curr_date`, `end_date`, `frequency`, `description`, `status`, `org_type`, `tax_type`, `hired_labour`, `is_vehicles_tax`, `is_property_tax`, `strict_condition`, `created_at`, `updated_at`, `created_by`, `updated_by`) VALUES
(11, 'Декларация по НДС', 'default', 'report', NULL, '', '', 0, '2019-01-25', NULL, '2040-12-31', 'quarter', 'До 25 числа месяца, след.за отчетным периодом', 'active', NULL, 'Общая система налогообложения', NULL, 0, 0, 0, 1541869498, 1541869838, 2, 2),
(12, 'Декларация по налогу на прибыль (годовая)', 'default', 'report', NULL, '', '', 0, '2019-03-28', '2019-03-28', '2030-12-31', 'year', 'Годовая - до 28 марта.', 'active', 'Юридическое лицо', 'Общая система налогообложения', '', 0, 0, 1, 1541870787, 1541922397, 2, 2),
(13, 'Декларация по налогу на прибыль (ежемесячная)', 'default', 'report', NULL, '', '', 0, '2018-01-28', NULL, '2030-12-31', 'month', 'До 28 числа месяца, след.за отчетным периодом; годовая до 28 марта', 'active', 'Юридическое лицо', 'Общая система налогообложения', '', 0, 0, 1, 1541870856, 1541920234, 2, 2),
(14, 'Сведения о среднесписочной численности', 'default', 'report', NULL, '', '', 0, '2019-01-20', '2019-01-21', '2030-12-31', 'year', '', 'active', '', '', 'Есть', 0, 0, 0, 1541870919, 1541922492, 2, 2),
(15, 'Форма 4-ФСС (в электронной форме)', 'default', 'report', NULL, '', '', 0, '2018-01-25', '2019-01-25', '2030-12-31', 'quarter', 'До 20 числа месяца, след.за отчетным периодом - если отчет в бумажном виде; До 25 числа - в электронном виде', 'active', '', '', 'Есть', 0, 0, 0, 1541871046, 1541922622, 2, 2),
(16, 'Форма 4-ФСС (в бумажном виде)', 'default', 'report', NULL, '', '', 0, '2018-01-20', '2019-01-21', '2030-12-31', 'quarter', '', 'active', '', '', 'Есть', 0, 0, 0, 1541871113, 1541922642, 2, 2),
(17, 'Подтверждение основного вида деятельности в ФСС', 'default', 'report', NULL, '', '', 0, '2018-04-15', '2019-04-15', '2030-12-31', 'year', '', 'active', 'Юридическое лицо', '', '', 0, 0, 0, 1541871179, 1541922672, 2, 2),
(18, 'Расчет по страховым взносам', 'default', 'report', NULL, '', '', 0, '2018-01-30', '2019-01-30', '2030-12-31', 'quarter', '', 'active', '', '', 'Есть', 0, 0, 0, 1541871252, 1541922717, 2, 2),
(19, 'Декларация по налогу на имущество', 'default', 'report', NULL, '', '', 0, '2018-03-30', '2019-04-01', '2030-12-31', 'year', '', 'active', 'Юридическое лицо', 'Общая система налогообложения', '', 0, 1, 1, 1541871337, 1541922793, 2, 2),
(20, 'Декларация по налогу на прибыль (ежеквартальная)', 'default', 'report', NULL, '', '', 0, '2018-01-28', NULL, '2030-12-31', 'quarter', 'За предыдущий месяц.\r\nДо 28 числа месяца, след.за отчетным периодом.', 'active', 'Юридическое лицо', 'Общая система налогообложения', '', 0, 0, 1, 1541921297, 1541922296, 2, 2),
(21, 'Авансы по налогу на имущество', 'default', 'report', NULL, '', '', 0, '2018-04-30', '2019-04-30', '2030-12-31', 'quarter', 'Квартальная (только за 1, 2 и 3 квартал)', 'active', 'Юридическое лицо', '', '', 0, 1, 1, 1541922906, 1541922921, 2, 2),
(22, 'Декларация по транспортному налогу', 'default', 'report', NULL, '', '', 0, '2018-02-01', '2019-02-01', '2030-12-31', 'year', '', 'active', 'Юридическое лицо', '', '', 1, 0, 1, 1541922982, 1541923071, 2, 2),
(23, 'Декларация по земельному налогу', 'default', 'report', NULL, '', '', 0, '2018-02-01', '2019-02-01', '2030-12-31', 'year', '', 'active', 'Юридическое лицо', '', '', 0, 1, 1, 1541923035, 1541923035, 2, 2),
(24, '6-НДФЛ', 'default', 'report', NULL, '', '', 0, '2018-03-31', '2018-12-31', '2030-12-31', 'quarter', '', 'active', '', '', 'Есть', 0, 0, 0, 1541923315, 1541923315, 2, 2),
(25, '2-НДФЛ', 'default', 'report', NULL, '', '', 0, '2019-03-31', '2019-04-01', '2030-12-31', 'year', '', 'active', '', '', 'Есть', 0, 0, 0, 1541923360, 1541923360, 2, 2),
(26, 'СЗВ-М', 'default', 'report', NULL, '', '', 0, '2018-01-15', NULL, '2030-12-31', 'month', '', 'active', '', '', 'Есть', 0, 0, 0, 1541923401, 1541923401, 2, 2),
(27, 'Бухгалтерская отчетность', 'default', 'report', NULL, '', '', 0, '2019-03-31', '2019-04-01', '2030-12-31', 'year', '', 'active', 'Юридическое лицо', '', '', 0, 0, 0, 1541923449, 1541923449, 2, 2),
(28, 'СЗВ-стаж', 'default', 'report', NULL, '', '', 0, '2018-03-01', '2019-03-01', '2030-12-31', 'year', '', 'active', '', '', 'Есть', 0, 0, 0, 1541923489, 1541923489, 2, 2),
(29, 'Декларация по УСН (юридическое лицо)', 'default', 'report', NULL, '', '', 0, '2019-03-31', '2019-04-01', '2030-12-31', 'year', '', 'active', 'Юридическое лицо', 'Упрощенная система налогообложения', '', 0, 0, 1, 1541923576, 1541923576, 2, 2),
(30, 'Декларация по УСН (ИП)', 'default', 'report', NULL, '', '', 0, '2018-05-03', '2019-05-03', '2030-12-31', 'year', '', 'active', 'Индивидуальный предприниматель', 'Упрощенная система налогообложения', '', 0, 0, 1, 1541923634, 1541923634, 2, 2),
(31, 'Декларация по ЕНВД', 'default', 'report', NULL, '', '', 0, '2018-01-20', '2019-01-21', '2030-12-31', 'quarter', '', 'active', '', 'Единый налог на вмененный доход', '', 0, 0, 0, 1541923744, 1541923744, 2, 2),
(32, 'Декларация по ЕСХН', 'default', 'report', NULL, '', '', 0, '2019-03-31', '2019-04-01', '2030-12-31', 'year', '', 'active', '', 'Единый сельскохозяйственный налог', '', 0, 0, 0, 1541923789, 1541923789, 2, 2),
(33, '3-НДФЛ', 'default', 'report', NULL, '', '', 0, '2019-04-30', '2019-04-30', '2030-12-31', 'year', '', 'active', 'Индивидуальный предприниматель', 'Общая система налогообложения', '', 0, 0, 1, 1541923842, 1541923842, 2, 2);

-- --------------------------------------------------------

--
-- Структура таблицы `scheduleb_event_exception`
--

CREATE TABLE IF NOT EXISTS `scheduleb_event_exception` (
  `id` int(11) unsigned NOT NULL,
  `event_id` int(11) unsigned NOT NULL,
  `start_date` date DEFAULT NULL,
  `frequency` enum('none','month','quarter','year') NOT NULL DEFAULT 'none' COMMENT 'Период повторения',
  `created_at` int(11) unsigned DEFAULT NULL,
  `updated_at` int(11) unsigned DEFAULT NULL,
  `created_by` int(11) unsigned DEFAULT NULL,
  `updated_by` int(11) unsigned DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `scheduleb_event_exception`
--

INSERT INTO `scheduleb_event_exception` (`id`, `event_id`, `start_date`, `frequency`, `created_at`, `updated_at`, `created_by`, `updated_by`) VALUES
(11, 11, NULL, 'none', 1541869498, 1541869498, 2, 2),
(12, 12, NULL, 'none', 1541870787, 1541870787, 2, 2),
(13, 13, NULL, 'none', 1541870856, 1541870856, 2, 2),
(14, 14, NULL, 'none', 1541870919, 1541870919, 2, 2),
(15, 15, NULL, 'none', 1541871046, 1541871046, 2, 2),
(16, 16, NULL, 'none', 1541871113, 1541871113, 2, 2),
(17, 17, NULL, 'none', 1541871179, 1541871179, 2, 2),
(18, 18, NULL, 'none', 1541871252, 1541871252, 2, 2),
(19, 19, NULL, 'none', 1541871337, 1541871337, 2, 2),
(20, 20, '2018-01-28', 'year', 1541921297, 1541922296, 2, 2),
(21, 21, '2019-01-30', 'year', 1541922906, 1541922906, 2, 2),
(22, 22, NULL, 'none', 1541922982, 1541922982, 2, 2),
(23, 23, NULL, 'none', 1541923035, 1541923035, 2, 2),
(24, 24, NULL, 'none', 1541923315, 1541923315, 2, 2),
(25, 25, NULL, 'none', 1541923360, 1541923360, 2, 2),
(26, 26, NULL, 'none', 1541923401, 1541923401, 2, 2),
(27, 27, NULL, 'none', 1541923449, 1541923449, 2, 2),
(28, 28, NULL, 'none', 1541923489, 1541923489, 2, 2),
(29, 29, NULL, 'none', 1541923576, 1541923576, 2, 2),
(30, 30, NULL, 'none', 1541923634, 1541923634, 2, 2),
(31, 31, NULL, 'none', 1541923744, 1541923744, 2, 2),
(32, 32, NULL, 'none', 1541923789, 1541923789, 2, 2),
(33, 33, NULL, 'none', 1541923842, 1541923842, 2, 2);

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `scheduleb_event`
--
ALTER TABLE `scheduleb_event`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_event_created` (`created_by`),
  ADD KEY `fk_event_updated` (`updated_by`);

--
-- Индексы таблицы `scheduleb_event_exception`
--
ALTER TABLE `scheduleb_event_exception`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_ee_created_by` (`created_by`),
  ADD KEY `fk_ee_updated_by` (`updated_by`),
  ADD KEY `fk_ee_event` (`event_id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `scheduleb_event`
--
ALTER TABLE `scheduleb_event`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=34;
--
-- AUTO_INCREMENT для таблицы `scheduleb_event_exception`
--
ALTER TABLE `scheduleb_event_exception`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=34;
--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `scheduleb_event`
--
ALTER TABLE `scheduleb_event`
  ADD CONSTRAINT `fk_event_created` FOREIGN KEY (`created_by`) REFERENCES `scheduleb_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_event_updated` FOREIGN KEY (`updated_by`) REFERENCES `scheduleb_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Ограничения внешнего ключа таблицы `scheduleb_event_exception`
--
ALTER TABLE `scheduleb_event_exception`
  ADD CONSTRAINT `fk_ee_created_by` FOREIGN KEY (`created_by`) REFERENCES `scheduleb_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_ee_event` FOREIGN KEY (`event_id`) REFERENCES `scheduleb_event` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_ee_updated_by` FOREIGN KEY (`updated_by`) REFERENCES `scheduleb_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
