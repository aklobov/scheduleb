<?php

/**
 * Eugine Terentev <eugine@terentev.net>
 */

namespace common\rbac\rule;

use yii\rbac\Item;
use yii\rbac\Rule;
use common\models\Event;
use common\models\User;

class EventDefaultRule extends Rule {

    /** @var string */
    public $name = 'eventDefaultRule';

    /**
     * @param int $user
     * @param Item $item
     * @param array $params
     * - model: model to check owner
     * @return bool
     */
    public function execute($user, $item, $params) {
//        return \Yii::$app->user->can(User::ROLE_MANAGER) &&
        $basicCheck = isset($params['model']) && $params['model'] instanceof Event;
        if ($basicCheck && !empty($params['model']->getAttribute('type'))) {
            return Event::TYPE_DEFAULT === $params['model']->getAttribute('type');
        }
        elseif($basicCheck && empty($params['model']->getAttribute('id'))) {
            //Новая модель
            return true;
        }
        return false;
//        return isset($params['model']) &&
//                $params['model'] instanceof Event &&
//                $params['model']->hasAttribute('type') &&
//                Event::TYPE_DEFAULT === $params['model']->getAttribute('type');
    }

}
