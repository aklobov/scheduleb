<?php

namespace common\assets;

use yii\web\AssetBundle;
//use yii\bootstrap\BootstrapAsset;
use common\assets\AdminLte;

class AdminLteDatepickerPluginAsset extends AssetBundle {

    public $sourcePath = '@bower/admin-lte/plugins';
    public $js         = [
        'datepicker/bootstrap-datepicker.js',
        'datepicker/locales/bootstrap-datepicker.ru.js',
    ];
    public $css        = [
        'datepicker/datepicker3.css',
    ];
    public $depends    = [
//        BootstrapAsset::class
        AdminLte::class
    ];

}
