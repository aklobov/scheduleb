<?php

namespace common\assets;

use yii\web\AssetBundle;
use common\assets\DataTablesPluginAsset;

class DataTablesPluginsAsset extends AssetBundle {

    public $sourcePath = '@common/components/datatables';
    public $js         = [
        'Buttons-1.5.4/js/dataTables.buttons.min.js',
        'Buttons-1.5.4/js/buttons.bootstrap.min.js',
        'Buttons-1.5.4/js/buttons.html5.min.js',
        'Buttons-1.5.4/js/buttons.colVis.min.js',
//        'js/buttons.flash.min.js',
//        'https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js',
//        'https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js',
        'FixedColumns-3.2.5/js/dataTables.fixedColumns.min.js',
        'FixedColumns-3.2.5/js/fixedColumns.bootstrap.min.js',
//        'FixedHeader-3.1.4/js/dataTables.fixedHeader.min.js',
//        'FixedHeader-3.1.4/js/fixedHeader.bootstrap.min.js',
    ];
    public $css        = [
        'Buttons-1.5.4/css/buttons.dataTables.min.css',
        'Buttons-1.5.4/css/buttons.bootstrap.min.css',
        'FixedColumns-3.2.5/css/fixedColumns.bootstrap.min.css',
//        'FixedHeader-3.1.4/css/fixedHeader.bootstrap.min.css',
    ];
    public $depends    = [
        DataTablesPluginAsset::class,
    ];

    //Media links
    //https://stackoverflow.com/questions/26487864/how-to-add-files-to-an-asset-that-has-its-sourcepath-in-the-vendor-directory-in
}
