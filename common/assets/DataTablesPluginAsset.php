<?php

namespace common\assets;

use yii\web\AssetBundle;
use yii\web\JqueryAsset;
use yii\bootstrap\BootstrapPluginAsset;

class DataTablesPluginAsset extends AssetBundle {

    public $sourcePath = '@common/components/datatables/DataTables-1.10.18';
    public $js         = [
        'js/jquery.dataTables.min.js',
        'js/dataTables.bootstrap.min.js',
    ];
    public $css        = [
        'css/dataTables.bootstrap.min.css',
        'css/jquery.dataTables.min.css',
    ];
    public $depends    = [
        JqueryAsset::class,
        BootstrapPluginAsset::class,
    ];
    
    //Media links
    //https://stackoverflow.com/questions/26487864/how-to-add-files-to-an-asset-that-has-its-sourcepath-in-the-vendor-directory-in

}
