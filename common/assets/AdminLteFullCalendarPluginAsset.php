<?php

namespace common\assets;

use yii\web\AssetBundle;
use common\assets\AdminLte;
use yii\web\JqueryAsset;
use trntv\yii\datetime\assets\MomentAsset;

class AdminLteFullCalendarPluginAsset extends AssetBundle {

    public $sourcePath = '@bower/admin-lte/plugins';
    public $js         = [
        'fullcalendar/fullcalendar.min.js',
    ];
    public $css        = [
        'fullcalendar/fullcalendar.min.css',
    ];
    public $depends    = [
        AdminLte::class,
        JqueryAsset::class,
        MomentAsset::class,
    ];

}
