<?php

namespace common\assets;

use yii\web\AssetBundle;
use common\assets\AdminLte;
use yii\web\JqueryAsset;
use trntv\yii\datetime\assets\MomentAsset;

class FullCalendarPluginAsset extends AssetBundle {

    public $sourcePath = '@bower/fullcalendar/dist';
    public $js         = [
        'fullcalendar.min.js',
        'locale/ru.js',
    ];
    public $css        = [
        'fullcalendar.min.css',
    ];
    public $depends    = [
        JqueryAsset::class,
        MomentAsset::class,
    ];

}
