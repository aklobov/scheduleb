<?php

namespace common\assets;

use yii\web\AssetBundle;
use common\assets\DataTablesPluginAsset;

class DataTablesPluginsAsset extends AssetBundle {

    public $sourcePath = '@npm';
    public $js         = [
        'datatables.net-buttons-bs/js/buttons.dataTables.min.js',
        'js/buttons.bootstrap.min.js',
        'js/buttons.html5.min.js',
        'js/buttons.colVis.min.js',
//        'js/buttons.flash.min.js',
//        'https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js',
//        'https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js',
    ];
    public $css        = [
        'css/buttons.dataTables.min.css',
        'css/buttons.bootstrap.min.css',
    ];
    public $depends    = [
        DataTablesPluginAsset::class,
    ];

    //Media links
    //https://stackoverflow.com/questions/26487864/how-to-add-files-to-an-asset-that-has-its-sourcepath-in-the-vendor-directory-in
}
