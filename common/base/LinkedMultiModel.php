<?php

namespace common\base;

/**
 * TwoLinkedModel
 * Работа с несколькими моделями, между которыми существует связь.
 * При сохранении первая в списке является ведущей.
 *
 * @author fistashkin
 */
class LinkedMultiModel extends MultiModel {

    /**
     * Base Model relation name
     * @var string
     */
    public $baseModelRelationName = null;

    /**
     * @param bool $runValidation
     * @return bool
     * @throws \yii\db\Exception
     */
    public function saveLinked($runValidation = true) {
        if ($runValidation && !$this->validate()) {
            return false;
        }
        $success     = true;
        $transaction = $this->getDb()->beginTransaction();
        $baseModel   = NULL;
        try {
            foreach ($this->models as $model) {
                if (empty($baseModel)) {
                    $success                     = $model->save(false);
                    $baseModel                   = $model;
                    $this->baseModelRelationName = strtolower((new \ReflectionClass($model))->getShortName());
                }
                else {
                    $model->link($this->baseModelRelationName, $baseModel);
                }
                if (!$success) {
                    $transaction->rollBack();
                    return false;
                }
            }
        }
        catch (yii\base\InvalidCallException $e) {
            $transaction->rollBack();
            return false;
        }
        $transaction->commit();
        return $success;
    }

}
