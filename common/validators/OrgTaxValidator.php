<?php

namespace common\validators;

use Yii;
use yii\validators\Validator;
use common\models\Org;

/**
 * Валидация связанных данных
 * тип организации -> система налогообложения
 */
class OrgTaxValidator extends Validator {

    /**
     * @inheritdoc
     */
    public function init() {
        parent::init();
        if ($this->message === null) {
            $this->message = '"{attribute}" не соответствует правовой форме организации';
        }
    }

    /**
     * @inheritdoc
     */
    public function validateValue($value) {
    }

    /**
     * @inheritdoc
     */
    public function clientValidateAttribute($model, $attribute, $view) {
        $org_type        = Org::TYPE_ORG_ENTITY;
        $tax_type_patent = Org::TYPE_TAX_PATENT;
        $tax_type_common = Org::TYPE_TAX_COMMON;
        $tax_type_usn    = Org::TYPE_TAX_USN;
        $tax_type_envd   = Org::TYPE_TAX_ENVD;
        $tax_type_eshn   = Org::TYPE_TAX_ESHN;
        return <<<"JS"
            org_type = $('input[name="Org[org_type]"]:checked').val();
            tax_types_raw = $('input[name="Org[tax_type][]"]:checked');
            tax_types = tax_types_raw.map(function(idx){
                return tax_types_raw[idx].value;
            });
            tax_types = tax_types.toArray();
        
            //$('input[value="$tax_type_patent"]').removeAttr("disabled");

            if(tax_types.length > 2){
                messages.push('Выберите не больше двух систем налогообложения.');
            }
            
            function isTaxPatent(tax_type){
                return tax_type == '$tax_type_patent';
            }
                
            if((org_type == '$org_type') && tax_types.some(isTaxPatent) ){
                messages.push('$tax_type_patent не совместима с организационно-правовой формой "$org_type".');
                //$('input[value="$tax_type_patent"]').prop("checked", false);
                //$('input[value="$tax_type_patent"]').attr("disabled", "disabled");
            }
                
                
            function isTaxUSN(tax_type){
                return tax_type == '$tax_type_usn';
            }
            function isTaxENVD(tax_type){
                return tax_type == '$tax_type_envd';
            }
            function isTaxESHN(tax_type){
                return tax_type == '$tax_type_eshn';
            }
            function isTaxCommon(tax_type){
                return tax_type == '$tax_type_common';
            }
                
            if(tax_types.some(isTaxCommon) && (tax_types.some(isTaxUSN) || tax_types.some(isTaxESHN))){
                messages.push('$tax_type_common не совместима с системами налогоообложения "$tax_type_usn" и "$tax_type_eshn".');
            }
            if(tax_types.some(isTaxUSN) && tax_types.some(isTaxESHN)){
                messages.push('$tax_type_usn не совместима с системой налогоообложения "$tax_type_eshn".');
            }
                

JS;
    }

}
