<?php

/*
 * Copyright 2014 Shaun Simmons
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

//TODO Сделать ограничение, принимающее recurrence pattern как список дат, исключенных из рассмотрения

namespace common\components\Recurr\Transformer\Constraint;

use Recurr\Transformer\Constraint;
use Recurr\DateUtil;
use Recurr\Rule;
use Recurr\Transformer\ArrayTransformer;
use Recurr\Transformer\ArrayTransformerConfig;
//use common\components\Recurr\Transformer\ArrayTransformerWeekends;

class ExceptionRRuleConstraint extends Constraint {

    protected $stopsTransformer = false;

    /** @var string */
    protected $rrule;

    /** @var string */
    protected $timeZone;

    /** @var \DateTimeInterface */
    protected $before;

    /** @var \DateTimeInterface */
    protected $after;

    /** @var bool */
    protected $inc;
    
    /** @var array */
    protected $dates = [];
    /** @var integer */
    protected $virtualLimit;

    /**
     * @param string $rrule
     * @param \DateTimeInterface $after
     * @param \DateTimeInterface $before
     * @param bool $inc Include date if it equals $after or $before.
     * @param string $timeZone Include date if it equals $after.
     * @param integer $virtualLimit Include date if it equals $after.
     */
    public function __construct(string $rrule, \DateTimeInterface $after = null, \DateTimeInterface $before = null, $inc = true, $timeZone = 'Europe/Moscow', $virtualLimit = 10) {
        $this->rrule        = $rrule;
        $this->after        = $after;
        $this->before       = $before;
        $this->inc          = $inc;
        $this->virtualLimit = $virtualLimit;
        $this->timeZone     = $timeZone;

        $rule = new Rule(
                $this->rrule, $this->after, $this->before, $this->timeZone
        );

        $transformerConfig = new ArrayTransformerConfig();
        $transformerConfig->enableLastDayOfMonthFix();
        $transformerConfig->setVirtualLimit($this->virtualLimit);

        $transformer = new ArrayTransformer($transformerConfig);
        $this->dates = $transformer->transform($rule)->startsBetween($this->after, $this->before, $this->inc)->toArray();
    }

    /**
     * Passes if $date not in dates array
     *
     * {@inheritdoc}
     */
    public function test(\DateTimeInterface $date) {
        foreach ($this->dates as $dt) {
            $exStart  = $dt->getStart();
//            $valStart = $date->getStart();
//            $interval = $exStart->diff($date);
            if ($exStart == $date) {
                return false;
            }
        }
        return true;
    }

//    protected function _filterDates(array $dates, array $exceptions) {
//        $resDates = array_filter($dates, function($val) use ($exceptions) {
//            foreach ($exceptions as $ex) {
//                $exStart  = $ex->getStart();
//                $valStart = $val->getStart();
//                if ($exStart == $valStart) {
//                    return false;
//                }
//            }
//            return true;
//        });
//        return $resDates;
//    }

}
