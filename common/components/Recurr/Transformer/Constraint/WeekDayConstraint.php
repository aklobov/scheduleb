<?php

/*
 * Copyright 2014 Shaun Simmons
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace common\components\Recurr\Transformer\Constraint;

use Recurr\Transformer\Constraint;
use Recurr\DateUtil;
use Recurr\Weekday;

class WeekDayConstraint extends Constraint
{

    protected $stopsTransformer = false;

    /** @var \Recurr\Weekday[] */
    protected $weekdays;

    /**
     * @param \DateTimeInterface $after
     * @param bool               $inc Include date if it equals $after.
     */
    public function __construct(array $weekdays)
    {
        foreach ($weekdays as $wd){
            $tmp = new Weekday($wd, 1);
            $this->weekdays[] = $tmp->weekday;
        }
    }

    /**
     * Passes if $date is after $after
     *
     * {@inheritdoc}
     */
    public function test(\DateTimeInterface $date)
    {
        $dayofweek = DateUtil::getDayOfWeek($date);
        return !in_array($dayofweek, $this->weekdays);
    }
}
