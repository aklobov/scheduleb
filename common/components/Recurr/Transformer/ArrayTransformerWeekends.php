<?php

/*

 */

namespace common\components\Recurr\Transformer;

use Recurr\Transformer\ArrayTransformer;
use Recurr\Transformer\ArrayTransformerConfig;
use Recurr\Rule;
use Recurr\Transformer\ConstraintInterface;
use Recurr\RecurrenceCollection;
use Recurr\DateUtil;

/**
 * ArrayTransformerWeekends создает массив событий из recurr правила.
 * Если событие выпадает на выходной день недели, то переносит его
 * на предыдущую пятницу или следующий понедельник.
 * TODO Ввести учёт праздничных дней. Список обновлять из сети Интернет. Админу уведомление.
 * TODO Не учитывает перенос рабочих дней на выходные.
 * 
 * @author fistashkin
 */
class ArrayTransformerWeekends extends ArrayTransformer {
    /*
     * Transform directions
     */

    const DIRECTION_FUTURE = 'future';
    const DIRECTION_PAST   = 'past';

    /**
     * Transform direction
     * @var string
     */
    protected $direction;

    /**
     *
     * 0 = Monday
     * 1 = Tuesday
     * 2 = Wednesday
     * 3 = Thursday
     * 4 = Friday
     * 5 = Saturday
     * 6 = Sunday 
     * Выходные дни: суббота (5) и воскресенье (6)
     * 
     * @var []|null 
     */
    protected $weekdays = [5, 6];

    public function __construct(ArrayTransformerConfig $config = null, string $direction = self::DIRECTION_FUTURE) {
        parent::__construct($config);
        $this->direction = $direction;
    }

    public function transform(Rule $rule, ConstraintInterface $constraint = null, $countConstraintFailures = true) {
        $recurrences = parent::transform($rule, $constraint, $countConstraintFailures);
        $res         = [];
        foreach ($recurrences as $recurr) {
            $dayofweek = DateUtil::getDayOfWeek($recurr->getStart());
            if (in_array($dayofweek, $this->weekdays)) {
                if ($this->direction == self::DIRECTION_FUTURE) {
                    $delta     = 7 - $dayofweek;
                    $interval  = new \DateInterval('P' . $delta . 'D');
                    $startDate = $recurr->getStart();
                    $startDate->add($interval);
                    $recurr->setStart($startDate);
                    $endDate   = $recurr->getEnd();
                    $endDate->add($interval);
                    $recurr->setEnd($endDate);
                }
                else {
                    $delta     = $dayofweek - 4;
                    $interval  = new \DateInterval('P' . $delta . 'D');
                    $startDate = $recurr->getStart();
                    $startDate->sub($interval);
                    $recurr->setStart($startDate);
                    $endDate   = $recurr->getEnd();
                    $endDate->sub($interval);
                    $recurr->setEnd($endDate);
                }
            }
            $res[] = $recurr;
        }
        return new RecurrenceCollection($res);
    }

}
