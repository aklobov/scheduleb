<?php
namespace common\components;

/**
 * Run console command from web app
 * https://github.com/yiisoft/yii2/issues/1764
 * @package app\components
 */
use Yii;

class ConsoleAppHelper {

    public static function runAction($route, $params = [], $controllerNamespace = null) {

        $oldApp = Yii::$app;

        // fcgi doesn't have STDIN and STDOUT defined by default
        defined('STDIN') or define('STDIN', fopen('php://stdin', 'r'));
        defined('STDOUT') or define('STDOUT', fopen('php://stdout', 'w'));

        /** @noinspection PhpIncludeInspection */
        $config = yii\helpers\ArrayHelper::merge(
                require(Yii::getAlias('@common/config/base.php')), 
                require(Yii::getAlias('@common/config/console.php')), 
                require(Yii::getAlias('@console/config/console.php'))
        );

        $consoleApp = new \yii\console\Application($config);

        if (!is_null($controllerNamespace)) {
            $consoleApp->controllerNamespace = $controllerNamespace;
        }

        try {

            // use current connection to DB
            Yii::$app->set('db', $oldApp->db);

            ob_start();

            $exitCode = $consoleApp->runAction(
                    $route, array_merge(func_get_arg(1), ['interactive' => false, 'color' => false])
            );

            $result = ob_get_clean();
            echo $result;
            Yii::trace($result, 'console');
        }
        catch (\Exception $e) {

            Yii::warning($e->getMessage() . ' in ' . $e->getFile() . ' on line ' . $e->getLine(), 'console');

            $exitCode = 1;
        }

        Yii::$app = $oldApp;

        return $exitCode;
    }

}
