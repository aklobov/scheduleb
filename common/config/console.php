<?php

return [
    'components' => [
        'urlManager' => [
            'class'     => 'yii\web\UrlManager',
            'hostInfo' => env('FRONTEND_HOST_INFO'). '/',
            'baseUrl' => '/',
            'scriptUrl' => '/index.php',
            'enablePrettyUrl' => true,
            'showScriptName' => false,
//            'urlFormat' => 'path',
        ]
    ],
];
