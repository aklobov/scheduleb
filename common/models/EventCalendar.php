<?php

namespace common\models;

use yii\helpers\ArrayHelper;

/**
 * EventCalendar populates event dates for calendar view
 *
 * @author fistashkin
 */
class EventCalendar extends Event {

    public function fields() {
        return [
            'title',
            'start' => 'start_date',
            'url'   => function() {
                return '#';
            },
            'color' => function() {
                return '#ff0000';
            },
            'description' => function($model){
                $orgs = ArrayHelper::getColumn($model->orgs, 'title');
                //https://fullcalendar.io/docs/eventRender
                return implode('<br>', $orgs);
            }
        ];
    }

}
