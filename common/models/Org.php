<?php

namespace common\models;

use Yii;
use common\models\query\OrgQuery;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use common\validators\OrgTaxValidator;

/**
 * This is the model class for table "{{%org}}".
 *
 * @property string $id
 * @property string $title Название
 * @property string $owner_id Ссылка на создателя
 * @property string $group_id Ссылка на группу
 * @property string $org_type Организационно-правовая форма
 * @property string $tax_type Система налогообложения
 * @property string $hired_labour Использование наемного труда
 * @property boolean $is_vehicles_tax Плательшик транспортного налога
 * @property boolean $is_property_tax Плательшик налога на имущество
 * @property int $is_default Организация с меткой default обслуживаются после окончания платных услуг
 * @property string $status
 * @property string $description Описание
 * @property string $created_at
 * @property string $updated_at
 * @property string $created_by
 * @property string $updated_by
 *
 * @property EventException[] $eventExceptions
 * @property User $createdByUser
 * @property Group $group
 * @property User $owner
 * @property User $updatedByUser
 * @property RelEventOrg[] $relEventOrgs
 * @property RelUserOrg[] $relUserOrgs
 */
class Org extends \yii\db\ActiveRecord {

    /**
     * Организационно-правовая форма
     * Поле org_type
     */
    const TYPE_ORG_ENTITY = 'Юридическое лицо';
    const TYPE_ORG_IP     = 'Индивидуальный предприниматель';

    /**
     * Система налогообложения
     * Поле tax_type
     */
    const TYPE_TAX_COMMON = 'Общая система налогообложения';
    const TYPE_TAX_USN    = 'Упрощенная система налогообложения';
    const TYPE_TAX_ENVD   = 'Единый налог на вмененный доход';
    const TYPE_TAX_ESHN   = 'Единый сельскохозяйственный налог';
    const TYPE_TAX_PATENT = 'Патентная система налогообложения';

    /**
     * Состояние
     * Поле status
     */
    const STATUS_ACTIVE   = 'active';
    const STATUS_TRASHED  = 'trashed';
    const STATUS_ARCHIVED = 'archived';
    const STATUS_DELETED  = 'deleted';

    /**
     * Наёмный труд
     * Поле hired_labour
     */
    const HIRED_LABOUR_YES = 'Есть';
    const HIRED_LABOUR_NO  = 'Нет';

    /**
     * Представление $tax_type в виде массива
     * @var _tax_type
     */
    private $_tax_type;

    /**
     * {@inheritdoc}
     */
    public static function tableName() {
        return '{{%org}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules() {
        $is_default_user_org_set = static::find()->isUserDefaultOrgSet();
        return [
            [['title', 'org_type'], 'required'],
            ['tax_type', OrgTaxValidator::class],
            ['tax_type_', 'in', 'range' => self::getAllTaxTypes(), 'allowArray' => TRUE],
//            ['tax_type', 'filter', 'filter' => function($val) {
//                    return is_array($val) ? implode(',', $val) : $val;
//                }],
//            ['tax_type', 'required' , 'whenClient' =>self::whenClientValidationTaxType()],
            ['tax_type', 'safe'],
            ['is_default', 'default', 'value' => !$is_default_user_org_set],
            [['is_vehicles_tax', 'is_property_tax'], 'default', 'value' => TRUE],
            [['is_default', 'is_vehicles_tax', 'is_property_tax'], 'boolean'],
            [['owner_id', 'group_id', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
            ['owner_id', 'default', 'value' => getMyId()],
            ['status', 'default', 'value' => self::STATUS_ACTIVE],
            ['status', 'in', 'range' => [self::STATUS_ACTIVE, self::STATUS_DELETED]],
            ['hired_labour', 'default', 'value' => self::HIRED_LABOUR_NO],
            ['hired_labour', 'in', 'range' => array_keys(self::getAllHiredLabourTypes())],
            ['org_type', 'in', 'range' => self::getAllOrgTypes()],
//            ['org_type', 'default', 'value' => self::TYPE_ORG_ENTITY],
            [['group_id'], 'exist', 'skipOnError' => true, 'targetClass' => Group::className(), 'targetAttribute' => ['group_id' => 'id']],
            [['owner_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['owner_id' => 'id']],
            [['title', 'description'], 'filter', 'filter' => 'trim', 'skipOnArray' => true],
            [['title'], 'string', 'max' => 128, 'min' => 1],
            [['description'], 'string', 'max' => 1024],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels() {
        return [
            'id'                   => 'ID',
            'title'                => 'Название',
            'owner_id'             => 'Создатель',
            'group_id'             => 'Группа',
            'org_type'             => 'Организационно-правовая форма',
            'tax_type'             => 'Система налогообложения',
            'tax_type_'            => 'Система налогообложения',
            'hired_labour'         => 'Наёмный труд',
            'is_vehicles_tax'      => 'Транспортный налог',
            'is_property_tax'      => 'Налог на имущество',
            'is_default'           => 'Основная', //Организация с меткой default обслуживается после окончания платных услуг
            'description'          => 'Описание',
            'status'               => 'Состояние',
            'remind_via_email'     => 'Напоминания по e-mail',
            'remind_via_messenger' => 'Напоминания через мессенджеры',
            'created_at'           => 'Создано',
            'updated_at'           => 'Изменено',
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            TimestampBehavior::class,
            BlameableBehavior::class,
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEventExceptions() {
        return $this->hasMany(EventException::className(), ['org_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedByUser() {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGroup() {
        return $this->hasOne(Group::className(), ['id' => 'group_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOwner() {
        return $this->hasOne(User::className(), ['id' => 'owner_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedByUser() {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReminders() {
        return $this->hasMany(RelEventOrg::className(), ['org_id' => 'id']);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReminder($event_id) {
        return $this->hasOne(RelEventOrg::className(), ['org_id' => 'id'])
                ->where(['event_id' => $event_id]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEvents() {
        return $this->hasMany(Event::class, ['id' => 'event_id'])
                        ->viaTable('{{%rel_event_org}}', ['org_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRelUserOrgs() {
        return $this->hasMany(RelUserOrg::className(), ['org_id' => 'id']);
    }

    /**
     * {@inheritdoc}
     * @return OrgQuery the active query used by this AR class.
     */
    public static function find() {
        return new OrgQuery(get_called_class());
    }

    public static function getAllOrgTypes() {
        $lst = [
            self::TYPE_ORG_ENTITY => self::TYPE_ORG_ENTITY,
            self::TYPE_ORG_IP     => self::TYPE_ORG_IP,
        ];
        return $lst;
    }

    public static function getAllTaxTypes() {
        $lst = [
            self::TYPE_TAX_COMMON => self::TYPE_TAX_COMMON,
            self::TYPE_TAX_ENVD   => self::TYPE_TAX_ENVD,
            self::TYPE_TAX_ESHN   => self::TYPE_TAX_ESHN,
            self::TYPE_TAX_PATENT => self::TYPE_TAX_PATENT,
            self::TYPE_TAX_USN    => self::TYPE_TAX_USN,
        ];
        return $lst;
    }

    public static function getAllHiredLabourTypes() {
        $lst = [
            self::HIRED_LABOUR_NO  => 'Нет наёмных работников',
            self::HIRED_LABOUR_YES => 'Есть наёмные работники',
        ];
        return $lst;
    }

//    public static function whenClientValidationTaxType() {
//        return <<<"JS"
//        function (attribute, value) {
//            org_type = $('input[name="Org[org_type]"]:checked').val();
//            //alert (org_type);
//        return 1;
//        }
//JS;
//    }

    public function getTax_Type_() {
        return strpos($this->tax_type, ',') ? explode(',', $this->tax_type) : $this->tax_type;
    }

    public function setTax_Type_($val) {
        $this->tax_type = is_array($val) ? implode(',', $val) : $val;
    }

    /**
     * Сделать организацией по умолчанию
     * @return Org
     */
    public function setDefault() {
        $res                          = NULL;
        $prev_default_org             = static::find()->getUserDefaultOrg();
        $prev_default_org->is_default = FALSE;
        if ($prev_default_org->save()) {
            $this->is_default = TRUE;
            $res              = $this->save();
        }
        return $res ? $this : NULL;
    }

    public function setDeleted() {
        $this->status = self::STATUS_DELETED;
        $res          = $this->save();
        return $res ? $this : NULL;
    }

    /**
     * Проверка условий применимости события к организации
     * @param \common\models\Event $event
     * @return boolean
     */
    public function isEventApplicable(Event $event) {
        if ($event->strict_condition) {
            $res = ($this->org_type == $event->org_type) &&
                    ($this->tax_type == $event->tax_type) &&
                    ($this->hired_labour == $event->hired_labour);
        }
        else {
            $res = $event->isInOrgType($this->org_type) ||
                    $event->isInTaxType($this->tax_type) ||
                    $event->isInHiredLabour($this->hired_labour);
        }
        return $res;
    }

}
