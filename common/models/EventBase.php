<?php

namespace common\models;

use Recurr\Rule;
use Recurr\Frequency;

/**
 * EventBase
 *
 * @author fistashkin
 */
class EventBase extends \yii\db\ActiveRecord {

    /**
     * Период повтора
     * 
     * Поле frequency
     */
    const FREQUENCY_NONE    = 'none';
    const FREQUENCY_MONTH   = 'month';
    const FREQUENCY_QUARTER = 'quarter';
    const FREQUENCY_YEAR    = 'year';

    public static function getAllFrequencies($freq = null) {
        $lst = [
            self::FREQUENCY_NONE    => 'Не повторять',
            self::FREQUENCY_MONTH   => 'Ежемесячно',
            self::FREQUENCY_QUARTER => 'Раз в три месяца',
            self::FREQUENCY_YEAR    => 'Ежегодно',
        ];
        return array_key_exists($freq, $lst) ? $lst[$freq] : $lst;
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels() {
        return [
            'id'          => 'Номер',
            'start_date'  => 'Дата начала',
            'start_date_' => 'Дата начала',
            'end_date'    => 'Дата окончания',
            'end_date_'   => 'Дата окончания',
            'frequency'   => 'Периодичность', //'Период повтора',
        ];
    }

    public function getStart_Date_() {
        if (empty($this->owner->start_date)) {
            return null;
        }
        $d = \DateTime::createFromFormat('Y-m-d', $this->owner->start_date);
        return $d->format('d.m.Y');
    }

    /**
     * 
     * @return \DateTime|null
     */
    public function getStart_Date_Raw() {
        if (empty($this->owner->start_date)) {
            return null;
        }
        $d = \DateTime::createFromFormat('Y-m-d', $this->owner->start_date);
        return $d;
    }

    public function setStart_Date_($val) {
        if (empty($val)) {
            $this->owner->start_date = null;
            return null;
        }
        $d                       = \DateTime::createFromFormat('d.m.Y', $val);
        $this->owner->start_date = $d->format('Y-m-d');
    }

    /**
     * 
     * @return \DateTime|null
     */
    public function getCurr_Date_Raw() {
        if (empty($this->curr_date)) {
            return null;
        }
        $d = \DateTime::createFromFormat('Y-m-d', $this->curr_date);
        return $d;
    }

    /**
     * Вспомогательная функция
     * @param Rule $rule
     * @param type $frequency
     * @return Rule
     */
    protected static function setFrequency_(Rule $rule, $frequency) {
        $frequency = strtolower($frequency);
        if (array_key_exists($frequency, self::getAllFrequencies())) {
            switch ($frequency) {
                case self::FREQUENCY_MONTH:
                    $rule->setFreq(Frequency::MONTHLY);
                    break;
                case self::FREQUENCY_QUARTER:
                    $rule->setFreq(Frequency::MONTHLY);
                    $rule->setInterval(3);
                    break;
                case self::FREQUENCY_YEAR:
                    $rule->setFreq(Frequency::YEARLY);
                    break;
                default:
                    $rule->setFreq(Frequency::YEARLY);
                    $rule->setCount(1);
            }
        }
        return $rule;
    }

    /**
     * 
     * @return string
     */
    public function getRecurrence_Pattern() {
        if (!empty($this->start_date) && !empty($this->frequency)) {
            $rule = new Rule;
            $rule = self::setFrequency_($rule, $this->frequency);
            return $rule->getString();
        }
        else {
            return '';
        }
    }

    /**
     * 
     * @return \common\models\RelEventOrg[]|array|null
     */
    public function getRelEventOrgs() {
        return $this->hasMany(Event::class, ['event_id' => 'id']);
    }

}
