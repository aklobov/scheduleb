<?php

namespace common\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\NotificationLog;

/**
 * NotificationLogSearch represents the model behind the search form of `common\models\NotificationLog`.
 */
class NotificationLogSearch extends NotificationLog {

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
            [['id', 'org_id', 'event_id', 'user_id', 'reminder_id', 'created_at',], 'integer'],
            [['type', 'status', 'action'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
        $query = NotificationLog::find()->where(['user_id' => getMyId()]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id'          => $this->id,
            'org_id'      => $this->org_id,
            'event_id'    => $this->event_id,
            'reminder_id' => $this->reminder_id,
            'user_id'     => $this->user_id,
            'status'      => $this->status,
            'type'        => $this->type,
            'created_at'  => $this->created_at,
        ]);

        return $dataProvider;
    }

}
