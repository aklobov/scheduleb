<?php

namespace common\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\RelEventOrg;

/**
 * RelEventOrgSearch represents the model behind the search form of `common\models\RelEventOrg`.
 */
class RelEventOrgSearch extends RelEventOrg {

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
            [['id', 'org_id', 'event_id', 'remind_before', 'show_before'], 'integer'],
            ['status', 'in', 'range' => RelEventOrg::getAllStatuses],
            ['event_title', 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
        $query = RelEventOrg::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id'            => $this->id,
            'org_id'        => $this->org_id,
            'event_id'      => $this->event_id,
            'remind_before' => $this->remind_before,
            'show_before'   => $this->show_before,
//            'is_vehicles_tax' => $this->is_vehicles_tax,
//            'is_property_tax' => $this->is_property_tax,
            'status'        => $this->status,
//            'created_at'    => $this->created_at,
//            'updated_at'    => $this->updated_at,
//            'created_by'    => $this->created_by,
//            'updated_by'    => $this->updated_by,
        ]);

        return $dataProvider;
    }

}
