<?php

namespace common\models\query;

use common\models\Event;
use common\models\RelEventOrg;

/**
 * This is the ActiveQuery class for [[\common\models\RelEventOrg]].
 *
 * @see \common\models\RelEventOrg
 */
class RelEventOrgQuery extends \yii\db\ActiveQuery {
    /* public function active()
      {
      return $this->andWhere('[[status]]=1');
      } */

    /**
     * {@inheritdoc}
     * @return \common\models\RelEventOrg[]|array
     */
    public function all($db = null) {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return \common\models\RelEventOrg|array|null
     */
    public function one($db = null) {
        return parent::one($db);
    }

    /**
     * Перечень напоминаний
     * @param string $date Дата проверки в формате Y-m-d ('2019-12-31')
     * @return \common\models\RelEventOrg[]|array
     */
    public function thisDay(string $date = NULL) {
        if (is_null($date)) {
            $date = date('Y-m-d');
        }
        return $this
                        ->select([
                            '{{%rel_event_org}}.*',
                            '{{%event}}.curr_date as event_date',
                            '{{%event}}.end_date as end_date',
                            'DATE_ADD({{%event}}.curr_date, INTERVAL -remind_before DAY) as remind_date',
                        ])
                        ->joinWith('event')
                        ->where('({{%event}}.end_date > :date) AND (DATE_ADD({{%event}}.curr_date, INTERVAL -remind_before DAY) = :date)')
                        ->addParams([':date' => $date]);
//                ->where([
////                    'YEAR(event_date) = YEAR(CURDATE())',
////                    'event_date > CURDATE()',
////                    '(DATEDIFF(remind_date, CURDATE()) < 100',
////                    '(DATEDIFF(remind_date, CURDATE()) > 0'
//                ]);
    }

    /**
     * Earlier $date
     * @param string $date
     * @return type
     */
    public function orEarlier(string $date = NULL) {
        if (is_null($date)) {
            $date = date('Y-m-d');
        }
        return $this
                        ->joinWith('event')
                        ->orWhere('({{%event}}.end_date > :date) AND (DATE_ADD({{%event}}.curr_date, INTERVAL -remind_before DAY) < :date)')
                        ->addParams([':date' => $date]);
    }

    /**
     * Перечень просроченных событий
     * @param string $date Дата проверки в формате Y-m-d ('2019-12-31')
     * @return \common\models\RelEventOrg[]|array
     */
    public function thisDayOutdated(string $date = NULL, array $statuses = [RelEventOrg::STATUS_ACTIVE, RelEventOrg::STATUS_CANCELED]) {
        if (is_null($date)) {
            $date = date('Y-m-d');
        }

        return $this
                        ->select([
                            '{{%rel_event_org}}.*',
                            '{{%event}}.curr_date as event_date',
                            '{{%event}}.end_date as end_date',
                            '{{%event}}.previous_date as previous_date',
                            'DATE_ADD({{%event}}.curr_date, INTERVAL -remind_before DAY) as remind_date',
                        ])
                        ->joinWith('event')
                        ->where('({{%event}}.end_date >= :date) AND ({{%event}}.curr_date > :date) AND ({{%event}}.previous_date < :date) AND {{%rel_event_org}}.status IN (' . implode(',', $statuses) . ')')
                        ->addParams([':date' => $date]);
    }

    /**
     * Список напоминаний, для которых подходит срок наступления события
     */
    public function comingSoon() {
        return $this
                        ->joinWith('event ev')
                        ->andWhere(['not in', '{{%rel_event_org}}.status', [RelEventOrg::STATUS_FINISHED, RelEventOrg::STATUS_CANCELED]])
                        ->andWhere('DATEDIFF(curr_date, CURDATE()) < remind_before')
                        ->andWhere('curr_date > CURDATE()');
    }

//    public function allByUser($user_id = null){
//        $user_id = $user_id ? $user_id : getMyId();
//        return $this
//                ->
//                ->where()
//    }
}
