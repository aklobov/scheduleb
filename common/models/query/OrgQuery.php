<?php

namespace common\models\query;
use Yii;


/**
 * This is the ActiveQuery class for [[Org]].
 *
 * @see Org
 */
class OrgQuery extends \yii\db\ActiveQuery {
    /* public function active()
      {
      return $this->andWhere('[[status]]=1');
      } */

    /**
     * {@inheritdoc}
     * @return Org[]|array
     */
    public function all($db = null) {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Org|array|null
     */
    public function one($db = null) {
        return parent::one($db);
    }

    /**
     * Организации пользователя
     * @return Org|array|null
     */
    public function allByUser($user_id = null, $db = null) {
        $user_id = $user_id ? $user_id : getMyId();
        return $this->andWhere(['owner_id' => $user_id]);
    }
    
    /**
     * Организации пользователя
     * @return Org|array|null
     */
    public function oneByUser($user_id = null, $db = null) {
        $user_id = $user_id ? $user_id : getMyId();
        return $this->andWhere(['owner_id' => $user_id])->one();
    }

    /**
     * Число организаций пользователя
     * @return integer
     */
    public function countByUser($user_id = null, $db = null) {
        return $this->allByUser($user_id, $db)->count();
    }
    
    /**
     * Проверка наличия организации по умолчанию
     * @return boolean
     */
    public function isUserDefaultOrgSet($user_id = null, $db = null) {
        return (bool)$this->allByUser($user_id, $db)->andWhere(['is_default' => TRUE])->count();
    }
    
    /**
     * Проверка наличия организации по умолчанию
     * @return Org|null
     */
    public function getUserDefaultOrg($user_id = null, $db = null) {
        return $this->allByUser($user_id, $db)->andWhere(['is_default' => TRUE])->one();
    }
    
}
