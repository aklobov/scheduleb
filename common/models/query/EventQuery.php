<?php

namespace common\models\query;

use common\models\Org;
use common\models\Event;

/**
 * This is the ActiveQuery class for [[Event]].
 *
 * @see Event
 */
class EventQuery extends \yii\db\ActiveQuery {
    /* public function active()
      {
      return $this->andWhere('[[status]]=1');
      } */

    /**
     * {@inheritdoc}
     * @return Event[]|array
     */
    public function all($db = null) {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Event|array|null
     */
    public function one($db = null) {
        return parent::one($db);
    }

    /**
     * Фильтр событий, применимых к организации
     * @return Event|array|null
     */
    public function applicable(Org $org = null, $type = Event::TYPE_DEFAULT) {
        if ($type = Event::TYPE_DEFAULT && !is_null($org)) {
            return $this
                            ->_applicableDefaults($org)
                            ->andWhere(['status' => Event::STATUS_ACTIVE]);
        }
        return $this
                        ->_applicableCustoms()
                        ->andWhere(['status' => Event::STATUS_ACTIVE]);
    }

    //События пользователя
    protected function _applicableCustoms() {
        return $this->andWhere([
                    'type'       => Event::TYPE_CUSTOM,
                    'created_by' => getMyId(),
        ]);
    }

    /**
     * Фильтр событий, применимых к организации
     * @return Event|array|null
     */
    protected function _applicableDefaults(Org $org) {
        /**
         * Четыре типа событий
         * 1 Стандартные события. Строгое соответствие параметров
         * 2 Стандартные события. Совпадение одного из параметров при нестрогом соответствии
         * 3 Пользовательские события. Строгое соответствие параметров
         * 4 Пользовательские события. Совпадение одного из параметров при нестрогом соответствии
         */
        $this->andWhere([
//            'or',
//            [
            'type' => Event::TYPE_DEFAULT,
//            ],
//            [
//                'and',
//                [
//                    'type'       => Event::TYPE_CUSTOM,
//                    'created_by' => getMyId(),
//                ]
//            ]
        ]);
//        $taxTypes = explode(',', $org->tax_type);
        if (is_array($org->tax_type_)) {
            $ORCondition = [
                'or',
            ];
            foreach ($org->tax_type_ as $taxType) {
                $ORCondition[] = ['like', 'tax_type', $taxType];
            }
            $ORCondition[] = ['like', 'org_type', $org->org_type];
            $ORCondition[] = ['like', 'hired_labour', $org->hired_labour];
//            $ORCondition[] = ['is_property_tax' => $org->is_property_tax];
//            $ORCondition[] = ['is_vehicles_tax' => $org->is_vehicles_tax];
            $condition2    = [
                'and',
                ['strict_condition' => FALSE],
                $ORCondition
            ];
        }
        else {
            $condition2 = [
                'and',
                ['strict_condition' => FALSE],
                [
                    'or',
                    ['like', 'org_type', $org->org_type],
                    ['like', 'tax_type', $org->tax_type],
                    ['like', 'hired_labour', $org->hired_labour],
//                    ['is_property_tax' => $org->is_property_tax],
//                    ['is_vehicles_tax' => $org->is_vehicles_tax],
                ]
            ];
        }

        //Строгое соответствие
        $condition3   = [
            'and',
            ['strict_condition' => TRUE],
        ];
        $condition3[] = [
            'or',
            ['org_type' => $org->org_type],
            ['org_type' => ''],
            ['org_type' => NULL]
        ];
        $condition3[] = [
            'or',
            ['hired_labour' => $org->hired_labour],
            ['hired_labour' => ''],
            ['hired_labour' => NULL]
        ];
        if (!empty($org->tax_type)) {
            if (is_array($org->tax_type_)) {
                $taxTypeCondition = [
                    'and'
                ];
                foreach ($org->tax_type_ as $taxType) {
                    $taxTypeCondition[] = ['like', 'tax_type', $taxType];
                }
                $condition3[] = [
                    'or',
                    $taxTypeCondition,
                    ['tax_type' => ''],
                    ['tax_type' => NULL]
                ];
            }
            else {
                $condition3[] = ['tax_type' => $org->tax_type];
            }
        }
        if (!empty($org->is_property_tax)) {
            $condition3[] = [
                'or',
                ['is_property_tax' => $org->is_property_tax],
                ['is_property_tax' => ''],
                ['is_property_tax' => NULL]
            ];
        }
        if (!empty($org->is_vehicles_tax)) {
            $condition3[] = [
                'or',
                ['is_vehicles_tax' => $org->is_vehicles_tax],
                ['is_vehicles_tax' => ''],
                ['is_vehicles_tax' => NULL]
            ];
        }
//        if (!empty($org->is_vehicles_tax)) {
//            $condition3['is_vehicles_tax'] = $org->is_vehicles_tax;
//        }
//        TODO Проверить корректность замены andWhere на andFilterWhere
//        return $this->andWhere([
        return $this->andFilterWhere([
                    'or',
//                    [
//                        'strict_condition' => TRUE,
//                        'org_type'         => $org->org_type,
//                        'tax_type'         => $org->tax_type,
//                        'hired_labour'     => $org->hired_labour,
//                        'is_property_tax'  => $org->is_property_tax,
//                        'is_vehicles_tax'  => $org->is_vehicles_tax,
//                    ],
                    $condition2,
                    $condition3,
        ]);
    }

//    public function byType(string $type = Event::TYPE_DEFAULT) {
//        $this->andWhere([
//            'org_id' => $org_id,
//        ]);
//        return $this->innerJoin('{{%event}}', '{{%event}}.id = {{%rel_event_org}}.event_id')
//                        ->andWhere([
//                            '{{%event}}.type' => $type,
//        ]);
//    }
//
//    public function byAction(int $org_id, string $action = Event::ACTION_REPORT) {
//        return $this->events($org_id, Event::TYPE_DEFAULT)
//                ->andWhere([
//                    '{{%event}}.action' => $action,
//        ]);
//    }

    /**
     * Перечень событий с датой ранее заданной
     * @param string $date Дата проверки в формате Y-m-d ('2019-12-31')
     * @return \common\models\Event[]|array
     */
    public function overdue($date = NULL) {
        if (is_null($date)) {
            $date = date('Y-m-d');
        }
        return $this->where('curr_date < :date')
                        ->addParams([':date' => $date]);
    }

    /**
     * Перечень событий
     * @param string $date Дата в формате Y-m-d ('2019-12-31')
     * @return \common\models\Event[]|array
     */
    public function thisDay($date = NULL) {
        if (is_null($date)) {
            $date = date('Y-m-d');
        }
        return $this->andWhere('curr_date = :date')
                        ->addParams([':date' => $date]);
    }

    /* Earlier $date
     * @param string $date
     * @return type
     */

    public function orEarlier(string $date = NULL) {
        if (is_null($date)) {
            $date = date('Y-m-d');
        }
        return $this->orWhere('curr_date < :date')
                        ->addParams([':date' => $date]);
    }

//    public function comingSoon(){
//        
//    }
}
