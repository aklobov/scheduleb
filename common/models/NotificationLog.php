<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "{{%notification_log}}".
 *
 * @property string $id
 * @property string $event_date
 * @property string $type
 * @property string $event_id
 * @property string $user_id Ссылка на профиль пользователя
 * @property string $org_id
 * @property string $reminder_id
 * @property string $title
 * @property string $message Служебные сообщения
 * @property boolean $is_success_email
 * @property string $created_at
 *
 * @property Event $event
 * @property Org $org
 * @property RelEventOrg $reminder
 * @property User $user
 */
class NotificationLog extends \yii\db\ActiveRecord {

    //Type
    const TYPE_EVENT  = 'event';
    const TYPE_REMIND = 'remind';
    //Transport
    const TRANSPORT_EMAIL = 'email';
    const TRANSPORT_VIBER = 'viber';

//    const TYPE_OUTDATED = 'outdated';

    public static function getAllTypes() {
        $lst = [
            self::TYPE_EVENT  => 'Событие',
            self::TYPE_REMIND => 'Напоминание',
//            self::TYPE_OUTDATED => 'Просроченное событие',
        ];
        return $lst;
//        return in_array($type, $lst) ? $lst[$type] : $lst;
    }

    /**
     * Пользовательские отметки о состоянии
     * Поле status
     */
    const STATUS_ACTIVE                = 'active'; //Отметка по умолчанию. Событие ожидает действий пользователя
    const STATUS_IN_WORK               = 'in_work'; //Пользователь отметил, что ведет работу над событием
    const STATUS_FINISHED              = 'finished'; //Отметка пользователя о сдаче отчёта/платежа
    const STATUS_CANCELED              = 'canceled'; //Отчёт нужен, но был просрочен. Отменен системой.
    const STATUS_EXPIRED               = 'expired'; //Событие пропущено.
    const STATUS_EXPIRED_AND_CONFIRMED = 'expired_and_confirmed'; //Событие пропущено, пользователь поставил отметку, что ознакомлен с фактом

//    const STATUS_EXPIRED  = 'Просрочено'; //Дата события прошла, 

    public static function getAllStatuses($status = null) {

        $lst = [
            self::STATUS_CANCELED              => 'Пропущено', //Отменено
            self::STATUS_ACTIVE                => 'Активно',
            self::STATUS_IN_WORK               => 'В работе',
            self::STATUS_FINISHED              => 'Завершено',
            self::STATUS_EXPIRED               => 'Просрочено',
            self::STATUS_EXPIRED_AND_CONFIRMED => 'Просрочено и подтверждено',
        ];
        return array_key_exists($status, $lst) ? $lst[$status] : $lst;
    }

//    const SCENARIO_CONSOLE_NOTIFICATION = 'console_notification';

    /**
     * {@inheritdoc}
     */
    public static function tableName() {
        return '{{%notification_log}}';
    }

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            [
                'class'              => TimestampBehavior::class,
                'updatedAtAttribute' => false
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
            [['event_date', 'remind_date', 'action', 'transport'], 'safe'],
            [['type', 'event_id'], 'required'],
            [['type'], 'string'],
            [['event_id', 'user_id', 'org_id', 'reminder_id'], 'integer'],
            ['is_success_email', 'boolean'],
            [['title'], 'string', 'max' => 250],
            [['message'], 'string', 'max' => 16383],
            ['status', 'in', 'range' => array_keys(self::getAllStatuses())],
            ['transport', 'default', 'value' => self::TRANSPORT_EMAIL],
            [['event_id'], 'exist', 'skipOnError' => true, 'targetClass' => Event::className(), 'targetAttribute' => ['event_id' => 'id']],
            [['org_id'], 'exist', 'skipOnError' => true, 'targetClass' => Org::className(), 'targetAttribute' => ['org_id' => 'id']],
            [['reminder_id'], 'exist', 'skipOnError' => true, 'targetClass' => RelEventOrg::className(), 'targetAttribute' => ['reminder_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
//    public function scenarios() {
//        $scenarios                                      = parent::scenarios();
//        $scenarios[self::SCENARIO_CONSOLE_NOTIFICATION] = [
//            'event_date',
//            'remind_date',
//            'type',
//            'transport',
//            'action',
//            'user_id',
//            'event_id',
//            'org_id',
//            'reminder_id',
//            'is_success_email',
//        ];
//        return $scenarios;
//    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels() {
        return [
            'id'               => 'ID',
            'event_date'       => 'Дата события',
            'remind_date'      => 'Дата напоминания',
            'type'             => 'Type',
            'event_id'         => 'Event ID',
            'user_id'          => 'User ID',
            'org_id'           => 'Org ID',
            'reminder_id'      => 'Reminder ID',
            'title'            => 'Title',
            'message'          => 'Message',
            'is_success_email' => 'Результат отправки email',
            'transport'        => 'Транспорт',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEvent() {
        return $this->hasOne(Event::className(), ['id' => 'event_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrg() {
        return $this->hasOne(Org::className(), ['id' => 'org_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReminder() {
        return $this->hasOne(RelEventOrg::className(), ['id' => 'reminder_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser() {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public function fields() {
        return [
            'event_date',
            'remind_date',
            'type',
            'status',
            'transport',
            'action',
            'user_id',
            'event_id',
            'org_id',
            'reminder_id',
            'is_success_email',
        ];
    }

    /**
     * {@inheritdoc}
     * @return \common\models\query\NotificationLogQuery the active query used by this AR class.
     */
    public static function find() {
        return new \common\models\query\NotificationLogQuery(get_called_class());
    }

}
