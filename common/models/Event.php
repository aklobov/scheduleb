<?php

namespace common\models;

use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use common\models\query\EventQuery;
use common\models\Org;
use common\models\Calendar;
use common\models\User;
use common\models\EventException;
//use common\validators\OrgTaxValidator;
use Recurr\Rule;
use Recurr\Frequency;
use Recurr\Transformer\ArrayTransformer;
use Recurr\Transformer\ArrayTransformerConfig;
use common\components\Recurr\Transformer\Constraint\ExceptionRRuleConstraint;
use common\components\Recurr\Transformer\ArrayTransformerWeekends;

//use yii\base\Event as YiiEvent;
//YiiEvent::on(Event::class, Event::EVENT_HAS_COME, $handler)
/**
 * This is the model class for table "{{%event}}".
 *
 * @property string $id
 * @property string $title Название события/платежа
 * @property string $type Событие стандартное или  создано пользователем
 * @property string $action
 * @property string $group_id Метка сходных событий, когда нужно выбрать одну альтернативу (Отчёт бумажный или электронный)
 * @property string $tags
 * @property string $gov_org Государственная контролирующая организация
 * @property int $is_default Показывать отчет/платеж при создании организации
 * @property string $start_date Начальная дата события
 * @property string $end_date Дата окончания события
 * @property string $frequency
 * @property string $strict_condition
 * @property boolean $is_vehicles_tax Плательшик транспортного налога
 * @property boolean $is_property_tax Плательшик налога на имущество
 * @property string $description
 * @property string $status Состояние события
 * @property string $notified_at Дата рассылки сообщения о событии
 * @property string $created_at
 * @property string $updated_at
 * @property string $created_by
 * @property string $updated_by
 *
 * @property User $createdByUser
 * @property User $updatedByUser
 * @property EventException[] $eventExceptions
 * @property RelEventOrg[] $relEventOrgs
 */
class Event extends EventBase {

    /**
     * Сценарии использования события
     * Отчёт или платеж
     * Поле action
     */
    const ACTION_REPORT  = 'report';
    const ACTION_PAYMENT = 'payment';
    const ACTION_CUSTOM  = 'custom';

    public static function getAllActions($action = null) {
        $lst = [
            self::ACTION_REPORT  => 'Отчёт',
            self::ACTION_PAYMENT => 'Платеж',
        ];
        return array_key_exists($action, $lst) ? $lst[$action] : $lst;
    }

    /**
     * Источник события
     * Стандартное или создано пользователем
     * Поле type
     */
    const TYPE_DEFAULT = 'default';
    const TYPE_CUSTOM  = 'custom';

    /**
     * Состояние
     * Поле status
     */
    const STATUS_ACTIVE   = 'active';
    const STATUS_TRASHED  = 'trashed';
    const STATUS_ARCHIVED = 'archived';
    const STATUS_DELETED  = 'deleted';
    const EVENT_HAS_COME  = 'eventHasCome';
    
    public static function getAllStatuses($status = null) {
        $lst = [
            self::STATUS_ACTIVE  => 'Активный',
            self::STATUS_DELETED  => 'Удален',
        ];
        return array_key_exists($status, $lst) ? $lst[$status] : $lst;
    }

//    public $exception_ = [
//        'start_date_'        => null,
//        'recurrence_pattern' => null,
//    ];
    //Проверка формы
//    public $exception_is_recurring;
//    public $exception_start_date = null;
//    public $exception_recurrence_pattern = null;

    /**
     * {@inheritdoc}
     */
    public static function tableName() {
        return '{{%event}}';
    }

//    public function scenarios() {
//        return [
//            self::ACTION_REPORT => [],
//            self::ACTION_PAYMENT => [],
//        ];
//    }

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
            [['title', 'start_date'], 'required'],
            [['id', 'title'], 'unique'],
//            ['title', 'unique'],
            [['start_date_', 'end_date_'], 'safe'],
            [['is_default', 'is_vehicles_tax', 'is_property_tax'], 'boolean'],
            [['id', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
            [['description'], 'string'],
            ['end_date', 'default', 'value' => date('Y-m-d', strtotime('last day of december +' . (int) Yii::$app->params['virtualEventLimit'] . ' years'))],
//            ['end_date', 'default', 'value' => '2098-12-31'],
//            [['start_date', 'end_date'], 'date', 'format' => 'php:Y-m-d'],
//            [['start_date', 'end_date'], 'filter', 'filter' => function($date_str) {
//                    $d = \DateTime::createFromFormat('d.m.Y', $date_str);
//                    return $d->format('Y-m-d');
//                }],
            [['title'], 'string', 'max' => 250],
            [['org_type', 'tax_type', 'hired_labour'], 'string'],
//            ['tax_type', OrgTaxValidator::class],
            ['tax_type_', 'in', 'range' => Org::getAllTaxTypes() + [NULL], 'allowArray' => TRUE],
            ['org_type_', 'in', 'range' => Org::getAllOrgTypes() + [''], 'allowArray' => TRUE],
            ['strict_condition', 'default', 'value' => true],
            ['strict_condition', 'boolean'],
            [['group_id'], 'string', 'max' => 48, 'skipOnEmpty' => true],
            ['frequency', 'default', 'value' => self::FREQUENCY_YEAR],
            ['frequency', 'in', 'range' => array_keys(self::getAllFrequencies())],
            [['tags', 'gov_org'], 'string', 'max' => 1024],
            ['status', 'in', 'range' => [self::STATUS_ACTIVE, self::STATUS_DELETED]],
            ['status', 'default', 'value' => self::STATUS_ACTIVE],
            ['type', 'default', 'value' => self::TYPE_DEFAULT],
            ['type', 'filter', 'filter' => function($value) {
                    //Костыль. Включает фильтр для web приложения. Переделать на сценарии (scenario).
                    if (Yii::$app instanceof \yii\web\Application) {
                        $can = Yii::$app->user->can('manageEventDefault', [
                            'model' => $this,
                        ]);
                        return $can ? $value : self::TYPE_CUSTOM;
                    }
                    else {
                        return $value;
                    }
                }],
            ['type', 'in', 'range' => [self::TYPE_CUSTOM, self::TYPE_DEFAULT]],
            ['action', 'default', 'value' => self::ACTION_REPORT],
            ['action', 'in', 'range' => [self::ACTION_REPORT, self::ACTION_PAYMENT]],
            [['title', 'description', 'tags', 'gov_org',], 'filter', 'filter' => 'trim', 'skipOnArray' => true],
//            ['type', 'default', 'value' => self::ACTION_PAYMENT, 'on' => self::ACTION_PAYMENT],
//            ['type', 'default', 'value' => self::ACTION_PAYMENT, 'on' => self::ACTION_PAYMENT],
//            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['created_by' => 'id']],
//            [['updated_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['updated_by' => 'id']],
            ['hired_labour_', 'in', 'range' => array_keys(Org::getAllHiredLabourTypes() + ['']), 'allowArray' => TRUE],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels() {
        $labels = parent::attributeLabels();
        return $labels + [
            'id'               => 'Номер',
            'title'            => 'Название',
            'type'             => 'Вид события',
            'action'           => 'Тип',
//            'handler'          => 'ссылка на функцию вычисляющую даты события',
            'group_id'         => 'Тег группы событий', //Идентификатор группы событий
            'tags'             => 'Теги',
            'gov_org'          => 'Государственная контролирующая организация',
            'is_default'       => 'Стандартный/пользовательский', //'Показывать отчет/платеж при создании организации',
            'start_date'       => 'Дата события',
            'start_date_'      => 'Дата события',
            'curr_date'        => 'Ближайшая дата',
            'end_date'         => 'Дата окончания',
            'end_date_'        => 'Дата окончания',
            'strict_condition' => 'Строгое соответствие условий',
//            'recurrence_pattern'           => 'Формула расчёта времени (Recurrence pattern)',
            'description'      => 'Описание',
            'status'           => 'Состояние события',
            'org_type_'        => 'Организационно-правовая форма',
            'org_type'         => 'Организационно-правовая форма',
            'tax_type'         => 'Система налогообложения',
            'tax_type_'        => 'Система налогообложения',
            'hired_labour_'    => 'Наёмный труд',
            'hired_labour'     => 'Наёмный труд',
            'is_vehicles_tax'  => 'Плательщик транспортного налога',
            'is_property_tax'  => 'Плательщик налога на имущество',
            'created_at'       => 'Дата создания',
            'updated_at'       => 'Дата изменения',
            'created_by'       => 'Создал',
            'updated_by'       => 'Изменил',
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            TimestampBehavior::class,
            BlameableBehavior::class,
        ];
    }

    public function getEnd_Date_() {
//        Yii::info($this->end_date);
        if (empty($this->end_date)) {
            return null;
        }
        $d = \DateTime::createFromFormat('Y-m-d', $this->end_date, new \DateTimeZone(Yii::$app->timeZone));
        return $d->format('d.m.Y');
    }

    public function setEnd_Date_($val) {
//        Yii::info('End date: ' . $this->end_date . ' are set to ' . $val);
        if (empty($val)) {
            $this->end_date = null;
            return null;
        }
        $d              = \DateTime::createFromFormat('d.m.Y', $val, new \DateTimeZone(Yii::$app->timeZone));
        $this->end_date = $d->format('Y-m-d');
    }

    public function getTax_Type_() {
        return strpos($this->tax_type, ',') ? explode(',', $this->tax_type) : $this->tax_type;
    }

    public function setTax_Type_($val) {
        $this->tax_type = is_array($val) ? implode(',', $val) : $val;
    }

    public function getOrg_Type_() {
        return strpos($this->org_type, ',') ? explode(',', $this->org_type) : $this->org_type;
    }

    public function isInTaxType($tax_type) {
        $types = $this->tax_type_;
        if (count($types) > 1) {
            $res = in_array($tax_type, $types);
        }
        else {
            $res = $tax_type == $types;
        }
        return $res;
    }

    public function setOrg_Type_($val) {
        $this->org_type = is_array($val) ? implode(',', $val) : $val;
    }

    public function isInOrgType($org_type) {
        $types = $this->org_type_;
        if (count($types) > 1) {
            $res = in_array($org_type, $types);
        }
        else {
            $res = $org_type == $types;
        }
        return $res;
    }

    public function getHired_Labour_() {
        return strpos($this->hired_labour, ',') ? explode(',', $this->hired_labour) : $this->hired_labour;
    }

    public function setHired_Labour_($val) {
        $this->hired_labour = is_array($val) ? implode(',', $val) : $val;
    }

    public function isInHiredLabour($hired_labour) {
        $types = $this->hired_labour_;
        if (count($types) > 1) {
            $res = in_array($hired_labour, $types);
        }
        else {
            $res = $hired_labour == $types;
        }
        return $res;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrgs() {
        return $this->hasMany(Org::class, ['id' => 'org_id'])
                        ->viaTable('{{%rel_event_org}}', ['event_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
//    public function getException_Start_Date() {
//        if (isset($this->eventException)) {
//            return $this->eventException->start_date_;
//        }
//        return $this->exception_['start_date_'];
//    }
//    public function setException_Start_Date($val) {
//        $this->exception_['start_date_'] = $val;
//    }
//    public function getException_Recurrence_Pattern() {
//        if (isset($this->eventException)) {
//            return $this->eventException->recurrence_pattern;
//        }
//        return $this->exception_['recurrence_pattern'];
//    }
//    public function setException_Recurrence_Pattern($val) {
//        $this->exception_['recurrence_pattern'] = $val;
//    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedByUser() {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedByUser() {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEventExceptions() {
        return $this->hasMany(EventException::className(), ['event_id' => 'id']);
    }

    /**
     * @return \common\models\EventException
     */
    public function getEventException() {
        return $this->hasOne(EventException::class, ['event_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReminders() {
        return $this->hasMany(RelEventOrg::class, ['event_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrgReminder($org_id) {
        //Not tesed
        return $this->hasOne(RelEventOrg::class, ['event_id' => 'id'])->andWhere(['org_id' => $org_id]);
    }

    /**
     * Пользователи, подписанные на событие
     * @return \yii\db\ActiveQuery
     */
    public function getUsers() {
        return $this->hasMany(User::class, ['id' => 'owner_id'])->via('orgs');
    }

    /**
     * {@inheritdoc}
     * @return EventQuery the active query used by this AR class.
     */
    public static function find() {
        return new EventQuery(get_called_class());
    }

    public function getDates(\DateTimeInterface $after = null, \DateTimeInterface $before = null, $inc = true) {
        if (empty($after)) {
//            $after = new \DateTime('first day of january', new \DateTimeZone(Yii::$app->timeZone));
            $after = new \DateTime('today', new \DateTimeZone(Yii::$app->timeZone));
        }

//        if (empty($before)) {
////            $before = new \DateTime('last day of december +' . (int) Yii::$app->params['virtualEventLimit'] . ' years', new \DateTimeZone(Yii::$app->timeZone));
//            $before = \DateTime::createFromFormat('Y-m-d', $this->end_date, new \DateTimeZone(Yii::$app->timeZone));
//        }
        //Определение минимальной даты
        $before_dates = [
            //Site Virtual Event Limit
            new \DateTime('last day of december +' . (int) Yii::$app->params['virtualEventLimit'] . ' years', new \DateTimeZone(Yii::$app->timeZone)),
            //Дата окончания события
            \DateTime::createFromFormat('Y-m-d', $this->end_date, new \DateTimeZone(Yii::$app->timeZone)),
        ];
        if (!empty($before)) {
            //Аргумент функции
            $before_dates[] = $before;
        }
        $before = min($before_dates);

        $rule = new Rule(
                $this->recurrence_pattern, \DateTime::createFromFormat('Y-m-d', $this->start_date, new \DateTimeZone(Yii::$app->timeZone)), \DateTime::createFromFormat('Y-m-d', $this->end_date, new \DateTimeZone(Yii::$app->timeZone)), Yii::$app->timeZone
        );

        $transformerConfig = new ArrayTransformerConfig();
        $transformerConfig->enableLastDayOfMonthFix();

//        $eventLimit = ($rule->getFreq() == Frequency::MONTHLY && $rule->getInterval() < 3) ? 3 * Yii::$app->params['virtualEventLimit'] : Yii::$app->params['virtualEventLimit'];
//        $transformerConfig->setVirtualLimit(Yii::$app->params['virtualEventLimit']);

        $constraint = null;
        if (($this->eventException instanceOf EventException) && !empty($this->eventException->start_date)) {
            $constraint = new ExceptionRRuleConstraint(
                    $this->eventException->recurrence_pattern, \DateTime::createFromFormat('Y-m-d', $this->eventException->start_date, new \DateTimeZone(Yii::$app->timeZone)), \DateTime::createFromFormat('Y-m-d', $this->end_date, new \DateTimeZone(Yii::$app->timeZone)), true, Yii::$app->timeZone, Yii::$app->params['virtualEventLimit']
            );
        }

        $transformer = new ArrayTransformerWeekends($transformerConfig, ArrayTransformerWeekends::DIRECTION_FUTURE);
        $dates       = $transformer->transform($rule, $constraint)->startsBetween($after, $before, $inc)->toArray();

//        return array_slice($dates, 0, (int) Yii::$app->params['virtualEventLimit']);
        return $dates;
    }

    /**
     * 
     * @param \DateTime $dtAfter
     * @param boolean $inc Включать крайние даты
     * @return \DateTime|null
     */
    public function getNext_Date_Raw(\DateTime $dtAfter = null, $inc = false) {
        if (!($dtAfter instanceOf \DateTime)) {
            $dtAfter = new \DateTime('now', new \DateTimeZone(Yii::$app->timeZone));
        }
//        $dtBefore = new \DateTime('last day of december +' . (int) Yii::$app->params['virtualEventLimit'] . ' years', new \DateTimeZone(Yii::$app->timeZone));
        $dtBefore = \DateTime::createFromFormat('Y-m-d', $this->end_date, new \DateTimeZone(Yii::$app->timeZone));
        $dates    = $this->getDates($dtAfter, $dtBefore, $inc);
        $date     = array_shift($dates);
        return empty($date) ? null : $date->getStart();
    }

    /**
     * Ближайшая дата наступления события
     * @return string|null
     */
    public function getNext_Date(\DateTime $dtAfter = null, $inc = false) {
        $date = $this->getNext_Date_Raw($dtAfter);
        return empty($date) ? null : $date->format('Y-m-d');
    }

//    $event->days_till_event
    public function getDaysTillEvent(\DateTime $dt = null, $inc = false) {
        if (!($dt instanceOf \DateTime)) {
            $dt = new \DateTime('now', new \DateTimeZone(Yii::$app->timeZone));
        }
        $eventDate = $this->getNext_Date_Raw($dt, $inc);
        return $eventDate->diff($dt)->format("%d");
    }

//    public function getDates_old(\DateTimeInterface $after = null, \DateTimeInterface $before = null, $inc = true) {
//        if (empty($after)) {
//            $after = new \DateTime('first day of january', new \DateTimeZone(Yii::$app->timeZone));
//        }
//
//        if (empty($before)) {
//            $before = new \DateTime('last day of december +' . (int) Yii::$app->params['virtualEventLimit'] . ' years', new \DateTimeZone(Yii::$app->timeZone));
//        }
//
//        $rule = new Rule(
//                $this->recurrence_pattern, \DateTime::createFromFormat('Y-m-d', $this->start_date, new \DateTimeZone(Yii::$app->timeZone)), \DateTime::createFromFormat('Y-m-d', $this->end_date, new \DateTimeZone(Yii::$app->timeZone)), Yii::$app->timeZone
//        );
//
//        $transformerConfig = new ArrayTransformerConfig();
//        $transformerConfig->enableLastDayOfMonthFix();
//        $transformerConfig->setVirtualLimit(Yii::$app->params['virtualEventLimit']);
//
//        $constraint = null;
//        if ($this->eventException instanceOf EventException) {
//            $constraint = new ExceptionRRuleConstraint(
//                    $this->eventException->recurrence_pattern, \DateTime::createFromFormat('Y-m-d', $this->eventException->start_date, new \DateTimeZone(Yii::$app->timeZone)), \DateTime::createFromFormat('Y-m-d', $this->end_date, new \DateTimeZone(Yii::$app->timeZone)), true, Yii::$app->timeZone, Yii::$app->params['virtualEventLimit']
//            );
//        }
//
//        $transformer = new ArrayTransformerWeekends($transformerConfig, ArrayTransformerWeekends::DIRECTION_FUTURE);
//        $dates       = $transformer->transform($rule, $constraint)->startsBetween($after, $before, $inc)->toArray();
//
////        $exceptionRule        = new Rule(
////                $this->eventException->recurrence_pattern, $this->eventException->start_date, $this->end_date, Yii::$app->timeZone
////        );
////        $exceptionDates       = $transformer->transform($exceptionRule)->startsBetween($after, $before, $inc)->toArray();
////        $res = $this->_filterDates($dates, $exceptionDates);
//        return $dates;
//    }
//    function setException_Is_Recurring($val) {
//        $val = strtolower($val);
//        if (in_array($val, self::getAllRecurrTypes())) {
//            switch ($val) {
//                case self::FREQUENCY_MONTH:
//            };
//        }
//    }
//    protected function _filterDates(array $dates, array $exceptions){
//        $resDates = array_filter($dates, function($val) use ($exceptions){
//            foreach ($exceptions as $ex){
//                $exStart = $ex->getStart();
//                $valStart = $val->getStart();
//                if($exStart == $valStart){
//                    return false;
//                }
//            }
//            return true;
//        });
//        return $resDates;
//    }


    public function beforeSave($insert) {
        if (!parent::beforeSave($insert)) {
            return false;
        }

        $this->curr_date = $this->next_date;
        return true;
    }

    public function afterSave($insert, $changedAttributes) {
        parent::afterSave($insert, $changedAttributes);
        Calendar::fillEvent($this->id);
    }

    /**
     * Обновляет curr_date события
     * @param Event $event
     * @return boolean|null
     */
    public static function fillOne($event) {
//        $event = Event::findOne($id);
        if (!empty($event)) {
            if ($event->next_date_raw > $event->curr_date_raw) {
                $event->previous_date = $event->curr_date;
                $event->curr_date     = $event->next_date;
                $res                  = $event->save();
//                if ($res) {
//                    self::trigger(self::EVENT_HAS_COME, $event);
//                }
                return $res;
            }
        }
        return NULL;
    }

    /**
     * Обновляет curr_date всех событий
     * @param Event $event
     * @return null
     */
    public static function fillAll() {
        $events = Event::find();
//        $events = Event::find()->where('curr_date < CURDATE()');
        foreach ($events->each() as $event) {
            self::fillOne($event);
        }
    }

    /**
     * Обновляет curr_date просроченных событий
     * @param Event $event
     * @return null
     */
    public static function fillOutdated($date = null) {
        $events = Event::find()->overdue($date);
        foreach ($events->each() as $event) {
            self::fillOne($event);
        }
    }

}
