<?php

namespace common\models;

use Yii;
use common\models\UserToken;
use yii\base\InvalidCallException;

/**
 * UserToken2 версия с возможностью сохранения параметров
 *
 * @author fistashkin
 */
class UserToken2 extends UserToken {
    public const TYPE_INVITE_USER = 'invite_user';
    public const TYPE_VIBER_ACTIVATION = 'viber_activation';
    public const TYPE_VIBER_DEACTIVATION = 'viber_deactivation';
    public const GUEST_USER_ID = -1;

    public function getParams2() {
        return json_decode($this->params, true);
    }

    public function setParams2($params = []) {
        $this->params = json_encode($params, JSON_UNESCAPED_UNICODE);
    }

    /**
     * @param mixed $user_id
     * @param string $type
     * @param int|null $duration
     * @param array|null $params
     * @return bool|UserToken
     * @throws \yii\base\Exception
     */
    public static function create($user_id, $type, $duration = null, $params = []) {
        $model = new self;
        $model->setAttributes([
            'user_id' => $user_id,
            'type' => $type,
            'token' => Yii::$app->security->generateRandomString(self::TOKEN_LENGTH),
            'expire_at' => $duration ? time() + $duration : null,
            'params2' => $params,
        ]);

        if (!$model->save()) {
            throw new InvalidCallException;
        };
        return $model;
    }

}
