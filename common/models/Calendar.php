<?php

namespace common\models;

use Yii;
use yii\helpers\Url;
use common\models\RelEventOrg;

/**
 * This is the model class for table "{{%calendar}}".
 *
 * @property string $event_date
 * @property string $type
 * @property string $event_id
 * @property string $user_id Ссылка на профиль пользователя
 * @property string $title
 * @property string $description
 */
class Calendar extends \yii\db\ActiveRecord {

    const TYPE_EVENT  = 'event';
    const TYPE_REMIND = 'remind';

    public static function getAllTypes() {
        $lst = [
            self::TYPE_EVENT  => 'Событие',
            self::TYPE_REMIND => 'Напоминание',
        ];
        return $lst;
//        return in_array($type, $lst) ? $lst[$type] : $lst;
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName() {
        return '{{%calendar}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
            ['id', 'unique'],
            [['event_date'], 'safe'],
//            [['type', 'event_id'], 'required'],
            [['type'], 'string'],
//            [['event_id', 'user_id', 'reminder_id', 'org_id'], 'integer'],
            [['event_id', 'user_id', 'reminder_id', 'org_id'], 'safe'],
            [['title'], 'string', 'max' => 250],
            [['description'], 'string', 'max' => 16383],
            [['event_date', 'type', 'event_id', 'user_id', 'reminder_id'], 'unique', 'targetAttribute' => ['event_date', 'type', 'event_id', 'user_id', 'reminder_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels() {
        return [
            'event_date'  => 'Event Date',
            'type'        => 'Type',
            'event_id'    => 'Event ID',
            'user_id'     => 'User ID',
            'title'       => 'Title',
            'description' => 'Description',
        ];
    }

    /**
     * {@inheritdoc}
     * @return \common\models\query\CalendarQuery the active query used by this AR class.
     */
    public static function find() {
        return new \common\models\query\CalendarQuery(get_called_class());
    }

    public function fields() {
        return [
//            'title',
            'title' => function($model) {
                return $model->title . ': ' . $model->org->title;
            },
            'start' => 'event_date',
//            'url'   => function($model) {
//                $url = ($model->type == self::TYPE_EVENT) ? Url::toRoute(['rel-event-org/view', 'id' => $model->reminder_id], true) : Url::toRoute(['rel-event-org/view', 'id' => $model->reminder_id], true);
//                return $url;
//            },
            'color' => function($model) {
                return ($model->type == self::TYPE_EVENT) ? 'DarkCyan' : 'Maroon';
//                return '#ff0000';
            },
//            'org' => 'org.title',
            'description' => function($model) {
//                $res = ($model->type == self::TYPE_EVENT) ? 'Событие для "' : 'Напоминание для "';
                $res = sprintf('"%s"', $model->org->title);
                return $res;
            }
//                $orgs = ArrayHelper::getColumn($model->orgs, 'title');
                //https://fullcalendar.io/docs/eventRender
//                return implode('<br>', $orgs);
//            }
        ];
    }

    /**
     * @return \common\models\EventException
     */
    public function getEvent() {
        return $this->hasOne(Event::class, ['id' => 'event_id']);
    }

    /**
     * @return \common\models\EventException
     */
    public function getReminder() {
        return $this->hasOne(RelEventOrg::class, ['id' => 'reminder_id']);
    }

    /**
     * @return \common\models\EventException
     */
    public function getOrg() {
        return $this->hasOne(Org::class, ['id' => 'org_id']);
    }

    /**
     * @return \common\models\EventException
     */
    public function getUser() {
        return $this->hasOne(User::class, ['id' => 'user_id']);
    }

    /**
     * Заполняет календарь событиями и напоминаниями
     * Пересчитывает весь календарь
     * Без учёта пользователей
     * @param integer $reminder_id
     * @return null
     */
    public static function FillCalendar() {
        self::deleteAll();
//        self::deleteAll('event_date >= CURDATE()');
        $events = Event::find()->where(['>', 'end_date', date('Y-m-d')]);
        foreach ($events->each() as $event) {
            self::fillEvent($event->id);
        }
    }

    /**
     * Обновляет календарь событиями и напоминаниями для заданного события $event_id
     * @param integer $event_id
     */
    public static function FillEvent($event_id) {
        self::deleteAll('event_id = :event_id AND event_date >= :date', [':event_id' => $event_id, ':date' => date('Y-m-d')]);
//        self::deleteAll(['event_id' => $event_id, ['>=', 'event_date', date('Y-m-d')] ]);
        $event = Event::find()
                        ->where(['id' => $event_id])
                        ->andWhere(['>', 'end_date', date('Y-m-d')])
                        ->with('reminders')->one();
        if (!empty($event)) {
            foreach ($event->reminders as $reminder) {
                self::fillReminder($reminder->id);
            }
        }
    }

    /**
     * Обновляет календарь событиями и напоминаниями для заданной связи $reminder_id
     * Без учёта пользователей
     * @param integer $reminder_id
     * @return null
     */
    public static function fillReminder($reminder_id) {
//        $reminder = RelEventOrg::findOne($reminder_id);
        $reminder = RelEventOrg::find()->with('event', 'owner')->where(['id' => $reminder_id])->one();
//        self::deleteAll(['reminder_id' => $reminder_id, 'user_id' => getMyId()]);
        self::deleteAll('reminder_id = :reminder_id AND event_date >= :date', [':reminder_id' => $reminder_id, ':date' => date('Y-m-d')]);
//        self::deleteAll(['reminder_id' => $reminder_id, 'event_date >= 1'], [':date' => date('Y-m-d')]);
        if (!empty($reminder)) {
            $event = $reminder->event;
            foreach ($event->dates as $d) {
                //Calendar event
                $calendar = new Calendar([
                    'event_date'  => $d->getStart()->format('Y-m-d'),
                    'event_id'    => $event->id,
                    'reminder_id' => $reminder_id,
                    'user_id'     => $reminder->owner->id,
                    'org_id'      => $reminder->org_id,
                    'title'       => $event->title,
                    'description' => !empty($reminder->description) ? $reminder->description : $event->description,
                    'type'        => Calendar::TYPE_EVENT,
                ]);
                $calendar->save();

//                    $d                  = \DateTime::createFromFormat('Y-m-d', $d);
                //Calendar reminder
                //Uncomment to save reminders on Calendar table
                $remind             = new Calendar($calendar->attributes);
                $remind->id         = null;
                $remind->event_date = $reminder->getRemind_Date($d->getStart());
                $remind->type       = Calendar::TYPE_REMIND;
                $remind->save();
            }
        }
    }

}
