<?php

namespace common\models;

use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use common\models\User;
use common\models\Calendar;
use yii\console\Application as ConsoleApplication;

/**
 * This is the model class for table "{{%rel_event_org}}".
 * Таблица напоминаний
 *
 * @property string $id
 * @property string $org_id
 * @property string $event_id
 * @property int $remind_before Напомнить за х дней до события
 * @property int $show_before Показать за х дней
 * @property string $created_at
 * @property string $updated_at
 * @property string $created_by
 * @property string $updated_by
 *
 * @property Event $event
 * @property Org $org
 * @property User $createdBy
 * @property User $updatedBy
 */
class RelEventOrg extends \yii\db\ActiveRecord {
//    public $event_date;
//    public $remind_date;

    /**
     * Пользовательские отметки о состоянии
     * Поле status
     */
    const STATUS_ACTIVE       = 'active'; //Отметка по умолчанию. Событие ожидает действий пользователя
    const STATUS_IN_WORK      = 'in_work'; //Пользователь отметил, что ведет работу над событием
    const STATUS_FINISHED     = 'finished'; //Отметка пользователя о сдаче отчёта/платежа
    const STATUS_CANCELED     = 'canceled'; //Отчёт нужен, но был просрочен. Отменен системой.
    const STATUS_SOON_FAKE    = 'soon'; //Подходит время сдачи. Используется при фильтрации списка GridView
    const STATUS_EXPIRED_FAKE = 'expired'; //Просрочено. Используется при фильтрации списка GridView
    const STATUS_NEEDED_FAKE  = 'needed'; //Осталось сдать. Используется при фильтрации списка GridView

//    const STATUS_EXPIRED  = 'Просрочено'; //Дата события прошла, 

    public static function getAllStatuses($status = null) {

        $lst = [
            self::STATUS_CANCELED => 'Пропущено', //Отменено
            self::STATUS_ACTIVE   => 'Активно',
            self::STATUS_IN_WORK  => 'В работе',
            self::STATUS_FINISHED => 'Завершено',
//            self::STATUS_EXPIRED  => 'Просрочено',
        ];
        return array_key_exists($status, $lst) ? $lst[$status] : $lst;
    }

    public function init() {
        parent::init();
        $this->on(static::EVENT_AFTER_DELETE, function($event) {
            Calendar::deleteAll('reminder_id = :reminder_id AND event_date >= :date', [':reminder_id' => $this->id, ':date' => date('Y-m-d')]);
        });
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName() {
        return '{{%rel_event_org}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
            ['remind_before', 'default', 'value' => ((Yii::$app instanceof ConsoleApplication) || Yii::$app->user->isGuest) ? 15 : Yii::$app->user->identity->userProfile->remind_before_default],
            ['remind_before', 'integer', 'max' => 15],
//            [['org_id', 'event_id', 'remind_before', 'show_before', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
//            [['event_id'], 'exist', 'skipOnError' => true, 'targetClass' => Event::className(), 'targetAttribute' => ['event_id' => 'id']],
//            [['org_id'], 'exist', 'skipOnError' => true, 'targetClass' => Org::className(), 'targetAttribute' => ['org_id' => 'id']],
            ['status', 'default', 'value' => self::STATUS_ACTIVE],
            ['status', 'in', 'range' => array_keys(self::getAllStatuses())],
            ['status_previous', 'in', 'range' => array_keys(self::getAllStatuses())],
            [['description'], 'string', 'max' => 16383],
//            [['event_title'], 'safe']
//            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['created_by' => 'id']],
//            [['updated_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['updated_by' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels() {
        return [
            'id'            => 'Номер',
            'org_id'        => 'ID организации',
            'event_id'      => 'ID события',
            'remind_date'   => 'Дата напоминания',
            'event_date'    => 'Дата события',
//            'event_org_type'      => 'Организационно-правовая форма',
//            'event_tax_type'      => 'Организационно-правовая форма',
//            'event_hired_labour'      => 'Организационно-правовая форма',
//            'event_frequency'      => 'Организационно-правовая форма',
            'remind_before' => 'Напомнить за Х дней до события',
            'show_before'   => 'Показать за Х дней',
            'status'        => 'Состояние',
            'description'   => 'Текст напоминания',
            'created_at'    => 'Дата создания',
            'updated_at'    => 'Дата изменения',
            'created_by'    => 'Создано',
            'updated_by'    => 'Изменено',
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            TimestampBehavior::class,
            BlameableBehavior::class,
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEvent() {
        return $this->hasOne(Event::className(), ['id' => 'event_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrg() {
        return $this->hasOne(Org::className(), ['id' => 'org_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOwner() {
        return $this->hasOne(User::className(), ['id' => 'owner_id'])->via('org');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy() {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy() {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }

    /**
     * {@inheritdoc}
     * @return \common\models\query\RelEventOrgQuery the active query used by this AR class.
     */
    public static function find() {
        return new \common\models\query\RelEventOrgQuery(get_called_class());
    }

//    public function getEvent_Title() {
//        if (!empty($this->event)) {
//            return $this->event->title;
//        }
//    }

    public function getRemind_Date_Raw(\DateTime $start_date = null) {
        $date = ($start_date instanceof \DateTime) ? $this->event->getNext_Date_Raw($start_date) : $this->event->curr_date_raw;
        if (!empty($date)) {
//            $date = \DateTime::createFromFormat('Y-m-d', $date, new \DateTimeZone(Yii::$app->timeZone));
            $date->modify('-' . $this->remind_before . 'day');
            return $date;
        }
        return null;
    }

    public function getRemind_Date(\DateTime $start_date = null) {
        $date = $this->getRemind_Date_Raw($start_date);
        return empty($date) ? null : $date->format('Y-m-d');
    }

    public function afterSave($insert, $changedAttributes) {
        parent::afterSave($insert, $changedAttributes);
        //Обновляем календарь, если есть изменения дат
        //TODO isNewRecord
//        $vl = $this->getDirtyAttributes(['remind_before']);
//        if ($insert || count($this->getDirtyAttributes(['remind_before']))) {
        if ($insert || array_key_exists('remind_before', $changedAttributes)) {
            Calendar::fillReminder($this->id);
        }
    }

    public function setStatus_($status) {
//        if (array_key_exists($status, self::getAllStatuses())) {
        $this->status_previous = $this->status;
        $this->status          = $status;
//        }
    }

}
