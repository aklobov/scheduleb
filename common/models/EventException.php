<?php

namespace common\models;

use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use common\models\query\EventExceptionQuery;

/**
 * This is the model class for table "{{%event_exception}}".
 *
 * @property int $id
 * @property string $event_id
 * @property string $start_date
 * @property string $recurrence_pattern
 * @property string $created_at
 * @property string $updated_at
 * @property string $created_by
 * @property string $updated_by
 *
 * @property User $createdByUser
 * @property Event $event
 * @property Org $org
 * @property User $updatedByUser
 */
class EventException extends EventBase {

    /**
     * {@inheritdoc}
     */
    public static function tableName() {
        return '{{%event_exception}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
            [['id'], 'unique'],
            [['id', 'event_id', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
            [['start_date', 'start_date_'], 'safe'],
            [['event_id'], 'exist', 'skipOnError' => true, 'targetClass' => Event::className(), 'targetAttribute' => ['event_id' => 'id']],
            ['frequency', 'default', 'value' => self::FREQUENCY_YEAR],
            ['frequency', 'in', 'range' => array_keys(self::getAllFrequencies())],
//            [['org_id'], 'exist', 'skipOnError' => true, 'targetClass' => Org::className(), 'targetAttribute' => ['org_id' => 'id']],
//            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['created_by' => 'id']],
//            [['updated_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['updated_by' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels() {
        $labels = parent::attributeLabels();
        return $labels + [
            'id'         => 'ID',
            'event_id'   => 'номер связанного события',
//            'recurrence_pattern' => 'Формула события',
            'created_at' => 'Дата создания',
            'updated_at' => 'Дата изменения',
            'created_by' => 'Создал',
            'updated_by' => 'Изменил',
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            TimestampBehavior::class,
            BlameableBehavior::class,
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedByUser() {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEvent() {
        return $this->hasOne(Event::className(), ['id' => 'event_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrg() {
        return $this->hasOne(Org::className(), ['id' => 'org_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedByUser() {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }

    /**
     * {@inheritdoc}
     * @return EventExceptionQuery the active query used by this AR class.
     */
    public static function find() {
        return new EventExceptionQuery(get_called_class());
    }

}
