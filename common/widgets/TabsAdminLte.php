<?php

namespace common\widgets;

use yii\bootstrap\Tabs;
use yii\helpers\Html;

/**
 * Description of TabsAdminLte
 *
 * @author fistashkin
 */
class TabsAdminLte extends Tabs{
    public function run() {
        $html = parent::run();
        return Html::tag('div', $html, ['class' => 'nav-tabs-custom']);
    }
}
