<?php

namespace common\grid;

use yii\grid\DataColumn;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/**
 * Class SQLEnumColumn
 * [
 *      'class' => 'common\grid\SQLEnumColumn',
 *      'attribute' => 'role',
 * ]
 * Expand sql enum field to list
 * @package common\components\grid
 */
class SQLEnumColumn extends \common\grid\EnumColumn {

    public $format = 'html';

    /**
     * @param mixed $model
     * @param mixed $key
     * @param int $index
     * @return mixed
     */
    public function getDataCellValue($model, $key, $index) {
        $value = parent::getDataCellValue($model, $key, $index);
//        $value = str_replace(',', '<br />', $value);
        if (!empty($value)) {
            $items = explode(',', $value);
            $res   = [];
            foreach ($items as $item) {
                $res[] = ArrayHelper::getValue($this->enum, $item, $item);
            }
            $value = implode(',<br>', $res);
        }
        return $value;
    }

}
