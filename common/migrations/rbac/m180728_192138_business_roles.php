<?php

use yii\db\Schema;
use common\rbac\Migration;
use common\models\User;

class m180728_192138_business_roles extends Migration {

    public function up() {
        /*
         * Оплаченная учетная запись пользователя
         * Полный доступ ко всем функциям в режиме 1 пользователя
         */
        $user_paid = $this->auth->createRole(User::ROLE_USER_PAID);
        $user_paid->description = 'Оплаченная учетная запись пользователя. Полный доступ ко всем функциям в режиме одного пользователя.';
        $this->auth->add($user_paid);

        /*
         * Оплаченная учетная запись администратора группы
         * Полный доступ ко всем функциям в многопользовательском режиме
         */
        $user_manager = $this->auth->createRole(User::ROLE_USER_MANAGER);
        $user_manager->description = 'Оплаченная учетная запись администратора группы. Полный доступ ко всем функциям в многопользовательском режиме.';
        $this->auth->add($user_manager);


        $user = $this->auth->getRole(User::ROLE_USER);
        $manager = $this->auth->getRole(User::ROLE_MANAGER);
        $this->auth->addChild($user_paid, $user);
        $this->auth->addChild($user_manager, $user_paid);
//        $this->auth->addChild($manager, $user_manager);
        
        $this->auth->assign($user_manager, 4);
        $this->auth->assign($user_paid, 5);
    }

    public function down() {
        $this->auth->remove($this->auth->getRole(User::ROLE_USER_PAID));
        $this->auth->remove($this->auth->getRole(User::ROLE_USER_MANAGER));
    }

}
