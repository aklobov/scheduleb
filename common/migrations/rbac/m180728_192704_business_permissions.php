<?php

use yii\db\Schema;
use common\rbac\Migration;
use common\models\User;

class m180728_192704_business_permissions extends Migration {

    public function up() {
        $userManagerRole = $this->auth->getRole(User::ROLE_USER_MANAGER);
//        $userPaidRole = $this->auth->getRole(User::ROLE_USER_PAID);

        $manageGroup = $this->auth->createPermission('manageGroup');
        $manageGroup->description = 'Разрешение на управление группами и пользователями в группах.';
        $this->auth->add($manageGroup);

        $this->auth->addChild($userManagerRole, $manageGroup);
        
        
//        $managerRole = $this->auth->getRole(User::ROLE_MANAGER);
//        $viewAllOrgs = $this->auth->createPermission('viewAllOrgs');
//        $viewAllOrgs->description = 'Разрешение на просмотр списка всех организаций.';
//        $this->auth->add($viewAllOrgs);
//
//        $this->auth->addChild($managerRole, $viewAllOrgs);
        
        
//        $managerRole = $this->auth->getRole(User::ROLE_MANAGER);
//        $manageEventDefault = $this->auth->createPermission('manageEventDefault');
//        $manageEventDefault->description = 'Разрешение на управление стандартными событиями (отчётами и налогами).';
//        $this->auth->add($manageEventDefault);
//
//        $this->auth->addChild($managerRole, $manageEventDefault);
        
        
        $userPaidRole = $this->auth->getRole(User::ROLE_USER_PAID);
        $notifyByMessengers = $this->auth->createPermission('notifyByMessengers');
        $notifyByMessengers->description = 'Разрешение на получение уведомлений через мессенджеры.';
        $this->auth->add($notifyByMessengers);

        $this->auth->addChild($userPaidRole, $notifyByMessengers);
        
        $exceedOrgLimit = $this->auth->createPermission('exceedOrgLimit');
        $exceedOrgLimit->description = 'Разрешение на одновременную работу с организациями сверх заданного лимита.';
        $this->auth->add($exceedOrgLimit);

        $this->auth->addChild($userPaidRole, $exceedOrgLimit);
        
        
    }

    public function down() {
        $this->auth->remove($this->auth->getPermission('manageGroup'));
        $this->auth->remove($this->auth->getPermission('manageEventDefault'));
        $this->auth->remove($this->auth->getPermission('notifyByMessengers'));
        $this->auth->remove($this->auth->getPermission('exceedOrgLimit'));
    }

}
