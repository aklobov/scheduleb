<?php

use common\models\User;
use common\rbac\Migration;
use common\rbac\rule\EventDefaultRule;

class m181017_151112_manage_event_default extends Migration {

    public function up() {
        $rule = new EventDefaultRule();
        $this->auth->add($rule);

        $role = $this->auth->getRole(User::ROLE_MANAGER);

        $manageEventDefaultPermission           = $this->auth->createPermission('manageEventDefault');
        $manageEventDefaultPermission->description = 'Разрешение на управление стандартными событиями (отчётами и налогами).';
        $manageEventDefaultPermission->ruleName = $rule->name;

        $this->auth->add($manageEventDefaultPermission);
        $this->auth->addChild($role, $manageEventDefaultPermission);
    }

    public function down() {
        $permission = $this->auth->getPermission('manageEventDefault');
        $rule       = $this->auth->getRule('eventDefaultRule');

        $this->auth->remove($permission);
        $this->auth->remove($rule);
    }

}
